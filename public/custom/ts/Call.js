"use strict";
class Call {
    constructor(url, type, dataType) {
        this.url = url;
        this.type = type;
        this.dataType = dataType;
        this.data = {};
        this.setData = (key, data) => {
            this.data[key] = data;
            return this;
        };
        this.setUrl = (url) => {
            this.url = url;
            return this;
        };
        this.onSuccess = (data) => {
        };
        this.success = (cb) => {
            this.onSuccess = cb;
        };
        this.request = () => {
            var that = this;
            console.log(that);
            $.ajax({
                url: that.url,
                type: that.type,
                data: that.data,
                success: function (result) {
                    //return that.onSuccess(result);
                }
            });
        };
        if (!this.dataType)
            this.dataType = 'json';
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Call;
//# sourceMappingURL=Call.js.map