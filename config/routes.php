<?php

return [

    'files' => [

        'dashboard.php',
        'admin-cruds.php',
        'reports.php',
        'admin/posts.php',
        'admin/verify_posts.php',
        'admin/mail.php',
        'admin/domains.php',
        'admin/announcements.php',
        'admin/earnings.php',
        'admin/payment.php',
        'admin/users.php',
        'admin/personals.php',
        'admin/personalprices.php',
        'campaigns.php',
        'client/intermediar.php',
        'payments.php',
        'referrals.php',
        'domains.php',
        'user-posts.php',
        'teste.php',
        'services/location.php',
        'concurs.php',
    ]
];