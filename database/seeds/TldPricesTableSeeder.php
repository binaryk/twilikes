<?php

use Carbon\Carbon as Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class UserTableSeeder
 */
class TldPricesTableSeeder extends Seeder
{
    public function run()
    {
        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        if (env('DB_CONNECTION') == 'mysql') {
            DB::table("tld_prices")->truncate();
        }

        //Add the master administrator, user id of 1
        $users = [
            [
                'id' => 1,
                'name'              => 'com',
                'price'             => 11.13,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'id' => 2,
                'name'              => 'net',
                'price'             => 10.64,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'id' => 3,
                'name'              => 'me',
                'price'             => 11.95,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'id' => 4,
                'name'              => 'biz',
                'price'             => 8.00,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'id' => 5,
                'name'              => 'org',
                'price'             => 10.64,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'id' => 6,
                'name'              => 'info',
                'price'             => 5.75,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'id' => 7,
                'name'              => 'co',
                'price'             => 10.82,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'id' => 8,
                'name'              => 'cc',
                'price'             => 17.4,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'id' => 9,
                'name'              => 'mobi',
                'price'             => 10.72,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
        ];

        DB::table('tld_prices')->insert($users);

        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }
}