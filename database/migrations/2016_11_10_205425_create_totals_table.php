<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTotalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newreports', function(Blueprint $t) {
            $t->increments('id');
            $t->integer('user_id')->index();
            $t->date('date');
            $t->integer('clicks')->default(0)->nullable();
            $t->integer('clicks_calitate')->default(0)->nullable();
            $t->float('quality',8,4)->default(0)->nullable();
            $t->float('ecpc',8,4)->default(0)->nullable();
            $t->float('ecpm', 8, 4)->default(0)->nullable();
            $t->float('earnings', 8, 4)->default(0)->nullable();
            $t->softDeletes();
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newreports');
    }
}
