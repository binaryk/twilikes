<?php namespace App\Http\Controllers\Superadmin;

use \App\Http\Controllers\Basic\BaseController;
use App\Models\Access\User\User;
use Illuminate\Support\Facades\Input;

use App\Repositories\Superadmin\Nomenclatoare\PersonalPrices\Grid;
use App\Repositories\Superadmin\Nomenclatoare\PersonalPrices\Rows;
use App\Repositories\Superadmin\Nomenclatoare\PersonalPrices\Form;
use App\Repositories\Superadmin\Nomenclatoare\PersonalPrices\Action;

class PersonalPricesController extends BaseController
{

    /**
     * $this->data() - vine din BaseController cu chestii comune (breadcrumb, meniu, ....)
     */
    public function getGrid($user_id)
    {
        $this->makeNavigation();
        return
            view('superadmin.nomenclatoare.personalprices.grid.index')
                ->with( $this->data('Eargnings', 'pentru filtrare') )
                ->withGrid( (new Grid())->parameters( Input::all())->addParameter('user_id',$user_id)->settings() )
                ->withToolbar( str_replace([Chr(13), Chr(10), Chr(9)], '', view('superadmin.nomenclatoare.personalprices.grid.toolbar')
                    ->withActionRoute(\URL::route('superadmin.nomenclatoare.personalprices.get-action-form', ['action' => 'insert', 'user_id' => $user_id]))
                    ->withActionBack(route('superadmin.nomenclatoare.personals.index'))->render()) )

                ->withIcon(null)
                ->withTitle('Prices for user: '.User::find($user_id)->name.', with email: '.User::find($user_id)->email)
                ->withDescription(null)
            ;
    }

    /*
     * Generarea inregistrari pentru Grid
     */
    public function dataSource($user_id)
    {
        return (new Rows())->parameters(Input::all())->addParameter('user_id',$user_id)->settings()->response();
    }

    /*
     * Trimite catre index formularul "actions"
     */
    public function getActionForm($action,$user_id, $id = NULL)
    {

        $form =
            (new Form($action, $id))
                ->setGridId("gridPersonalPrices")
                ->route(\URL::route('superadmin.nomenclatoare.personalprices-do-action', ['action' => $action,'user_id' => $user_id, 'id' => $id]))
        ;
        return \Response::json([
            'caption' => $form->caption(),
            'form'    => $form->render(),
            'action'  => $form->actionCaption(),
            'route'   => $form->getRoute(),
            'impact' => $action,
        ]);
    }

    /**
     * do Action - se face adaugarea/modificarea/stergere
     */
    public function doAction( $action, $user_id, $id = NULL )
    {

        $data = Input::get('data');
        $data['user_id'] = $user_id;
        $action = (new Action($action, $id))->data($data);
        return \Response::json($action->commit());
    }

}