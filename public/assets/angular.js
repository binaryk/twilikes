'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.directives = exports.controllers = undefined;

var _angular = require('angular');

var _angular2 = _interopRequireDefault(_angular);

var _lodash = require('lodash');

var ld = _interopRequireWildcard(_lodash);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var dms = _angular2.default.module('dms', ['dms.controllers', 'dms.directives', 'dms.services']).config(function ($interpolateProvider, $locationProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
    /* $locationProvider.html5Mode({
         enabled: true,
         requireBase: false
     });*/
}).run(function ($rootScope) {
    $rootScope.config = _config;
});
'use strict';
var controllers = exports.controllers = _angular2.default.module('dms.controllers', []);
'use strict';
function MainCtrl($scope) {
    console.log("MainCtrl");
}

MainCtrl.$inject = ['$scope'];
controllers.controller('MainCtrl', MainCtrl);
'use strict';

var services = _angular2.default.module('dms.services', []);
'use strict';
var directives = exports.directives = _angular2.default.module('dms.directives', []);
//# sourceMappingURL=angular.js.map
