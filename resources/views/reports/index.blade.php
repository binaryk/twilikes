@extends("template.~template")

@section('content')

    <div id="reports-wrapper">
       @include('reports.demo')
    </div>


@stop

@section('js')
    @parent
   <script type="text/javascript">
   		window.serverDate =  '{!! $date !!}';
   </script>
    <script type="text/javascript" src="{!! asset('theme/js/moment/moment.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('theme/js/datepicker/daterangepicker.js') !!}"></script>
@stop