@extends('template.~template')

@section('content')

    <div id="dashboard">
        @include('dashboard.content')
    </div>

    <div id="profile">
        <div class="modal fade" id="todayNotice" data-toggle="modal" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" @click="close()"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Notice</h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger" v-show="true">
                            <p>
                                Please note that stats for 1 November will be updated as soon as possible. We experienced DDOS attacks and some difficulties in working on full capacity lately.
                            </p>

                            <br>
                            We assure you that our specialists and technicians are continuously working to remedy the technical problems. Thanks for understanding!
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal" data-toggle="modal" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" @click="close()"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Subdomain title</h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger" v-show="errMsg">
                            @{{errMsg}}
                        </div>
                        <div class="alert alert-success" v-show="successMsg">
                            @{{successMsg}}
                        </div>
                        <div class="form-group" v-bind:class="{ 'has-error': errMsg }">
                            <label for="subdomain" class="control-label col-md-3">Subdomain</label>
                            <input type="text" class="form-control col-md-9" id="subdomain" v-model="subdomain" placeholder="funny...">
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="save_subdomain" @click="submit()">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
    <script src="{!! asset('theme/js/chartjs/chart.min.js') !!}"></script>

    <script src="{!! asset('custom/js/vue/profile/profile.js') !!}"></script>

    <script type="text/javascript">
        @if(@$firstLogin)
                Profile.base = "{!! url('/') !!}"
        Profile.showModal();
        @endif
    </script>

@endsection