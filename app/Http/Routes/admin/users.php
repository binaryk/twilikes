<?php
Route::group(['namespace' => 'Superadmin', 'middleware' => 'admin'], function () {

    Route::get('superadmin/nomenclatoare/users', 'UsersController@getGrid')
        ->name('superadmin.nomenclatoare.users.index');

    Route::post('superadmin/nomenclatoare/users-source', 'UsersController@dataSource')
        ->name('superadmin.nomenclatoare.users.data-source');

    /*
     * Sa se incarce formularul de actiun (actions form)
     */
    Route::post('superadmin/nomenclatoare/users-get-action-form/{action}/{id?}', 'UsersController@getActionForm')
        ->name('superadmin.nomenclatoare.users.get-action-form');

    /*
     * ce se intampla la Adauga/Salveaza/Sterge
     */
    Route::post('superadmin/nomenclatoare/users-action/{action}/{id?}', 'UsersController@doAction')
        ->name('superadmin.nomenclatoare.users-do-action');
});

