@extends('frontend.layouts.master')

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="panel panel-dark panel-flat">
        <div class="panel-heading text-center">
            <a href="#">
                <img src="{!! asset('img/logo.png') !!}" alt="Image" class="block-center img-rounded">
            </a>
        </div>
        <div class="panel-body">
            <p class="text-center pv">PASSWORD RESET</p>
            {!! Form::open(['url' => 'password/email']) !!}
                <p class="text-center">Fill with your mail to receive instructions on how to reset your password.</p>
                <div class="form-group has-feedback">
                    <label for="resetInputEmail1" class="text-muted">Email address</label>
                    {!! Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email')]) !!}
                    <span class="fa fa-envelope form-control-feedback text-muted"></span>
                </div>
                <button type="submit" class="btn btn-danger btn-block">Reset</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
