#!/usr/bin/perl
##########################################################################
# IP2Location - Command Line Interface
#
# Created by: IP2Location.com
#
# Website: www.ip2location.com
#
##########################################################################
#
# COPYRIGHT NOTICE:
#
# Copyright(c) 2002-2009 IP2Location.com.  All Rights Reserved.
#
# By using this program you agree to indemnify IP2Location.com from any
# liability.
#
##########################################################################


BEGIN { push @INC, substr($0, 0, rindex($0, "\/")); } 
use strict;
use Geo::IP2Location;

my $version = "2.00";
my %parameters;
my %switches;

&get_options;
&process_options;

sub print_debug {
	my $i;
	foreach $i (keys(%parameters))
	{
		print STDERR "$i - $parameters{$i}\n";
	}
}

my @ips;
if ($parameters{"ip"} ne "") {
	push (@ips, $parameters{"ip"});
}
if ($parameters{"inputfile"} ne "") {
	if (-e $parameters{"inputfile"}) {
	} else {
		print STDERR "ERROR: Unable to find input file " . $parameters{"inputfile"} . "!\n";
		exit;
	}
	open IN, "<" . $parameters{"inputfile"};
	while (<IN>)
	{
		$_ =~ s/\s//g;
		$_ =~ s/\r//g;
		$_ =~ s/\n//g;
		push (@ips, $_);
	}
	close IN;
}


my $obj = Geo::IP2Location->open($parameters{"datafile"});
my $ip = "";
my $lines = "";

foreach $ip (@ips)
{
	my @array;
	@array = $obj->get_all( $ip );
	unshift (@array, $ip);
	$lines .= &printline(@array) . "\n";
}

if ($parameters{"format"} eq "xml")
{
	$lines = "<xml>\n" . $lines . "</xml>";
} else {
	my @array;
	push(@array, "IP_ADDRESS");
	push(@array, "COUNTRY_SHORT");
	push(@array, "COUNTRY_LONG");
	push(@array, "REGION");
	push(@array, "CITY");
	push(@array, "LATITUDE");
	push(@array, "LONGITUDE");
	push(@array, "ZIPCODE");
	push(@array, "TIMEZONE");
	push(@array, "ISP_NAME");
	push(@array, "DOMAIN_NAME");
	push(@array, "NETSPEED");
	push(@array, "IDDCODE");
	push(@array, "AREACODE");
	push(@array, "WEATHERSTATIONCODE");
	push(@array, "WEATHERSTATIONNAME");
	$lines = &printline(@array) . "\n" . $lines;
}

if ($parameters{"outputfile"} eq "") {
	print STDOUT $lines;
} else {
	open OUT, ">" . $parameters{"outputfile"};
	print OUT $lines;
	close OUT;
}

sub printline
{
	my @array = @_;
	my $line = "";
	my $mode = lc($parameters{"format"});
	if ($mode eq "") {
		$mode = "csv";
	}
	if ($mode eq "csv") {
		my $cell = "";
		foreach $cell (@array)
		{
			$line .= "\"$cell\",";
		}
		$line =~ s/\,$//;
	}
	if ($mode eq "tab") {
		my $cell = "";
		foreach $cell (@array)
		{
			$line .= "$cell\t";
		}
		$line =~ s/\t$//;
	}
	if ($mode eq "xml") {
		my $cell = "";
		$line .= "<ip2location>\n";
		$cell = shift(@array);
		$line .= "  <ip>$cell</ip>\n";
		$cell = shift(@array);
		$line .= "  <countryshort>$cell</countryshort>\n";
		$cell = shift(@array);
		$line .= "  <countrylong>$cell</countrylong>\n";
		$cell = shift(@array);
		$line .= "  <region>$cell</region>\n";
		$cell = shift(@array);
		$line .= "  <city>$cell</city>\n";
		$cell = shift(@array);
		$line .= "  <latitude>$cell</latitude>\n";
		$cell = shift(@array);
		$line .= "  <longitude>$cell</longitude>\n";
		$cell = shift(@array);
		$line .= "  <zipcode>$cell</zipcode>\n";
		$cell = shift(@array);
		$line .= "  <timezone>$cell</timezone>\n";
		$cell = shift(@array);
		$line .= "  <ispname>$cell</ispname>\n";
		$cell = shift(@array);
		$line .= "  <domainname>$cell</domainname>\n";		
		$cell = shift(@array);
		$line .= "  <netspeed>$cell</netspeed>\n";
		$cell = shift(@array);
		$line .= "  <iddcode>$cell</iddcode>\n";
		$cell = shift(@array);
		$line .= "  <areacode>$cell</areacode>\n";
		$cell = shift(@array);
		$line .= "  <weatherstationcode>$cell</weatherstationcode>\n";
		$cell = shift(@array);
		$line .= "  <weatherstationname>$cell</weatherstationname>\n";
		$line .= "</ip2location>";
		#my $cell = "";
		#foreach $cell (@array)
		#{
		#	$line .= "$cell\t";
		#}
		#$line =~ s/\t$//;
	}
	my $key = "This parameter is unavailable for selected data file. Please upgrade the data file.";
	$line =~ s/$key/N\/A/g;
	return $line;
}

sub strip
{
	my $i = shift(@_);
	$i =~ s/^\s+//g;
	$i =~ s/\s+$//g;
	return $i;
}

sub get_options
{
  my $previous_opt = "";
  my $arg = "";
  
  foreach $arg (@ARGV) 
  {
    $_ = $arg;
    if (/^-([a-zA-Z0-9]+)/)
    {
    	my $txt = $1;
    	$switches{$txt} = $txt;
      $previous_opt = $txt;
      #push(@all_opts, $1);
    } else {
      $parameters{$previous_opt} .=  " " . $arg;
      #push(@all_args, $arg);
    }
  }
}

sub process_options
{
	if (defined($switches{"help"})) {
		&display_help;
		exit;
	}

	if (defined($switches{"version"})) {
		&display_version;
		exit;
	}

	$parameters{"inputfile"} = &strip($parameters{"inputfile"});
	$parameters{"ip"} = &strip($parameters{"ip"});
	$parameters{"outputfile"} = &strip($parameters{"outputfile"});
	$parameters{"datafile"} = &strip($parameters{"datafile"});
	$parameters{"format"} = &strip($parameters{"format"});
	
	my $dirty = 0;
	if ($parameters{"datafile"} eq "")
	{
		if ($dirty == 0) {
			&display_help;
			$dirty = 1;
		}
		print STDERR "ERROR: Missing -datafile parameter!\n";
	}

	if (($parameters{"ip"} eq "") and ($parameters{"inputfile"} eq ""))
	{
		if ($dirty == 0) {
			&display_help;
			$dirty = 1;
		}
		print STDERR "ERROR: Missing either -ip or -inputfile parameter!\n";
	}

	if ($dirty == 1) {
		exit;
	}
}

sub display_help
{
	my $l;
	my @c;
	$l = 0;
	$c[++$l] = "                                                                     ";
  $c[++$l] = "USAGE: $0 -datafile <IP2Location_database>               ";
  $c[++$l] = "                      -inputfile <IP_list_input_file>                ";
  $c[++$l] = "                      -ip <single_IP_address>                        ";
  $c[++$l] = "                      -outputfile <output_file_name>                 ";
  $c[++$l] = "                      -format <output_format>                        ";
  $c[++$l] = "                      -help                                          ";
  $c[++$l] = "                                                                     ";
  $c[++$l] = "   -datafile   Specify the path of IP2Location .BIN data file        ";
  $c[++$l] = "                                                                     ";
  $c[++$l] = "   -inputfile  Specify an input file with IP address list            ";
  $c[++$l] = "                                                                     ";
  $c[++$l] = "   -ip         Specify an IP address query                           ";
  $c[++$l] = "                                                                     ";
  $c[++$l] = "   -outputfile Specify output file for query results                 ";
  $c[++$l] = "                                                                     ";
	$c[++$l] = "   -format     Specify output format                                 ";
  $c[++$l] = "               Default format is comma delimited (CSV)               ";
  $c[++$l] = "               Supports CSV, TAB or XML                              ";
  $c[++$l] = "                                                                     ";
  $c[++$l] = "   -help       Display this help file                                ";
  $c[++$l] = "                                                                     ";
  $c[++$l] = "   -version    Display the version number                            ";    
  $c[++$l] = "                                                                     ";	
  $c[++$l] = "   Email    : support\@ip2location.com                               ";  
  $c[++$l] = "   URL      : http://www.ip2location.com                             ";  
  $c[++$l] = "                                                                     "; 
  $c[++$l] = "---------------------------------------------------------------------"; 
	$, = "\n";
	print STDERR @c;
	print STDERR "\n";
}

sub display_version
{
	my $l;
	my @c;
	$l = 0;
	$c[++$l] = "                                                                     ";
  $c[++$l] = " Program Name : $0                                   ";
  $c[++$l] = " Version      : $version                             ";
  $c[++$l] = " Email        : support\@ip2location.com                             ";  
  $c[++$l] = " URL          : http://www.ip2location.com                           ";  
  $c[++$l] = "                                                                     "; 
	$, = "\n";
	print STDERR @c;
	print STDERR "\n";
}