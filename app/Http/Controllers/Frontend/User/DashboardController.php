<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class DashboardController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $first_login = Input::get('first_login') == "1";/*if true ==> e prima logare*/
        javascript()->put([
            'firstLogin' => $first_login,
        ]);

        return view('frontend.user.dashboard')
            ->withUser(access()->user())->with(['firstLogin' => $first_login]);
    }
}
