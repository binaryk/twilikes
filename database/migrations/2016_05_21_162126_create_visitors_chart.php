<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitorsChart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitors_chart', function (Blueprint $t) {
            $t->bigIncrements('id');
            $t->bigInteger('user_id');
            $t->string('data');
            $t->integer('country_id');
            $t->integer('visitors');
            $t->softDeletes();
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visitors_chart');
    }
}
