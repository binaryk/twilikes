<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Models\Bill;
use App\Models\NewReferral;
use App\Models\Payment;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Input;

/**
 * Class LanguageController
 * @package App\Http\Controllers
 */
class PaymentController extends Controller
{

    /**
     * @param $lang
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index()
    {
        return view('payment.index')
            ->with([
            ]);
    }

    public function apiMethods()
    {
        return success(['data' =>  Payment::methods()]);
    }

    public function apiGetUserData()
    {
        return success(['data' => access()->user()]);
    }

    public function apiUpdatePaymentMethod()
    {
        $data = Input::only(['payment_method', 'paypal_email','paypal_name','bank_name','bank_street','iban','holder_name','bank_city','bank_zip','swift_code','wire_street','wire_country','wire_city', 'skrill_name', 'skrill_email']);
        access()->user()->update($data);
        return success([],'Payment method is updated.');
    }

    public function apiPaymentHistory()
    {
        $bills = Bill::where('user_id', access()->user()->id)->get();

        set_time_limit(500);

        return success(
            [
            'bills' => $bills,
            'unpaid' => auth()->user()->real_with_today_unpaid,
            'user' => access()->user(),
            'method' => Payment::methods()[access()->user()->payment_method],
            ]
        );
    }
}