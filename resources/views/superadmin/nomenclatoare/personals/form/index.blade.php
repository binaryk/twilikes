<div class="row">

    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Textbox::make()
            ->name( $field = 'name')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Name' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->name : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => 'Name...',
                'data-field'  => $field,
                'data-type'   => 'textbox',
                'disabled' => 'disabled',
            ])
            ->help('')
            ->render()
        !!}
	</div>
    <div class="col-md-12 form-group">
        {!!
        App\Comptechsoft\Ui\Controls\Selectbox::make()
        ->label('Personal config')
        ->icon('')
        ->wrapper('col-sm-10')
        ->lclass('col-sm-2')
        ->name('personal')
        ->value(@$record->personal)
        ->addClass('form-control form-data-source')
        ->withAttribute('data-field', 'personal')
        ->withAttribute('data-type', 'combobox')
        ->disabled(false)
        ->options(\App\Models\Access\User\User::personal())
        ->render()
        !!}
    </div>

    <hr>

    <div class="col-md-12 form-group">
        {!!
        App\Comptechsoft\Ui\Controls\Selectbox::make()
        ->label('Hide common domains ?')
        ->icon('')
        ->wrapper('col-sm-10')
        ->lclass('col-sm-2')
        ->name('view_common_domains')
        ->value(@$record->view_common_domains)
        ->addClass('form-control form-data-source')
        ->withAttribute('data-field', 'view_common_domains')
        ->withAttribute('data-type', 'combobox')
        ->disabled(false)
        ->options(\App\Models\Access\User\User::view_common_domains())
        ->render()
        !!}
    </div>

    <div class="col-md-12 form-group">
        <h3>User domains</h3>
        @if(count($record->domains) === 0)
            <p> -- No domains -- </p>
        @endif
        <table class="table table-hovered">
            <thead>
            <tr>
                <th>Title</th>
                <th>Available</th>
            </tr>
            </thead>
            <tbody>
            @foreach($record->domains as $domain)
                <tr>
                    <td>{!! $domain->title !!} </td>
                    <td>{!! pingpong($domain->available) !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <ul class="list-group">

        </ul>
    </div>



</div>