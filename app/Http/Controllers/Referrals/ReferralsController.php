<?php

namespace App\Http\Controllers\Referrals;

use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use App\Models\NewReferral;
use App\Models\Referral;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Cookie\CookieJar;
use Cookie;
/**
 * Class LanguageController
 * @package App\Http\Controllers
 */
class ReferralsController extends Controller
{
    /**
     * @param $lang
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index()
    {
        $start_date = '2016-11-7';
        $end_date = '2016-11-13';

            $dbData = DB::table('referrals')
                ->leftJoin('users', 'users.id','=','referrals.invited_id')
                ->select(DB::raw('
                sum(earning) as earnings,
                DATE_FORMAT(referrals.created_at, "%Y-%m-%d") as date,
                users.name,
                referrals.invited_id as connection_id
            '))
                ->where('referrals.invited_by_id', '=', access()->user()->id)
                ->whereDate('referrals.created_at','>=',$start_date)
                ->whereDate('referrals.created_at','<=',$end_date)
                ->orderBy('date')
                ->groupBy('invited_id','date')
                ->get();

        \Debugbar::measure('connections', function() use ($start_date, $end_date){
            $total = DB::table('referrals')
                ->select(DB::raw('sum(earning) as total'))
                ->where('referrals.invited_by_id', '=', access()->user()->id)
                ->whereDate('referrals.created_at','>=',$start_date)
                ->whereDate('referrals.created_at','<=',$end_date)
                ->first();
        });
        return view('referrals.index');
    }

    public function data()
    {
        $connections = auth()->user()->connections;
        $user_id = \Hashids::encode(access()->user()->id);
        $linkview = "http://twilikes.com/affiliate-publisher?user=".$user_id;
        $referral_link = url()->to('affiliate-publisher?user='.$user_id);
        $start_date = Input::get('startDate');
        $end_date   = Input::get('endDate');
        $data       = $this->obtainDatePerDays($start_date, $end_date);
        $out        = [];
        $end_carbon_client_date = Carbon::createFromFormat('Y-m-d',$end_date);
        $server_now = Carbon::now();

        if($server_now->lte($end_carbon_client_date)) {
            DB::setFetchMode(\PDO::FETCH_ASSOC);

            $todayData = DB::table('areferrals')
                ->leftJoin('users', 'users.id', '=', 'areferrals.invited_id')
                ->select(DB::raw('
                sum(earning) as earnings,
                DATE_FORMAT(areferrals.created_at, "%Y-%m-%d") as date,
                users.name name,
                areferrals.invited_id as connection_id,
                areferrals.invited_by_id user_id
            '))
                ->where('areferrals.invited_by_id', '=', access()->user()->id)
                ->whereDate('areferrals.created_at', '=', $end_date)
                ->orderBy('date')
                ->groupBy('invited_id', 'date')
                ->get();

            $dbData = NewReferral::where('user_id', access()->user()->id)
                ->whereDate('date','>=',$start_date)
                ->whereDate('date','<=',Carbon::createFromFormat('Y-m-d',$end_date)->subDay()->toDateString())
                ->get();

            if($todayData && count($todayData) > 0) {
                $todayData = array_pop($todayData);
                $preData = [
                    'date' => $todayData['date'],
                    'earnings' => $todayData['earnings'],
                    'name' => $todayData['name'],
                    'user_id' => $todayData['user_id'],
                    'connection_id' => $todayData['connection_id']
                ];
                $object_with_today_report = new NewReferral($preData);
                $dbData->push($object_with_today_report);
            }

        }   else    {

            $dbData = NewReferral::where('user_id', access()->user()->id)
                ->whereDate('date','>=',$start_date)
                ->whereDate('date','<=',Carbon::createFromFormat('Y-m-d',$end_date)->toDateString())
                ->get();

        }

        foreach($data as $dayData){

            // am access la data orara, si la un array cu oameni pe ziua de azi
            $summ            = 0;
            $day_report      =[];
            $day_report['date'] = $dayData['date']->toDateString();
            $day_report['users'] = [];

            foreach($connections as $connection){
                $day_report['users'][$connection->id] = [
                    'summ' => number_format(0,4),
                    'name' => $connection->name,
                ];
            };

            foreach($dbData as $dData){
             if($day_report['date'] == $dData->date){
                 $day_report['users'][$dData->connection_id] =
                     [
                         'summ' => number_format($dData->earnings,4),
                         'name' => $dData->name,
                     ];
                 }
            }

            $out[] = $day_report;
      }

        $total = 0;
        foreach($out as $item) {
            foreach($item['users'] as $user) {
                $total += floatval($user['summ']);
            }
        }

        if($total){
            $total = number_format($total, 3);
        } else {
            $total = 0;
        }
        return success([ 'data' => $out, 'total' => $total, 'link' => $referral_link, 'linkview' => $linkview ]);
    }


    public function obtainDataPerDays($start_date, $end_date)
    {
        $cStartDate = Carbon::createFromFormat('Y-m-d',$start_date);
        $cEndDate = Carbon::createFromFormat('Y-m-d',$end_date);
        $diff = $cEndDate->diffInDays($cStartDate) + 1 ;
        $data = [];
        if($diff > 0){

            for($i = 0; $i < $diff; $i++){
                $currentData = new Carbon($cStartDate);
                $currentData->toDateString();
                $tmp = [];
                $tmp['date'] = $currentData;
                $tmp['data'] = Referral::where('invited_by_id',access()->user()->id)->whereDate( 'created_at','=',$currentData->toDateString())->get();
                $data[] = $tmp;
                $cStartDate->addDay();
            }
        }

        return $data;
    }


    public function obtainDatePerDays($start_date, $end_date)
    {
        $cStartDate = Carbon::createFromFormat('Y-m-d',$start_date);
        $cEndDate = Carbon::createFromFormat('Y-m-d',$end_date);
        $diff = $cEndDate->diffInDays($cStartDate) + 1 ;
        $data = [];
        if($diff > 0){

            for($i = 0; $i < $diff; $i++){
                $currentData = new Carbon($cStartDate);
                $currentData->toDateString();
                $tmp = [];
                $tmp['date'] = $currentData;
                $tmp['data'] = [];
                $data[] = $tmp;
                $cStartDate->addDay();
            }
        }

        return $data;
    }

    public function extern(\Request $request)
    {
        $user_id = \Hashids::decode(Input::get('user'));
        $user_id = @$user_id[0];
        $response = new Response(view('frontend.auth.register'));
        $response->withCookie(cookie('invited_by', $user_id, 360));
        return $response;
    }



}