<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesEarningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles_earning', function(Blueprint $t) {
            $t->increments('id');
            $t->integer('user_id')->index();
            $t->integer('user_post_id')->index();
            $t->integer('post_id')->index();
            $t->string('photo');
            $t->string('title');
            $t->integer('clicks');
            $t->integer('clicks_calitate');
            $t->float('quality');
            $t->float('earnings', 8, 4);
            $t->float('ecpc', 8, 4);
            $t->float('ecpm', 8, 4);
            $t->timestamp('post_created_at');
            $t->timestamp('user_post_created_at');
            $t->timestamp('reports_created_at');
            $t->timestamps();
            $t->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles_earning');
    }
}
