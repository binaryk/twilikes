<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDomainsAddNameServers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domains', function(Blueprint $t){
            $t->string('nameserver1');
            $t->string('nameserver2');
            $t->string('nameserver3');
            $t->tinyInteger('already_have')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domains', function(Blueprint $t){
            $t->dropColumn(['nameserver1','nameserver2','nameserver3','already_have']);
        });
    }
}
