<?php namespace App\Repositories\Superadmin\Nomenclatoare\Personals;

use App\Models\Access\User\User;

class Form extends \App\Comptechsoft\Form\Form
{

	public function __construct($action, $id)
	{
		parent::__construct($action, $id);
		
		$this->substantiv = 'User';

		$this->captions = [
	        'insert' => 'Adăugare ' . $this->substantiv,
	        'update' => 'Modificare ' . $this->substantiv . ' (#' . $this->id . ')',
	        'delete' => 'Ştergere ' . $this->substantiv,
	    ];

	    if( $id )
	    {
	    	$this->record(User::with('domains')->where('id', ( (int) $id ))->first());

			$this->setParameter('file','<div id="insert"></div>');
	    }

	    $this->view('superadmin.nomenclatoare.personals.form.index');


	}
}