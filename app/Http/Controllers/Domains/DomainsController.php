<?php namespace App\Http\Controllers\Domains;

use App\Http\Controllers\Controller;
use App\Models\Nomenclatoare\Domain;
use App\Models\TldPrice;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Input;

use Helge\Loader\JsonLoader;
use Helge\Client\SimpleWhoisClient;
use Helge\Service\DomainAvailability;
/**
 * Class LanguageController
 * @package App\Http\Controllers
 */
class DomainsController extends Controller
{

    public function index()
    {
        return view('domains.index')->withTitle('Custom domain');
    }

    public function all()
    {
        $domains = access()->user()->domains()->get();

        return success(['domains' => $domains]);
    }

    public function available()
    {
        $domain = str_replace(" ","",Input::get('domain'));

        try{
            $whoisClient = new SimpleWhoisClient();
            $dataLoader = new JsonLoader(public_path('data/servers/servers.json'));
            $service = new DomainAvailability($whoisClient, $dataLoader);
            $status = false;
            if ($service->isAvailable($domain) && Domain::where('title',$domain)->count() == 0)
            {
                $status = true;
            }

            $price = $this->price($domain);
            $msg = 'If do you click on Confirm button, the price of domain name will be deducted from your balance.';
            if(access()->user()->unpaid < $price){
                $msg = 'Sorry, your balance is lower than the price of domain name.';
            }

            if(! $status){
                $msg = 'Sorry, this domain is already in use.';
            }

            $price = number_format($price, 2);

            return success(
                [
                    'status' => $status, 'domain' => $domain, 'price' => $price, 'lowBalance' => access()->user()->unpaid < $price,
                    'userBalance' => access()->user()->unpaid], $msg);
        }
        catch(\Exception $e){
            return error('Sorry, but you can\'t buy this tld extension. Please use one of these com,net,me,org,biz,info,co,cc,mobi');
        }

    }

    public function price($domain)
    {
        $array = explode('.',$domain);
        $tld = end($array);
        $price = TldPrice::where('name',$tld)->first();
        if(! $price){
            throw new \Exception;
        }
        return $price->price;
    }

    public function buy()
    {
        $price = Input::get('price');
        $domain = str_replace(" ","",Input::get('domain'));

        try{
            if(Domain::where('title',$domain)->first()){
                throw new \Exception('Sorry, this domain is already used by another user.');
            }
            $price = $this->price($domain);
            $this->subUnpaid($price);
            $user_domain = Domain::create([
                'user_id' => access()->user()->id,
                'title' => $domain,
                'price' => $price,
                'available' => 0,
            ]);
            return success(['user_domain' => $user_domain], 'Your domain will be available in maximum  24 hours because of name servers propagation.');
        }catch(\Exception $e){
            return error($e->getMessage());
        }
    }

    public function subUnpaid($price)
    {
        if(access()->user()->unpaid < $price){
            throw new \Exception('Sorry, your balance is lower than the price of domain name.');
        }else{
            access()->user()->unpaid -= floatval($price);
            access()->user()->save();
        }

    }

    public function nameservers()
    {
        $data = str_replace(" ","",Input::get('form'));
        $data['already_have'] = '1';
        $data['user_id'] = access()->user()->id;
        $data['price'] = 0;
        $data['available'] = 0;
        $user_domain = Domain::create($data);
        return success(['user_domain' => $user_domain], 'Your domain will be available in maximum  24 hours because of name servers propagation.');
    }
}