<?php namespace App\Repositories\Superadmin\Nomenclatoare\Earnings;

use App\Models\Nomenclatoare\Earning;

class Action extends \App\Comptechsoft\Form\Action
{

	public function __construct($action, $id)
	{
		parent::__construct($action, $id);

		if( $action == 'insert' )
        {
            $record = new Earning();
        }
        else
        {
            $record = Earning::find( (int) $id);
        }

        $this->record($record);

		$this
			->addRule(['insert'], 'country_id', 'unique:earnings', 'Country must by unique')
			->addRule(['insert', 'update'], 'desktop_price', 'required', 'Price is require')
//			->addRule(['insert', 'update'], 'title', 'max:128', 'Numărul maxim de caractere este 128')
		;
	}
}