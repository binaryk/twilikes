<?php namespace App\Repositories\Superadmin\Nomenclatoare\Domains;

use \App\Models\Nomenclatoare\Domain;
use \App\Comptechsoft\Datatable\Cell;

class Rows extends \App\Comptechsoft\Datatable\Rows
{

	public function __construct()
	{
		$this->searcheableColumns([1 => 'id', 2 => 'title', 3 => 'user']);
		$this->orderableColumns([1 => 'id', 2 => 'title', 3 => 'user', 4 => 'available', 5 => 'personal']);

		$this->addCells([
			'rec-no'     => (new Cell())->source(function($record){ return $record->properties->recCount . '.'; }),
			'id'         => (new Cell())->source(function($record){ return $record->id; }),
			'title'      => (new Cell())->source(function($record){ return $record->title; }),
			'user'      => (new Cell())->source(function($record){
				if($record->user_id){

					$user = $record->user()->first();
					return $user->name .', '.$user->email;
				}else{
					return ' - ';
				}
			}),
			'available'      => (new Cell())->source(function($record){ return pingpong($record->available); }),
			'personal'      => (new Cell())->source(function($record){ return pingpong($record->already_have); }),
			'actions'    => (new Cell())->source(function($record){

				return 
					view('__layouts.pages.datatable.cells.actions')
					->withRecord($record)
					->withUpdateRoute(\URL::route('superadmin.nomenclatoare.domains.get-action-form', ['action' => 'update', 'id' => $record->id]))
					->withDeleteRoute(\URL::route('superadmin.nomenclatoare.domains.get-action-form', ['action' => 'delete', 'id' => $record->id]))
					->render();
			}),
		]);
	}

	/*
	 * Toate inregistrarile din tabela ferme 
	 */
	protected function totalRecordsCount()
	{
		return Domain::count();
	}

	/*
	 * Toate inregistrarile obtinute in urma unei filtrari Datatable
	 */
	protected function filteredRecordsCount()
	{
		/*
		 * WhereRaw
		 */
		$where = $this->where();
		if( ! $where )
		{
			return Domain::count();
		}
		return Domain::whereRaw($where)->count();
	}

	/*
	 * Setul de date care trebuie trimis catre Datatable
	 */
	protected function getDataSet()
	{
		/*
		 * OrderBy
		 */
		$order_criteria = $this->orderBy();
		$result = 
			Domain::orderBy( $order_criteria['field'], $order_criteria['direction'] )
			;
		/*
		 * WhereRaw
		 */
		$where = $this->where();
		if( ! $where )
		{
			return $result->skip($this->start())->take($this->length())->get();
		}
		return $result->whereRaw($where)->skip($this->start())->take($this->length())->get();
	}
}