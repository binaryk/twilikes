<?php

Route::group(['namespace' => 'Admin', 'middleware' => 'auth'], function () {

    Route::post('admin-cruds-store', 'SimulatorController@store')
        ->name('admin.cruds.store');
    Route::get('admin-cruds', 'SimulatorController@index')
        ->name('admin.cruds');
});

Route::group(['namespace' => 'Admin', 'middleware' => 'auth'], function () {

    Route::get('admin-message', 'MessageController@index')
        ->name('admin_message.index');

    Route::post('admin-message', 'MessageController@store')
        ->name('admin_message.store');

});