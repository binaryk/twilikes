<?php namespace App\Repositories\Superadmin\Nomenclatoare\VerifyPosts;

use \App\Comptechsoft\Datatable\Column;
use \App\Comptechsoft\Datatable\Header;
use \App\Comptechsoft\Datatable\Ajax;
use \App\Comptechsoft\Datatable\Order;
use \App\Comptechsoft\Datatable\Filter;

class Grid extends \App\Comptechsoft\Datatable\Grid
{

	public function __construct()
	{

		$this->noGlobalFilter();
		/*
		 * II asociez ID la grid
		 */
		$this->id('gridVerifyPosts');

		/*
		 * In ce fisier sa se genereze js-ul pentru .DataTable({...});
		 */
		$this->js( str_replace('\\', '/', public_path('custom/js/verify_posts/grid.js') ));

		/*
		 * De la ce ruta vine raspunsul cu randuri
		 */
		$this->ajax( (new Ajax())->setUrl(\URL::route('superadmin.nomenclatoare.verify_posts.data-source'))->setExtra(['a' => 1, 'b' => 2]) );
		
		/*
		 * Care coloana contine ordinea initiala
		 */
		$this->order( (new Order())->setColumn(1)->setDirection('asc') );

		/*
		 * Definirea coloanelor
		 **/
		$this->addColumns([

			'rec-no' =>
				(new Column())
				->withHeader( 
					(new Header())
					->with(['caption' => '#', 'width' => 5])
				)
				->alignRight(),

			'id' =>
				(new Column())
				->withHeader(
					(new Header())
						->with(['caption' => 'ID', 'width' => 5])
				)
				->alignCenter()
				->orderable(),

			'title' =>
				(new Column())
				->withHeader( 
					(new Header())
					->with(['caption' => 'Title', 'width' => 10])
					->withFilter( (new Filter())->view('superadmin.nomenclatoare.verify_posts.grid.filters.categorie') )
				)->orderable(),

			'category' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Category', 'width' => 5])
				)->orderable(),

			'link' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Photo link', 'width' => 18])
				)->orderable(),

			'username' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'User', 'width' => 18])
				)->orderable(),

			'status' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Status', 'width' => 18])
				)->orderable(),

			'view_for_all' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'View for all', 'width' => 5])
				)->orderable(),

			'actions' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Actions'])
				),


		
		]);

	}
}