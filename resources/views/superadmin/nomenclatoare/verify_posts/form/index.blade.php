<div class="row">
    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Textbox::make()
            ->name( $field = 'location')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Photo (link)' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->location : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => 'http://...',
                'data-field'  => $field,
                'data-type'   => 'textbox'
            ])
            ->help('')
            ->render()
        !!}
    </div>
    @role('Administrator')

    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Textbox::make()
            ->name( $field = 'title')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Title' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->title : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => 'Title...',
                'data-field'  => $field,
                'data-type'   => 'textbox'
            ])
            ->help('')
            ->render()
        !!}
	</div>

    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Textbox::make()
            ->name( $field = 'link')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Link' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->link : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => 'http://...',
                'data-field'  => $field,
                'data-type'   => 'textbox'
            ])
            ->help('')
            ->render()
        !!}
	</div>



    <div class="col-md-12 form-group">
            {!!
            App\Comptechsoft\Ui\Controls\Selectbox::make()
            ->label('Category')
            ->icon('')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->name('category')
            ->value($action != 'insert' ? $record->category : '')
            ->addClass('form-control form-data-source')
            ->withAttribute('data-field', 'category')
            ->withAttribute('data-type', 'combobox')
            ->disabled($action == 'delete')
            ->options(App\Models\Nomenclatoare\Category::category())
            ->render()
            !!}
    </div>

  <div class="row form-group">
      <div class="col-xs-12" style="border-bottom: 1px dashed #eee;">
          {!!
          App\Repositories\Ui\Controls\Editbox::make()
          ->name( $field = 'description')
          ->class('form-group mb')
          ->wrapper('col-sm-10')
          ->lclass('col-sm-2')
          ->caption( 'Descriere' )
          ->placeholder('')
          ->icon(NULL)
          ->value( $action != 'insert' ? $record->{$field} : '')
          ->attributes([
              'id'          => $field,
              'class'       => 'form-control form-data-source  note-editor note-editor-margin',
              'placeholder' => 'Descrierea postului...',
              'data-field'  => $field,
              'data-type'   => 'textbox'
          ])
          ->setFlag( 'readonly', $action == 'delete' )
          ->help('')
          ->render()
      !!}
      </div>
      <div class="col-md-12 form-group">
          {!!
          App\Comptechsoft\Ui\Controls\Selectbox::make()
          ->label('Status')
          ->icon('')
          ->wrapper('col-sm-10')
          ->lclass('col-sm-2')
          ->name('user_confirmation')
          ->value($action != 'insert' ? $record->user_confirmation : '')
          ->addClass('form-control form-data-source')
          ->withAttribute('data-field', 'user_confirmation')
          ->withAttribute('data-type', 'combobox')
          ->disabled($action == 'delete')
          ->options(\App\Models\Nomenclatoare\Post::statuses())
          ->render()
          !!}
      </div>
      <div class="col-md-12 form-group">
          {!!
          App\Comptechsoft\Ui\Controls\Selectbox::make()
          ->label('View for all')
          ->icon('')
          ->wrapper('col-sm-10')
          ->lclass('col-sm-2')
          ->name('view_for_all')
          ->value($action != 'insert' ? $record->view_for_all : '')
          ->addClass('form-control form-data-source')
          ->withAttribute('data-field', 'view_for_all')
          ->withAttribute('data-type', 'combobox')
          ->disabled($action == 'delete')
          ->options(['0' => 'No','1' => 'Yes'])
          ->render()
          !!}
      </div>
      @endauth

      @role('SubAdmin')

      <div class="col-md-12 form-group">
          {!!
          App\Comptechsoft\Ui\Controls\Selectbox::make()
          ->label('Status')
          ->icon('')
          ->wrapper('col-sm-10')
          ->lclass('col-sm-2')
          ->name('user_confirmation')
          ->value($action != 'insert' ? $record->user_confirmation : '')
          ->addClass('form-control form-data-source')
          ->withAttribute('data-field', 'user_confirmation')
          ->withAttribute('data-type', 'combobox')
          ->disabled($action == 'delete')
          ->options(\App\Models\Nomenclatoare\Post::statuses())
          ->render()
          !!}
      </div>
      @endauth
  </div>
</div>