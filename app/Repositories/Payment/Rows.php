<?php namespace App\Repositories\Superadmin\Nomenclatoare\Payment;

use App\Models\Access\User\User;
use \App\Comptechsoft\Datatable\Cell;
use App\Models\Payment;

class Rows extends \App\Comptechsoft\Datatable\Rows
{

	public function __construct()
	{
		$this->searcheableColumns([1 => 'id', 2 => 'title', 3 => 'description']);
		$this->orderableColumns([1 => 'id', 2 => 'title']);

		$this->addCells([
			'rec-no'     => (new Cell())->source(function($record){ return $record->properties->recCount . '.'; }),
			'id'         => (new Cell())->source(function($record){ return $record->id; }),
			'name'      => (new Cell())->source(function($record){ return $record->name; }),
//			'real_unpaid'      => (new Cell())->source(function($record){ return $record->real_unpaid; }),
			'unpaid'      => (new Cell())->source(function($record){ return $record->unpaid; }),
			'status'      => (new Cell())->source(function($record){ return $record->payment_status ? @Payment::status2()[$record->payment_status] : '-'; }),
			'payment_method'      => (new Cell())->source(function($record){ return $record->payment_method ? @Payment::methods()[$record->payment_method]['title'] : '-'; }),
			'actions'    => (new Cell())->source(function($record){

				return 
					view('__layouts.pages.datatable.cells.actions')
					->withRecord($record)
					->withUpdateRoute(\URL::route('superadmin.nomenclatoare.payment.get-action-form', ['action' => 'update', 'id' => $record->id]))
//					->withDeleteRoute(\URL::route('superadmin.nomenclatoare.payment.get-action-form', ['action' => 'delete', 'id' => $record->id]))
					->render();
			}),
		]);
	}

	/*
	 * Toate inregistrarile din tabela ferme 
	 */
	protected function totalRecordsCount()
	{
		return User::count();
	}

	/*
	 * Toate inregistrarile obtinute in urma unei filtrari Datatable
	 */
	protected function filteredRecordsCount()
	{
		/*
		 * WhereRaw
		 */
		$where = $this->where();
		if( ! $where )
		{
			return User::where('unpaid','>=', 20)->count();
		}
		return User::whereRaw($where)->count();
	}

	/*
	 * Setul de date care trebuie trimis catre Datatable
	 */
	protected function getDataSet()
	{
		/*
		 * OrderBy
		 */
		$order_criteria = $this->orderBy();
		$result = 
			User::where('unpaid','>=', 20)->orderBy( $order_criteria['field'], $order_criteria['direction'] );
		/*
		 * WhereRaw
		 */
		$where = $this->where();
		if( ! $where )
		{
			return $result->skip($this->start())->take($this->length())->get();
		}
		return $result->whereRaw($where)->skip($this->start())->take($this->length())->get();
	}
}