<?php namespace App\Repositories\UserPosts;

use \App\Comptechsoft\Datatable\Column;
use \App\Comptechsoft\Datatable\Header;
use \App\Comptechsoft\Datatable\Ajax;
use \App\Comptechsoft\Datatable\Order;
use \App\Comptechsoft\Datatable\Filter;

class Grid extends \App\Comptechsoft\Datatable\Grid
{

	public function __construct()
	{

		$this->noGlobalFilter();
		/*
		 * II asociez ID la grid
		 */
		$this->id('gridUserPosts');

		/*
		 * In ce fisier sa se genereze js-ul pentru .DataTable({...});
		 */
		$this->js( str_replace('\\', '/', public_path('custom/js/user_posts/grid.js') ));

		/*
		 * De la ce ruta vine raspunsul cu randuri
		 */
		$this->ajax( (new Ajax())->setUrl(\URL::route('user_posts.data-source'))->setExtra(['a' => 1, 'b' => 2]) );
		
		/*
		 * Care coloana contine ordinea initiala
		 */
		$this->order( (new Order())->setColumn(1)->setDirection('asc') );

		/*
		 * Definirea coloanelor
		 **/
		$this->addColumns([
 
			'title' =>
				(new Column())
				->withHeader( 
					(new Header())
					->with(['caption' => 'Title', 'width' => 40])
					->withFilter( (new Filter())->view('user_posts.grid.filters.categorie') )
				)->orderable(),

			'category' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Category', 'width' => 17])
				)->orderable(),

			'status' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Status', 'width' => 17])
				)->orderable(),

			'description' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Description', 'width' => 17])
					->withFilter( (new Filter())->view('user_posts.grid.filters.description') )
				)->orderable(),

			'actions' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Actions'])
				),


		
		]);

	}
}