<?php

namespace App\Services\Access\Traits;

use App\Http\Requests\Frontend\User\UpdateFbRequest;
use App\Models\Access\User\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;
use App\Events\Frontend\Auth\UserLoggedIn;
use App\Events\Frontend\Auth\UserLoggedOut;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Http\Requests\Frontend\Auth\LoginRequest;

/**
 * Class AuthenticatesUsers
 * @package App\Services\Access\Traits
 */
trait AuthenticatesUsers
{
    use RedirectsUsers;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm(Request $r)
    {
        if (! starts_with($r->root(), 'http://dashboard.twilikes.com') && env('APP_ENV') !== 'dev')
        {
            return redirect()->to('http://google.com');
        }
        return view('frontend.auth.login')
            ->withSocialiteLinks($this->getSocialLinks());
    }

    /**
     * @param LoginRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function login(LoginRequest $request)
    {
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = in_array(
            ThrottlesLogins::class, class_uses_recursive(get_class($this))
        );

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        if (auth()->attempt($request->only($this->loginUsername(), 'password'), $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return redirect()->back()
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => trans('auth.failed'),
            ]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout()
    {
        /**
         * Remove the socialite session variable if exists
         */
        if (app('session')->has(config('access.socialite_session_name'))) {
            app('session')->forget(config('access.socialite_session_name'));
        }

        event(new UserLoggedOut(access()->user()));
        auth()->logout();
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }

    /**
     * This is here so we can use the default Laravel ThrottlesLogins trait
     *
     * @return string
     */
    public function loginUsername()
    {
        return 'email';
    }

    /**
     * @param Request $request
     * @param $throttles
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        /**
         * Check to see if the users account is confirmed and active
         */
        if (! access()->user()->emailConfirmed === 1) {
            $id = access()->user()->id;
            auth()->logout();
            return trans('exceptions.frontend.auth.confirmation.resend', ['user_id' => $id]);
        } elseif(! access()->user()->isConfirmed()){
            return \Redirect::to('security-check');
        }
        elseif (! access()->user()->isActive()) {
            auth()->logout();
            return trans('exceptions.frontend.auth.deactivated');
        }

        event(new UserLoggedIn(access()->user()));
        $first_login  =  auth()->user()->last_login == NULL;
        auth()->user()->last_login = Carbon::now();
        auth()->user()->save();
        if($first_login){
//            return redirect()->action('\App\Http\Controllers\Frontend\User\DashboardController@index', ['first_login' => $first_login]);
            return redirect()->action('\App\Http\Controllers\App\DashboardController@index', ['first_login' => $first_login]);
        }
        return redirect()->intended($this->redirectPath());
    }

    public function securityCheck(UpdateFbRequest $request)
    {
        $data = $request->all();
        $user_id = $data['user_id'];
        $hash = $data['hash'];
        $user = User::where('id', $user_id)->where('hash_confirm', $hash)->first();
        if($user && $request->hasFile('fbPrintScreen') && $request->file('fbPrintScreen')->isValid()){
            $result = $request->file('fbPrintScreen')->move(public_path('uploads/fbPrintScreens'), str_random(20).'_'.$request->file('fbPrintScreen')->getClientOriginalName());
            $fileLocation = 'uploads/fbPrintScreens/'.$result->getFilename();
            $user->fbPrintScreen = $fileLocation;
            $user->fbPage = $data['fbPage'];
            $user->save();
            return view('frontend.pending-confirmation')->withMessage(trans('exceptions.frontend.auth.confirmation.pendingConfirmation'))->with([ 'hasFbFields' => true,]);
        } else {
            return redirect()->back()->withInput();
        }
    }

    public function showSecurityCheck()
    {
        $id = access()->user()->id;
        $hash = str_random(20);
        $hasFbFileds = access()->user()->fbPage != '' && access()->user()->fbPrintScreen != '';
        access()->user()->hash_confirm = $hash;
        access()->user()->save();
        auth()->logout();
        return view('frontend.pending-confirmation')
            ->withMessage(trans('exceptions.frontend.auth.confirmation.pendingConfirmation'))->with(['user_id' => $id,'hash' => $hash, 'hasFbFields' => $hasFbFileds]);
    }
}
