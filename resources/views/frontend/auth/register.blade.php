@extends('frontend.layouts.master')

@section('content')
    <div class="panel panel-dark panel-flat">
        <div class="panel-heading text-center">
            <a href="#">
                <img src="img/logo.png" alt="Image" class="block-center img-rounded">
            </a>
        </div>
        <div class="panel-body">
            <p class="text-center pv">SIGNUP TO GET INSTANT ACCESS.</p>
            {!! Form::open(['url' => 'register', 'class' => 'mb-lg', 'files' => 'true']) !!}
                <div class="form-group has-feedback">
                    <label for="signupInputEmail1" class="text-muted">Full Name</label>
                    {!! Form::input('name', 'name', null, ['class' => 'form-control', 'placeholder' => 'Full Name']) !!}
                    <span class="fa fa-user form-control-feedback text-muted"></span>
                </div>
                <div class="form-group has-feedback">
                    <label for="signupInputEmail1" class="text-muted">Email address</label>
                    {!! Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email')]) !!}
                    <span class="fa fa-envelope form-control-feedback text-muted"></span>
                </div>
                <div class="form-group has-feedback">
                    <label for="signupInputPassword1" class="text-muted">Password</label>
                    {!! Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password')]) !!}
                    <span class="fa fa-lock form-control-feedback text-muted"></span>
                </div>
                <div class="form-group has-feedback">
                    <label for="signupInputRePassword1" class="text-muted">Retype Password</label>
                    {!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password_confirmation')]) !!}
                    <span class="fa fa-lock form-control-feedback text-muted"></span>
                </div>
                <div class="form-group has-feedback">
                    <label for="fbPage" class="text-muted">Fanpage URL</label>
                    {!! Form::input('fbPage', 'fbPage', null, ['class' => 'form-control', 'placeholder' => 'Fanpage URL']) !!}
                </div>
                <div class="form-group has-feedback">
                     <label for="fbPrintScreen" class="text-muted">Fanpage Activity Log Print Screen</label>
                    {!! Form::file('fbPrintScreen', ['class' => 'form-control']) !!}
                </div>
                <div class="clearfix">
                    <div class="checkbox c-checkbox pull-left mt0">
                        <label>
                            <input type="checkbox" value="" required name="agreed">
                           <span class="fa fa-check"></span>I agree with the <a href="http://twilikes.com/tos.html">terms</a>
                        </label>
                    </div>
                </div>
                <button type="submit" class="btn btn-block btn-primary mt-lg">Create account</button>
            {!! Form::close() !!}
            <p class="pt-lg text-center">Have an account?</p><a href="{!! url('login') !!}" class="btn btn-block btn-default">Login</a>
        </div>
    </div>
@endsection