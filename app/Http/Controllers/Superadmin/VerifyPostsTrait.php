<?php namespace App\Http\Controllers\Superadmin;


use App\Models\Nomenclatoare\Post;
use Carbon\Carbon;
use DateTime;

trait VerifyPostsTrait
{

    public function source($post_id)
    {
        $post = Post::with('items')->where('id',$post_id)->first();


        $counter = 0;
        $text    = $post->title . '<br/>' . $post->description .  '<br/>';
        foreach($post->items as $index => $item){

            $counter++;
            $block = $index % 2 === 0 ? '[adinserter block="8"]' : '';
            $ads   = $index % 2 === 0 ?  '' : '[ads1]';
            $next_page = $index % 2 === 0 ? '' : '<!--nextpage-->';
            $ads2  = '';


            if($post->items->count() === $index + 1) {
                $block  = '';
                $ads    = '[ads1]';
                $next_page = '<!--nextpage-->';
                $ads2   = '[ads2]';
            }


            $text .= view('superadmin.nomenclatoare.verify_posts.source.item')
                ->with([
                    'index' => $index  + 1,
                    'title' => $item->title,
                    'description' => $item->description,
                    'img' => $item->location,
                    'block' => $block,
                    'ads' => $ads,
                    'next_page' => $next_page,
                    'ads2' => $ads2
                ])->render();


        }

        dd($text);
    }

}