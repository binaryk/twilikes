;var gridUsers = function(options)
{
$.extend( this, options);

this.id = 'gridUsers';

this.dt = $('#' + this.id).DataTable({
processing   : true,
serverSide   : true,
stateSave    : false,
autoWidth    : false,
pagingType   : 'bootstrap_full_number',
lengthMenu   : [[5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100], [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100]],
displayStart : 0,
pageLength   : 5,
dom          : '<"row"<"col-xs-12"lp<"comptech-soft-toolbar pull-right">>><"row"<"col-sm-12 galonline-datatable-container"t>>',
ajax         : {
url  : 'http://localhost/twilikes/public/superadmin/nomenclatoare/users-source',
type : 'post',
data : {
_token : 'uvZ4AIDNI7JDeI4Gk1RM6BwcK2sS30yZlVDUYXAr'
}
},
order        : [1, 'asc'],
columns      : [{data : 'rec-no', orderable:false, className:'cell-right'},{data : 'id', orderable:true, className:'cell-center'},{data : 'name', orderable:true, className:''},{data : 'email', orderable:true, className:''},{data : 'confirmed', orderable:true, className:''},{data : 'fbPage', orderable:true, className:''},{data : 'fbPrintScreen', orderable:true, className:''},{data : 'created_at', orderable:true, className:''},{data : 'actions', orderable:false, className:''},],
language     : {
"decimal"		    : "",
"emptyTable"	    : "No data",
"info"			    : "_START_ ... _END_ [_TOTAL_/_MAX_]",
"infoEmpty"		    : "",
"infoFiltered"	    : "",
"infoPostFix"	    : "",
"thousands"		    : ",",
"lengthMenu"	    : "_MENU_", /*  Ã®nregistrÄƒri / paginÄƒ" */
"loadingRecords"    : "Loading...",
"processing"        : "Processing...",
"search"            : "",
"searchPlaceholder" : "Search...",
"zeroRecords"       : "Data not found",
"paginate"          : {
"first"    : 'Prima paginÄƒ',
"previous" : 'Pagina precedentÄƒ',
"next"     : 'Pagina urmÄƒtoare',
"last"     : 'Ultima paginÄƒ'
},
"aria": {
"sortAscending"  : ": activate to sort column ascending",
"sortDescending" : ": activate to sort column descending"
}
},
});


/*
* filtrare cu yadcf
* yadcf.init( this.dt, [{column_number: 0}]);
* am renuntat la yadcf
*/

var _this = this;

/*
* filtrae la nivel de coloana
*/
$(document).on('keyup change', '#gridUsers .datatable-filter-by-column', function(e){
var column = _this.dt.columns( $(this).data('column-index') );
if( $(this).val() != column.search()[0] )
{
var search_expresion = $(this).data('column-search').replace('[%]', $(this).val() );
column.search(search_expresion).draw();
}
});

/*
* Butonul reolad
*/
if($('#reload-gridUsers').length)
{
$('#reload-gridUsers').click( function(e){
_this.dt.ajax.reload( function(json){ console.log(json); }, true);
});
}

/*
* Numarul de inregistrari afisate
*/
if( $('#record-count-gridUsers').length )
{
_this.dt.on('draw.dt', function(event, settings){
$('#record-count-gridUsers').html( _this.dt.page.info().recordsTotal );
});
}

}
/*
End datatable initialization
*/