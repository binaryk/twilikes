<?php namespace App\Models\Nomenclatoare;


class Domain extends \App\Comptech\Model\Model
{


    protected $table = 'domains';
    protected $guarded = ['id'];

    public static function Combobox()
    {
        $result = ['0' => trans('actions.no-selection')];
        $records = self::orderBy('id')->get();
        foreach( $records as $i => $record )
        {
            $result[$record->id] = $record->title;
        }
        return $result;

    }

    public function user()
    {
        return $this->belongsTo(\App\Models\Access\User\User::class, 'user_id');
    }

    public static function available()
    {
        return [
            '0' => 'No',
            '1' => 'Yes',
        ];
    }
}
