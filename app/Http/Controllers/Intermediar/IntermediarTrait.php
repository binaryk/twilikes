<?php namespace App\Http\Controllers\Intermediar;

use App\Models\Country;
use App\Models\EarningsChart;
use App\Models\Nomenclatoare\Earning;
use App\Models\Nomenclatoare\PersonalPrice;
use App\Models\VisitorsChart;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Jenssegers\Agent\Agent;
use App\Models\Access\User\User;
use App\Models\Referral;
use App\Models\Areport;

trait IntemediarTrait
{

    public function updateEarningsChart($user_id, $price, $ip, $post_id, $personal = 0, $quality = 0)
    {
            if($user_id == 38){
//                \Log::debug('Pas 7 :: Chart earnings pt user_id = '.$user_id.' de pe ip: '.$ip);
            }
            // daca deja am tara pe ziua de azi nu o mai fac odata
            $country_id = $this->getLocalCountryId($ip);
            if($earningsChart = EarningsChart::where('country_id', $country_id)->where('user_id',$user_id)->where('data', Carbon::now()->toDateString())->first()){
                $earningsChart->price = floatval($earningsChart->price) + floatval($price);
                $earningsChart->save();
            }else{
                EarningsChart::create([
                    'user_id' => $user_id,
                    'country_id' => $country_id,
                    'price' => floatval($price),
                    'data' => Carbon::now()->toDateString()
                ]);
            }
    }

    public function updateVisitorsChart($user_id, $ip)
    {
        $country_id = $this->getLocalCountryId($ip);
        if($earningsChart = VisitorsChart::where('country_id', $country_id)->where('user_id',$user_id)->where('data', Carbon::now()->toDateString())->first()){
            $earningsChart->visitors = intval($earningsChart->visitors) +  1;
            $earningsChart->save();
        }else {
            VisitorsChart::create([
                'user_id' => $user_id,
                'country_id' => $country_id,
                'data' => Carbon::now()->toDateString(),
                'visitors' => 1
            ]);
        }
    }


    public function incUserAmount($user_id, $amount, $ip, $post_id, $personal = 0,  $quality = 0)
    {
    /*    try{
                $user = User::find($user_id);
                //$user->unpaid = floatval($user->unpaid) + $amount;
                if ($user->unpaid >= 20) {
                    $user->payment_status = "pending";
                }
                $user->save();

                if ($user->invited_by) {
                    $invitedByUser = User::find($user->invited_by);
                    $summ = config('twilikes.referral') * $amount;
                    $summ = floatval($summ);
                    $invitedByUser->unpaid = floatval($invitedByUser->unpaid) + $summ;
                    $invitedByUser->save();
                    Referral::create([
                        'data' => Carbon::now()->toDateString(),
                        'invited_by_id' => $invitedByUser->id,
                        'invited_id' => $user->id,
                        'earning' => $summ
                    ]);
                }


        }catch(\Exception $e){
        }*/
    }

    public function updateReportTable($referrer, $user_id, $post_id, $domain_id, $user_post_id,$request_ip, $price = 0, $quality = 0)
    {
        // aici intra doar o singura data per user
        $agent = new Agent();
        $report =  Areport::create([
            'user_id'       => $user_id,
            'post_id'       => $post_id,
            'domain_id'     => $domain_id,
            'user_post_id'  => $user_post_id,
            'phone'     => $agent->isMobile(),
            'tablet'    => $agent->isTablet(),
            'desktop'   => $agent->isDesktop(),
            'last_click'=> Carbon::now(),
            'visitor_ip'=> $request_ip,
            'quality'   => $quality,
            'price'     => $price,
            'device'    => $this->getCountryId($request_ip),
        ]);

        return $report;
    }
    public function getPrice($ip, $agent, $personal = 0, $user_id = null)
    {
        $location = true;
        $code = "OTH";
        $founded  = null;
//        \Log::info('ip is: '.$ip);
        if(! $code = codecountry($ip) ){
            $code = "OTH";
        }
//        \Log::info("country code: " . $code);
        $price = 0;
        $isDesktop = $agent->isDesktop();
        if($location){
            $country = Country::where('code', strtoupper($code))->first();
            if($personal){
                $price = $this->getPersonalPriceByCountry($country, $isDesktop, $user_id);
            }else{
                $price = $this->getPriceByCountry($country, $isDesktop);
            }
        }
        return $price;
    }

    public function getCountryId($ip)
    {
        $code = "OTH";
        if(! $code = codecountry($ip) ){
            $code = "OTH";
        }

        $country = Country::where('code', strtoupper($code))->first();
        if($country){
            return $country->id;
        }

        return 9999;


        $location = true;
        $founded  = null;
        if(! $code = codecountry($ip) ){
            $code = "OTH";
        }
        $country_id = 0;
        if($location){
            $country = Country::where('code', strtoupper($code))->first();
            $country_id = $country->id;
        }
        return $country_id;
    }

    public function getPersonalPriceByCountry($country, $isDesktop, $user_id)
    {
        if($user_id){
            if($country){
//                \Log::info("country is: ". @$country->country);
                $earnings = PersonalPrice::where('user_id', $user_id)->where('country_id', $country->id)->first();
            }else{
//                \Log::info("country is: Others");
                $earnings = PersonalPrice::where('user_id', $user_id)->where('country_id', 9999)->first();
            }

            if($earnings){
                if($isDesktop){
//                    \Log::info("desktop price is: ".$earnings->desktop_price);
                    return $earnings->desktop_price;
                }else{
//                    \Log::info("mobile price is: ".$earnings->mobile_price);
                    return $earnings->mobile_price;
                }
            }else{
                return 0;
            }
            return 0;
        }

    }

    public function getPriceByCountry($country, $isDesktop)
    {
        if($country){
//            \Log::info("country is: ". @$country->country);
            $earnings = Earning::where('country_id', $country->id)->first();
        }else{
//            \Log::info("country is: Others");
            $earnings = Earning::where('country_id', 9999)->first();
        }

        if($earnings){
            if($isDesktop){
//                \Log::info("desktop price is: ".$earnings->desktop_price);
                return $earnings->desktop_price;
            }else{
//                \Log::info("mobile price is: ".$earnings->mobile_price);
                return $earnings->mobile_price;
            }
        }else{
            return 0;
        }
        return 0;
    }

    public function updateDeviceChart($user)
    {

    }

    public function getLocalCountryId($visitor_ip)
    {
        // returnez id-ul tarii din eargnings (cele adaugate de admin)
        $code = "OTH";
//        \Log::info('chart ip is: '.$visitor_ip);
        if(! $code = codecountry($visitor_ip) ){
            $code = "OTH";
        }
//        \Log::info("chart country code: " . $code);
        $country        = Country::where('code', strtoupper($code))->first();
        $earnings = null;
        if($country){
            $earnings = Earning::where('country_id', $country->id)->first();
        }else{
            $earnings = Earning::where('country_id', 9999)->first();
        }
        if($earnings){
            return $earnings->country_id;
        }
        return 9999;
    }

    public function resizeImage($path, $user_id, $isMobile)
    {
        if($isMobile){
            return $this->mobileResize($path, $user_id);
        }else{
            return $this->desktopResize($path, $user_id);
        }

    }

    public function mobileResize($path, $user_id)
    {
        $img                = Image::make($path);
        $base_name          = $img->basename;
        $destination_folder = public_path('uploads/'.$user_id.'/mini/mobile');
        if(File::exists($destination_folder . '/' . $base_name)){
            return asset('uploads/'.$user_id.'/mini/mobile/'.$base_name);
        }

        $img->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        if( ! File::isDirectory($destination_folder)){
            File::makeDirectory($destination_folder, 0775, true);
        }
        $rest = $img->save($destination_folder.'/'.$base_name);
        $image_location = asset('uploads/'.$user_id.'/mini/mobile/'.$rest->basename);
        return $image_location;
    }

    public function desktopResize($path, $user_id)
    {
        $img = Image::make($path);
        $base_name = $img->basename;
        $destination_folder = public_path('uploads/'.$user_id.'/mini/desktop');

        if( ! File::isDirectory($destination_folder)){
            File::makeDirectory($destination_folder, 0775, true);
        }

        if(File::exists($destination_folder . '/' . $base_name)){
            return asset('uploads/'.$user_id.'/mini/desktop/'.$base_name);
        }

        $img->resize(400, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $rest = $img->save($destination_folder.'/'.$base_name);
        $image_location = asset('uploads/'.$user_id.'/mini/desktop/'.$rest->basename);
        return $image_location;
    }

    public function selfTodayClicked($ip, $post_id,$user_id)
    {

        $selfClicks = Areport::where('visitor_ip',$ip)->where('post_id',$post_id)->where('user_id',$user_id)->orderBy('id','DESC')->first();
        if($selfClicks){
            $lastClickTime = Carbon::createFromFormat('Y-m-d H:i:s',$selfClicks->last_click);
            $diff = Carbon::now()->diffInHours($lastClickTime);

            if( $diff <= 24){
                return $selfClicks;
            }
        }else{
            return false;
        }
        return false;
    }




}