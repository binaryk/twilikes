<?php namespace App\Models;


use App\Models\Nomenclatoare\Post;

class PostView extends \App\Comptech\Model\Model
{


    protected $table = 'posts_views';
    protected $guarded = ['id'];

    public static function Combobox()
    {
        $result = ['0' => trans('actions.no-selection')];
        $records = self::orderBy('id')->get();
        foreach( $records as $i => $record )
        {
            $result[$record->id] = $record->categorie;
        }
        return $result;

    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }

}
