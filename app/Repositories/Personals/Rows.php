<?php namespace App\Repositories\Superadmin\Nomenclatoare\Personals;

use App\Models\Access\User\User;
use \App\Comptechsoft\Datatable\Cell;
use App\Models\Payment;

class Rows extends \App\Comptechsoft\Datatable\Rows
{

	public function __construct()
	{
		$this->searcheableColumns([1 => 'id', 2 => 'name', 3 => 'email']);
		$this->orderableColumns([1 => 'id', 2 => 'name', 3 => 'email',]);

		$this->addCells([
			'rec-no'     => (new Cell())->source(function($record){ return $record->properties->recCount . '.'; }),
			'id'         => (new Cell())->source(function($record){ return $record->id; }),
			'name'      => (new Cell())->source(function($record){ return $record->name; }),
			'email'      => (new Cell())->source(function($record){ return $record->email; }),
			'personal'      => (new Cell())->source(function($record){ return $record->personal_label; }),
			'created_at'      => (new Cell())->source(function($record){ return $record->created_at->toDateTimeString(); }),
			'actions'    => (new Cell())->source(function($record){

				return 
					view('superadmin.nomenclatoare.personals.grid.actions')
					->withRecord($record)
					->withPriceRoute(route('superadmin.nomenclatoare.personalprices.index',['user_id' => $record->id]))
					->withUpdateRoute(\URL::route('superadmin.nomenclatoare.personals.get-action-form', ['action' => 'update', 'id' => $record->id]))
//					->withDeleteRoute(\URL::route('superadmin.nomenclatoare.personals.get-action-form', ['action' => 'delete', 'id' => $record->id]))
					->render();
			}),
		]);
	}

	/*
	 * Toate inregistrarile din tabela ferme 
	 */
	protected function totalRecordsCount()
	{
		return User::count();
	}

	/*
	 * Toate inregistrarile obtinute in urma unei filtrari Datatable
	 */
	protected function filteredRecordsCount()
	{
		/*
		 * WhereRaw
		 */
		$where = $this->where();
		if( ! $where )
		{
			return User::count();
		}
		return User::whereRaw($where)->count();
	}

	/*
	 * Setul de date care trebuie trimis catre Datatable
	 */
	protected function getDataSet()
	{
		/*
		 * OrderBy
		 */
		$order_criteria = $this->orderBy();
		$result = User::orderBy( $order_criteria['field'], $order_criteria['direction'] );
		/*
		 * WhereRaw
		 */
		$where = $this->where();
		if( ! $where )
		{
			return $result->skip($this->start())->take($this->length())->get();
		}
		return $result->whereRaw($where)->skip($this->start())->take($this->length())->get();
	}
}