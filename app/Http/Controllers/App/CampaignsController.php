<?php namespace App\Http\Controllers\App;

use App\Comptech\Datatable\Rows\Collection\Collection;
use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use App\Models\Nomenclatoare\Category;
use App\Models\Nomenclatoare\Post;
use App\Models\UserPost;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Jenssegers\Agent\Agent;
use PragmaRX\Tracker\Tracker;


/**
 * Class LanguageController
 * @package App\Http\Controllers
 */
class CampaignsController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        return view('campaigns.index')->withTitle('Campaigns')->withPosts($posts)->withLink('http://localhost/twilikes/public/client/lejRe/mbk5e/lejRe/en5Rd');
    }

    public function apiAll()
    {
        $agent = new Agent();
        $model = Input::get('filter');
        $option = Input::get('option');
        $LIMIT = 18;
        $data = [];



        if($model){
            switch($model){
                case 'latest':
                    if($option){
                        $products = Post::where('user_id',0)
                            ->where('havent_image',0)
                            ->orWhere('user_id','!=',0)->where('user_confirmation',1)->where('view_for_all',1)
                            ->where('category',$option)->orderBy('created_at','DESC')->paginate($LIMIT);
                    }else{
                        $products = Post::where('user_id',0)
                            ->where('havent_image',0)
                            ->orWhere('user_id','!=',0)->where('user_confirmation',1)->where('view_for_all',1)
                            ->orderBy('created_at','DESC')->paginate($LIMIT);
                    }
                    $data = $this->posted($products->toArray());
                    break;
                case 'oldest':
                    if($option){
                        $products = Post::where('user_id',0)
                            ->where('havent_image',0)
                            ->orWhere('user_id','!=',0)->where('user_confirmation',1)->where('view_for_all',1)
                            ->where('category',$option)->orderBy('id','ASC')->paginate($LIMIT);
                    }else{
                        $products = Post::where('user_id',0)
                            ->where('havent_image',0)
                            ->orWhere('user_id','!=',0)->where('user_confirmation',1)->where('view_for_all',1)
                            ->orderBy('id','ASC')->paginate($LIMIT);
                    }
                    $data = $this->posted($products->toArray());
                    break;
                case 'all':
                    $products =
                        $option ? Post::where('user_id',0)
                            ->where('havent_image',0)
                            ->orWhere('user_id','!=',0)->where('user_confirmation',1)->where('view_for_all',1)
                            ->where('category',$option)->orderBy('id','DESC')->paginate($LIMIT) :
                        Post::where('user_id',0)
                            ->where('havent_image',0)
                            ->orWhere('user_id','!=',0)->where('user_confirmation',1)->where('view_for_all',1)
                            ->orderBy('id','DESC')->paginate($LIMIT);
                    $random   = $this->random($products->toArray());
                    $data = $this->posted($random);
                break;

                case 'ready':
                    $data = access()->user()->posts()->whereNull('reset')->where('havent_image', 0)->groupBy('post_id')->paginate($LIMIT);
                break;
                case 'not_posted':
                    $ids = DB::table('user_posts')->where('user_id', access()->user()->id)->whereNull('reset')->lists('post_id');
                    $data = Post::where('user_id',0)
                        ->where('havent_image',0)
                        ->orWhere('user_id','!=',0)->where('user_confirmation',1)->where('view_for_all',1)
                        ->whereNotIn('id',$ids)->orderBy('id','DESC')->groupBy('id')->paginate($LIMIT);
                break;
                case 'popular':
                    $yesterday = Carbon::yesterday()->toDateString();
                    $products = Post::leftJoin('posts_views','posts.id','=','posts_views.post_id')
                        ->select(DB::raw('posts.*, posts_views.date, posts_views.views'))
//                        ->where('posts.user_id',0)
//                        ->orWhere('posts.user_id','!=',0)->where('posts.user_confirmation',1)->where('posts.view_for_all',1)
                        ->where('posts_views.date','=',$yesterday)
                        ->where('posts_views.quality',0)
                        ->where('posts.havent_image',0)
                        ->orderBy('posts_views.views','DESC')
                        ->paginate($LIMIT);
                    $data = $products;
                    break;
                case 'custom':
                    $substring = Input::get('substring');
                    $data = Post::where('title','like',$substring.'%')
                        ->where('havent_image',0)
//                        ->where('user_id',0)
//                        ->orWhere('user_id','!=',0)->where('user_confirmation',1)->where('view_for_all',1)
                        ->paginate($LIMIT);
                    $data = $this->posted($data->toArray());
                    break;
                case 'personal':
                    if($option){
                        $products = Post::where('user_id','=',auth()->user()->id)->where('havent_image', 0)->where('user_confirmation',1)->where('category',$option)->orderBy('id','DESC')->paginate($LIMIT);
                    }else{
                        $products = Post::where('user_id','=',auth()->user()->id)->where('havent_image', 0)->where('user_confirmation',1)->orderBy('id','DESC')->paginate($LIMIT);
                    }
                    $data = $this->posted($products->toArray());
                    break;;
                default:
                    $data = Post::where('title','like',$model.'%')->get();
            }
        }
        return response()->json([
                'code' => 200,
                'msg' => '',
                'data' => $data,
                'isMobile' => $agent->isMobile(),
        ]);
//        return success(['data' => $data]);
    }

    public function categories()
    {
        return Category::all();
    }

    public function posted($data)
    {
        $all_ids = UserPost::where('user_id',access()->user()->id)->whereNull('reset')->lists('post_id');

        foreach($data['data'] as $key => $post){
//            $node = UserPost::where('user_id',access()->user()->id)->where('post_id',$post['id'])->whereNull('reset')->first();
            $no_exists = $all_ids->filter(function($item) use ($post) {
                return $item === $post['id'];
            })->isEmpty();

            if(! $no_exists){
                $post['posted'] = 1;
            }else{
                $post['posted'] = 0;
            }
            $data['data'][$key] = $post;
        }

        return $data;
    }

    public function random($data, $rand = false)
    {
        $elements = $data['data'];
        if($rand) {
            shuffle($elements);
        }
        $data['data'] = $elements;
        return $data;
    }

    public function reset()
    {
        UserPost::where('user_id', auth()->user()->id)
            ->update([
            'reset' => Carbon::now(),
        ]);
        return success([]);
    }

}