module App{
    declare var $;
    declare var Spinner;
    export class File{
        protected imgBox;
        protected oldBox;
        protected n404Box;
        protected fileExists;
        constructor(public impact, public node, public component = null){
            this.imgBox = $('#image-upload-box');
            this.oldBox = $('#image-old-box');
            this.n404Box = $('#file_not_found');
            this.fileExists = (this.n404Box.length === 0);

        }

        init(){
            this[this.impact]();
        }

        insert(){
            this.imgBox.removeClass('hidden');
            this.oldBox.addClass('hidden');
            this.initFileUpload();
        }

        initFileUpload(){
            var file = this.node.fileinput({
                'dropZoneEnabled' : true,
                'showCaption'     : false,
                'showUpload'      : false,
                'showRemove'      : false,
                'showCancel'      : false,
                'showClose'       : false,
                'captionClass'     : 'caption-class',
                'allowedFileTypes'     : ['image'],
                'allowedFileExtensions'     : ['jpg','gif','png','jpeg'],
                'fileTypeSettings': {
                    image: function(vType, vName) {
                        return (typeof vType !== "undefined") ? vType.match('image.*') : vName.match(/\.(gif|png|jpe?g)$/i);
                    }
                },
                maxFileCount: 1,
                validateInitialCount: true,
                uploadUrl: "api/apiUploadPhotoFrom",
                uploadAsync: true,
                uploadExtraData: function(){
                    if(this.component && this.component.extraData){
                        return this.component.extraData();
                    } else {
                        return {};
                    }
                }.bind(this),
                previewSettings: {
                    // image: { width: "50px", height: "auto" },
                    // html: { width: "50px", height: "auto" },
                    // other: { width: "50px", height: "auto" }
                }
            });

            this.node.on('fileimageloaded', this.onFileImageLoaded.bind(this));
            this.node.on('change', this.onChange);
            this.node.on('fileerror', this.fileError.bind(this));
            this.node.on('fileuploaded', this.fileUploaded.bind(this)); 
            this.node.on('filereset', this.fileReset.bind(this));
            this.node.on('filepredelete', function(event, key) {
                console.log('Key = ' + key);
            });
            this.node.on('filedeleted', function(event, key) {
                console.log('Key = ' + key);
            });
            // this.node.on('filebatchpreupload', this.onFileBatchPreUpload.bind(this))
            // this.node.on('filepreajax', this.onFilePreAjax.bind(this))
        }

        onChange(){

        }

        fileError(event, data, msg){
            this.component.onFileLoadError(event, data, msg);
        }

        onFileImageLoaded(event, previewId){
            this.component.onImageLoaded(event, previewId, this.node);
        }

        onFileBatchPreUpload(event, data, previewId, index){
            console.log(1);
            this.component.fileBatchPreUpload(event, data, previewId, index, this.node);
        }

        onFilePreAjax(event, previewId, index){
            console.log('pre ajax', index);
        }

        fileUploaded(event, data, previewId, index){
            this.component.fileUploaded(event, data, previewId, index);
        }

        fileReset(event){
            this.component.fileReset(event);
        }

    }
}