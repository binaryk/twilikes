Vue.component('post', {
    template: '#user-post',
    data: function(){
      return {
            regex: (new RegExp("^(https?|ftp)://.*(jpeg|png|gif|bmp|jpg)")),
            existsUploadedPhoto : false,
            linkIsValid:          undefined,
            validationIsActivated: false,
        }
    },
    ready: function(){
        this.initUpload()
    },

    created: function(){
        this.item.isValid = this.isValid.bind(this);
    },

    props: ['item', 'post', 'delete', 'index'],

    methods: {
        remove: function (item) {
            this.delete(item)
        },


        initUpload: function () {
            this.upload = new App.File('insert', $(this.$el).find('input[type=file]'), this);
            this.upload.init()
        },
        external: function () {
            var $container;
            if( this.regex.test(this.item.link) ) {
                this.linkIsValid = true;
                this.$nextTick(function(){
                    $container = $(this.$el).find('.uploaded-image');
                    $container.css('background-image', 'url('+ this.item.link + ')');
                    this.item.photo = true;
                    this.item.location = this.item.link;
                    this.uploadAsyncImageFromUrl(this.item.link);
                }.bind(this));

            } else {
                this.linkIsValid = false;
            }
        },
        onImageLoaded: function(event, previewId, node){
            node.fileinput('upload');
            this.existsUploadedPhoto = true;
            $(this.$el).find('.btn-file').hide();
        },
        onFileLoadError: function (event, data, msg) {
            this.existsUploadedPhoto = false;
        },

        uploadAsyncImageFromUrl: function(url){
            $.post('api/apiUploadPhotoFromUrl', {url: url, post_id: this.post.id, item_id: this.item.id}, function(data){
                this.post.id = data.post.id;
                this.item.id = data.item.id;
                this.item.photo = true;
            }.bind(this));
        },

        fileUploaded: function(event, data, previewId, index){
            this.post.id = data.response.post.id;
            this.item.id = data.response.item.id;
            this.item.photo = true;
        },

        fileReset: function(event) {
            this.existsUploadedPhoto = false;
            $(this.$el).find('.btn-file').show();
        },

        extraData: function () {
            return {
                post_id: this.post.id,
                item_id: this.item.id,
                forPost: false
            }
        },

        isValid : function() {
            /*
            * does have title, description and image
            * */
            this.validationIsActivated = true;
            if(! this.item.photo) {
                return false;
            }

            if(this.item.title.length === 0) {
                return false;
            }

            // if(this.item.description.length === 0) {
            //     return false;
            // }

            if(! this.item.photo) {
                return false;
            }

            return true;
        }
    }
});


new Vue({
    el: 'form#gridUserPosts',
    data: function(){
        return {
            items: [
                {
                    id: -1,
                    post_id: -1,
                    title: '',
                    description: '',
                    photo: false,
                    link: '',
                    canRemove: false,
                    isValid: null
                },
               {
                    id: -1,
                    post_id: -1,
                    title: '',
                    description: '',
                    photo: false,
                    link: '',
                    canRemove: false,
                    isValid: null
                },
                {
                    id: -1,
                    post_id: -1,
                    title: '',
                    description: '',
                    photo: false,
                    link: '',
                    canRemove: false,
                    isValid: null
                },
                {
                    id: -1,
                    post_id: -1,
                    title: '',
                    description: '',
                    photo: false,
                    link: '',
                    canRemove: false,
                    isValid: null
                }
            ],
            post: {
                id: -1,
                title: '',
                description: '',
                photo: false,
                category: 0,
                link: '',
                location: ''
            },
            validationIsActivated: false,
            existsUploadedPhoto: false,
            regex: (new RegExp("^(https?|ftp)://.*(jpeg|png|gif|bmp|jpg)")),
            linkIsValid:          undefined
        }
    },
    ready: function () {
        currentForm.storeCb = this.store; //new Promise( this.store );
        currentForm.cancelCb = this.cancel; //new Promise( this.store );
        this.initPostPhotoUploadForm();

    },


    methods: {
        create: function () {
          this.items.push({
              id: -1,
              post_id: -1,
              title: '',
              description: '',
              photo: false,
              link: '',
              canRemove: true,
              isValid: null
          });
        },
        store : function(){
            const that = this;
            this.post.items = this.items;
            this.validationIsActivated = true;
                return new Promise(function(resolve, reject) {
                    if(that.validateForm()) {
                        $.ajax({
                            url      : 'my-articles-action/insert',
                            type     : 'POST',
                            data     :   {
                                post: that.post
                            },
                            success  : function( result )
                            {
                                if(result.success) {
                                    resolve(result);
                                } else {
                                    reject(result);
                                }
                            }
                        });
                    } else {
                        reject({ success : false });
                    }
                });
        },
        cancel : function(){
            const that = this;
            this.post.items = this.items;
            return new Promise(function(resolve, reject) {
                $.ajax({
                    url      : 'my-articles-action/clear',
                    type     : 'POST',
                    data     :   {
                        post: that.post
                    },
                    success  : function( result )
                    {
                        if(result.success) {
                            resolve(result);
                        } else {
                            reject(result);
                        }
                    }
                });
            });
        },

        external: function () {
            var $container;

            if( this.regex.test(this.post.link) ) {
                this.linkIsValid = true;
                this.$nextTick(function(){
                    $container = $(this.$el).find('#image-upload-box .uploaded-image');
                    $container.css('background-image', 'url('+ this.post.link + ')');
                    this.post.location = this.post.link;
                    this.uploadAsyncImageFromUrl(this.post.link);
                }.bind(this));

            } else {
                this.linkIsValid = false;
            }
        },

        typeLink: function(event) {

        },

        delete : function(item) {
            const index = this.items.indexOf(item);
            this.items.splice(index, 1);
        },

        initPostPhotoUploadForm: function(){
            var file = new App.File('insert', $('#file'), this);
            file.init();
        },

        onImageLoaded: function(event, previewId, node){
            node.fileinput('upload');
            this.existsUploadedPhoto = true;
            $(this.$el).find('#image-upload-box .btn-file').hide();
        },

        fileUploaded: function(event, data, previewId, index){
            this.post.photo = true;
            this.post.id = data.response.post.id;
        },

        extraData: function () {
            return {
                post_id: this.post.id,
                forPost: true

            }
        },

        uploadAsyncImageFromUrl: function(url){
            $.post('api/apiUploadPhotoFromUrl', {url: url, post_id: this.post.id}, function(data){
                this.post.id = data.post.id;
            }.bind(this));
        },

        validateForm : function() {
            var isValid = true;
            this.items.forEach( function(el){

                if(! el.isValid()){
                    isValid = false;
                }
            });

            return isValid;
        }
    }
});