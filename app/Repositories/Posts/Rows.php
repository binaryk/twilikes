<?php namespace App\Repositories\Superadmin\Nomenclatoare\Posts;

use \App\Models\Nomenclatoare\Post;
use \App\Comptechsoft\Datatable\Cell;

class Rows extends \App\Comptechsoft\Datatable\Rows
{

	public function __construct()
	{
		$this->searcheableColumns([1 => 'id', 2 => 'title', 3 => 'description']);
		$this->orderableColumns([1 => 'id', 2 => 'title']);

		$this->addCells([
			'rec-no'     => (new Cell())->source(function($record){ return $record->properties->recCount . '.'; }),
			'id'         => (new Cell())->source(function($record){ return $record->id; }),
			'title'      => (new Cell())->source(function($record){ return $record->title; }),
			'description'=> (new Cell())->source(function($record){ return $record->description; }),
			'link'=> (new Cell())->source(function($record){ return $record->link; }),
			'category'=> (new Cell())->source(function($record){ return @\App\Models\Nomenclatoare\Category::category()[$record->category]; }),
			'location'   => (new Cell())->source(function($record){ return view('superadmin.nomenclatoare.posts.grid.filters.location')->with(['name' => $record->img_name,'location' => $record->location])->render(); }),
			'actions'    => (new Cell())->source(function($record){

				return 
					view('__layouts.pages.datatable.cells.actions')
					->withRecord($record)
					->withUpdateRoute(\URL::route('superadmin.nomenclatoare.posts.get-action-form', ['action' => 'update', 'id' => $record->id]))
					->withDeleteRoute(\URL::route('superadmin.nomenclatoare.posts.get-action-form', ['action' => 'delete', 'id' => $record->id]))
					->render();
			}),
		]);
	}

	/*
	 * Toate inregistrarile din tabela ferme 
	 */
	protected function totalRecordsCount()
	{
		return Post::count();
	}

	/*
	 * Toate inregistrarile obtinute in urma unei filtrari Datatable
	 */
	protected function filteredRecordsCount()
	{
		/*
		 * WhereRaw
		 */
		$where = $this->where();
		if( ! $where )
		{
			return Post::count();
		}
		return Post::whereRaw($where)->count();
	}

	/*
	 * Setul de date care trebuie trimis catre Datatable
	 */
	protected function getDataSet()
	{
		/*
		 * OrderBy
		 */
		$order_criteria = $this->orderBy();
		$result = 
			Post::where('user_id',0)->orderBy( $order_criteria['field'], $order_criteria['direction'] )
			;
		/*
		 * WhereRaw
		 */
		$where = $this->where();
		if( ! $where )
		{
			return $result->skip($this->start())->take($this->length())->get();
		}
		return $result->whereRaw($where)->skip($this->start())->take($this->length())->get();
	}
}