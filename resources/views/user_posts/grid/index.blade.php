@extends('__layouts.pages.datatable.index')
@section('pretable')
	<div class="alert alert-info">
		Approved articles can be found on "Campaigns > My Campaigns"
	</div>
@endsection
@section('css')
	@parent
	<link rel="stylesheet" href="{!! asset('vendor\file-input\css\fileinput.min.css') !!}">
	<link rel="stylesheet" href="{!! asset('custom/css/file-icons.css') !!}">
	<link rel="stylesheet" href="{!! asset('custom/css/user-post.css') !!}">
	<style>

	</style>
	@stop

@section('custom-javascript-files')
	@parent
	<!-- includ js-ul pentru datatable creat dinamic -->
	{!! 
	App\Comptechsoft\Ui\Html\Scripts::make([
		'custom/js/user_posts/grid',
		'custom/js/user_posts/File',
		'custom/js/user_posts/form'
	])->render()
	!!}

	{!! HTML::script('vendor/file-input/js/fileinput.min.js') !!}
@stop

@section('jquery-document-ready')
	@parent
	var grid  = new gridUserPosts();
	window.currentForm = new indexGrid({
		grid       : grid,
		toolbar    : '{!! $toolbar !!}',
		_token     : '{{ csrf_token() }}',
		form_width : 12,
	}).init();


@stop
