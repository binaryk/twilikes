<?php namespace App\Models;

use App\Comptech\Model\Model;
use Carbon\Carbon;
class Message extends Model
{
    protected $table = "default_messages";
    protected $guarded = ['_token','id'];

}