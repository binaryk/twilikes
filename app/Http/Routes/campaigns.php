<?php
Route::group(['namespace' => 'App', 'middleware' => 'auth'], function () {
    Route::get('campaigns-index/{filter?}', 'CampaignsController@index')
        ->name('campaigns.index');

    Route::get('api/campaigns', 'CampaignsController@apiAll')
        ->name('api.campaigns.index');

    Route::get('api/categories', 'CampaignsController@categories')
        ->name('api.categories.index');

    Route::post('api/campaigns-reset', 'CampaignsController@reset')
        ->name('api.categories.reset');

//    Route::get('campaigns-index/{filter?}', 'CampaignsController@apiAll')
//        ->name('campaigns.show');
});