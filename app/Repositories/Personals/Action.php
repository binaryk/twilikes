<?php namespace App\Repositories\Superadmin\Nomenclatoare\Personals;

use App\Models\Access\User\User;

class Action extends \App\Comptechsoft\Form\Action
{

	public function __construct($action, $id)
	{
		parent::__construct($action, $id);

		if( $action == 'insert' )
        {
            $record = new User();
        }
        else
        {
            $record = User::find( (int) $id);
        }

        $this->record($record);

		$this
//			->addRule(['insert', 'update'], 'title', 'required', 'Titlul trebuie completat')
//			->addRule(['insert', 'update'], 'title', 'max:128', 'Numărul maxim de caractere este 128')
		;
	}
}