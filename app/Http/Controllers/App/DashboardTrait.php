<?php
/**
 * Created by PhpStorm.
 * User: lupac
 * Date: 5/20/2016
 * Time: 5:39 PM
 */

namespace App\Http\Controllers\App;

use App\Models\Country;
use App\Models\EarningsChart;
use App\Models\Nomenclatoare\Earning;
use App\Models\VisitorsChart;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Input;
use Jenssegers\Agent\Agent;
use Stevebauman\Location\Facades\Location;
use Illuminate\Support\Facades\DB;

trait DashboardTrait
{

    public function getEarningsChart()
    {
        $localStorage = Input::get('localStorage') === "false" ? null : Input::get('localStorage');
        if(! config('app.localStorage') || true) {
            return success(['days' => $this->getWeekStrings(), 'data' => $this->getEarningsDataOnly(), 'serverTime' => Carbon::now()->toDateTimeString(), 'refresh' => true, 'user' => access()->user()->id ]);
        }

        if( ! $localStorage || ! $localStorage['timestamp'] || ! array_keys_exists(['data', 'user'], $localStorage) || access()->user()->id != $localStorage['user'] ) {
            return success(['days' => $this->getWeekStrings(), 'data' => $this->getEarningsDataOnly(), 'serverTime' => Carbon::now()->toDateTimeString(), 'refresh' => true, 'user' => access()->user()->id ]);
        }

        $serverTimeFromStorage = Carbon::createFromFormat('Y-m-d H:i:s', $localStorage['timestamp']);
        $anotherDay = Carbon::now()->diffInDays( $serverTimeFromStorage )  > 0;

        if( $anotherDay ) {
            return success(['days' => $this->getWeekStrings(), 'data' => $this->getEarningsDataOnly(), 'serverTime' => Carbon::now()->toDateTimeString(), 'refresh' => true, 'user' => access()->user()->id ]);
        } else {
            // aceeasi zi, returnez doar ziua de azi din storage
            $dataToReturnWithActualDay = $localStorage['data'];

            $todayEarnings = $this->getEarningsDataOnly('today');
            foreach ($localStorage['data'] as $key =>$state) {
                $dataToReturnWithActualDay[$key]['data'][7] = $todayEarnings[$key]['data'][7];
            }

            foreach($localStorage['data'] as $k => $state) {
                foreach($state['data'] as $key => $stat) {
                    $state['data'][$key] = floatval($stat);
                }

                $dataToReturnWithActualDay[$k] = $state;
            }

            return success(['days' => $this->getWeekStrings(), 'data' => $dataToReturnWithActualDay, 'serverTime' => Carbon::now()->toDateTimeString(), 'refresh' => false, 'user' => access()->user()->id ]);
        }

    }

    public function getEarningsDataOnly($filter = null)
    {
        $todayDate = Carbon::now()->subDay();
        $weekBack = Carbon::now()->subWeek();
        $data = EarningsChart::whereBetween('earnings_chart.data',[$weekBack, $todayDate])
            ->where('user_id', access()->user()->id)
            ->orderBy('data','ASC')
            ->get();
        $states = [
            '0' => [
                'name' => 'Australia',
                'data' => $this->getEmptyArray(),
            ],
            '1' => [
                'name' => 'Canada',
                'data' => $this->getEmptyArray(),
            ],
            '2' => [
                'name' => 'United Kingdom',
                'data' => $this->getEmptyArray(),
            ],
            '3' => [
                'name' => 'United States',
                'data' => $this->getEmptyArray(),
            ],
            '4' => [
                'name' => 'India',
                'data' => $this->getEmptyArray(),
            ],
            '5' => [
                'name' => 'Others',
                'data' => $this->getEmptyArray(),
            ],
        ];

        foreach($data as $k => $chart){
            $index = Carbon::createFromFormat('Y-m-d', $chart->data)->diffInDays($weekBack);
            if(array_key_exists($chart->country_id - 1, $states)) {
                $states[$chart->country_id - 1]['data'][$index] += normalFloat($chart->price, 4);
            } else {
                $states['5']['data'][$index] += normalFloat($chart->price, 4);
            }
        }

        foreach($this->getTodayEarningsChartData() as $k => $today) {
            $states[$k]['data'][7] = $today['data'][7];
        }
        return $states;
    }

    public function getTodayEarningsChartData()
    {

        $states = [
            '0' => [
                'name' => 'Australia',
                'data' => $this->getEmptyArray(),
            ],
            '1' => [
                'name' => 'Canada',
                'data' => $this->getEmptyArray(),
            ],
            '2' => [
                'name' => 'United Kingdom',
                'data' => $this->getEmptyArray(),
            ],
            '3' => [
                'name' => 'United States',
                'data' => $this->getEmptyArray(),
            ],
            '4' => [
                'name' => 'India',
                'data' => $this->getEmptyArray(),
            ],
            '5' => [
                'name' => 'Others',
                'data' => $this->getEmptyArray(),
            ],
        ];
        $data = DB::table('areports')
            ->select(DB::raw('
                sum(price) as price,
                device as country_id,
                areports.created_at,
                DATE_FORMAT(areports.created_at, "%Y-%m-%d") as data,
                DATE_FORMAT(areports.created_at, "%m-%d-%Y") as date
            '))
            ->whereDate('created_at','=',Carbon::today())
            ->where('user_id', '=', access()->user()->id)
            ->groupBy('country_id')
            ->groupBy('date')
            ->orderBy('date')
            ->get();

        foreach($data as $k => $chart){
            $index = 7;
            if(array_key_exists($chart->country_id - 1, $states)) {
                $states[$chart->country_id - 1]['data'][$index] += normalFloat($chart->price, 4);
            } else {
                $states['5']['data'][$index] += normalFloat($chart->price, 4);
            }
        }

        return $states;
    }


    public function getEmptyArray($len = 7)
    {
        return [
            '0' => 0,
            '1' => 0,
            '2' => 0,
            '3' => 0,
            '4' => 0,
            '5' => 0,
            '6' => 0,
            '7' => 0,
        ];
    }

    public function getEarningsDataOnlyOld($filter = null)
    {
        $australia = [
            'name' => 'Australia',
            'data' => [
                '0' => 0,
                '1' => 0,
                '2' => 0,
                '3' => 0,
                '4' => 0,
                '5' => 0,
                '6' => 0,
                '7' => 0,
            ],
        ];
        $canada = [
            'name' => 'Canada',
            'data' => [
                '0' => 0,
                '1' => 0,
                '2' => 0,
                '3' => 0,
                '4' => 0,
                '5' => 0,
                '6' => 0,
                '7' => 0,
            ],
        ];
        $uk = [
            'name' => 'United Kingdom',
            'data' => [
                '0' => 0,
                '1' => 0,
                '2' => 0,
                '3' => 0,
                '4' => 0,
                '5' => 0,
                '6' => 0,
                '7' => 0,
            ],
        ];
        $us = [
            'name' => 'United States',
            'data' => [
                '0' => 0,
                '1' => 0,
                '2' => 0,
                '3' => 0,
                '4' => 0,
                '5' => 0,
                '6' => 0,
                '7' => 0,
            ],
        ];
        $in = [
            'name' => 'India',
            'data' => [
                '0' => 0,
                '1' => 0,
                '2' => 0,
                '3' => 0,
                '4' => 0,
                '5' => 0,
                '6' => 0,
                '7' => 0,
            ],
        ];
        $oters = [
            'name' => 'Others',
            'data' => [
                '0' => 0,
                '1' => 0,
                '2' => 0,
                '3' => 0,
                '4' => 0,
                '5' => 0,
                '6' => 0,
                '7' => 0,
            ],
        ];


        $todayDate = Carbon::now();
        switch ($filter) {
            case 'today' :
                $weekBack = Carbon::createFromFormat('Y-m-d H:i:s',$todayDate->year.'-'.$todayDate->month .'-'.$todayDate->day.' '.'00:00:00');
                break;
            default:
                $weekBack  = Carbon::now()->subWeek();
        }
        /*     $weekChart = EarningsChart::where('user_id', access()->user()->id)
                 ->whereDate('data', '>=',$weekBack->toDateString())
                 ->whereDate('data', '<=',$todayDate->toDateString())
                 ->orderBy('id')
                 ->get();*/

        $weekChart = DB::table('areports')
            ->select(DB::raw('
                sum(price) as price,
                device as country_id,
                areports.created_at,
                DATE_FORMAT(areports.created_at, "%Y-%m-%d") as data,
                DATE_FORMAT(areports.created_at, "%m-%d-%Y") as date
            '))
            ->where('user_id', '=', access()->user()->id)
//            ->whereDate('areports.created_at','>=',$weekBack)
//            ->whereDate('areports.created_at','<=',$todayDate)
            ->whereBetween('areports.created_at',[$weekBack, $todayDate])
            ->groupBy('country_id')
            ->groupBy('date')
            ->orderBy('date')
            ->get();


        foreach($weekChart as $k => $chart){

            $index = Carbon::createFromFormat('Y-m-d', $chart->data)->diffInDays($weekBack);
            switch($chart->country_id){
                case 1:
                    $australia['data'][$filter === 'today' ? 7 : $index] += normalFloat($chart->price, 4);
                    break;
                case 2:
                    $canada['data'][$filter === 'today' ? 7 : $index] += normalFloat($chart->price, 4);
                    break;
                case 3:
                    $uk['data'][$filter === 'today' ? 7 : $index] += normalFloat($chart->price, 4);
                    break;
                case 4:
                    $us['data'][$filter === 'today' ? 7 : $index] += normalFloat($chart->price, 4);
                    break;
                case 5:
                    $in['data'][$filter === 'today' ? 7 : $index] += normalFloat($chart->price, 4);
                    break;
                case 9999:
                    $oters['data'][$filter === 'today' ? 7 : $index] += normalFloat($chart->price, 4);
                    break;
                default:
                    $oters['data'][$filter === 'today' ? 7 : $index] += normalFloat($chart->price, 4);

            }
        }


        $states = [
            '0' => $australia,
            '1' => $canada,
            '2' => $uk,
            '3' => $us,
            '4' => $in,
            '5' => $oters,
        ];

        return $states;
    }


    public function getWeekStrings()
    {
        $todayDate = Carbon::now();
        $weekBack  = Carbon::now()->subWeek();

        $weekStrings = [];

        $diff = $todayDate->diffInDays($weekBack) + 1 ;

        if($diff > 0){

            for($i = 0; $i < $diff; $i++){
                $weekBack = new Carbon($weekBack);
                $weekBack->toDateString();

                $string = $weekBack->day . ' ' . $this->sampleMonthName($weekBack->month);
                $weekStrings[] = $string;




                $weekBack->addDay();
            }

        }

        return $weekStrings;
    }


    public function apiTodayVisitorTrait()
    {
        $dayVisitors = VisitorsChart::with('countries')->where('user_id', access()->user()->id)->whereDate('data','=',Carbon::now()->toDateString())->get();

        $data = [
            '1' => 0,
            '2' => 0,
            '3' => 0,
            '4' => 0,
            '5' => 0,
            '9999' => 0,
        ];
        $total = 0;
        foreach($dayVisitors as $k => $visitor){
            if($visitor->countries && $visitor->countries->fake == 1){
                $data[9999] += $visitor->visitors;
            }else{
                $data[$visitor->country_id] = $visitor->visitors;
            }

            $total += $visitor->visitors;
        }

        if($total){
            $data['1'] = $data['1'] * 100 / $total;
            $data['2'] = $data['2'] * 100 / $total;
            $data['3'] = $data['3'] * 100 / $total;
            $data['4'] = $data['4'] * 100 / $total;
            $data['5'] = $data['5'] * 100 / $total;
            $data['9999'] = $data['9999'] * 100 / $total;
        }

        $data['1'] =   round($data['1'], 2);
        $data['2'] =   round($data['2'], 2);
        $data['3'] =   round($data['3'], 2);
        $data['4'] =   round($data['4'], 2);
        $data['5'] =   round($data['5'], 2);
        $data['9999'] =round($data['9999'], 2);

        $input = [];
        foreach($data as $k => $d){
            $input[] = $d;
        }

        return success(['input' => $input, 'data' => $data]);


    }


}