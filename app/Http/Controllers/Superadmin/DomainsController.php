<?php namespace App\Http\Controllers\Superadmin;

use \App\Http\Controllers\Basic\BaseController;
use App\Models\Domain;
use App\Models\Nomenclatoare\Post;
use App\Models\UserPost;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use App\Repositories\Superadmin\Nomenclatoare\Domains\Grid;
use App\Repositories\Superadmin\Nomenclatoare\Domains\Rows;
use App\Repositories\Superadmin\Nomenclatoare\Domains\Form;
use App\Repositories\Superadmin\Nomenclatoare\Domains\Action;

class DomainsController extends BaseController
{

    /**
     * $this->data() - vine din BaseController cu chestii comune (breadcrumb, meniu, ....)
     */
    public function getGrid()
    {
        $this->makeNavigation();
        return
            view('superadmin.nomenclatoare.domains.grid.index')
                ->with( $this->data('Domains', 'pentru filtrare') )
                ->withGrid( (new Grid())->parameters( Input::all()) )
                ->withToolbar( str_replace([Chr(13), Chr(10), Chr(9)], '', view('superadmin.nomenclatoare.domains.grid.toolbar')->withActionRoute(\URL::route('superadmin.nomenclatoare.domains.get-action-form', ['action' => 'insert']))->render()) )
                ->withIcon(NULL)
                ->withDescription(NULL)
            ;
    }

    /*
     * Generarea inregistrari pentru Grid
     */
    public function dataSource()
    {
        return (new Rows())->parameters(Input::all())->response();
    }

    /*
     * Trimite catre index formularul "actions"
     */
    public function getActionForm($action, $id = NULL)
    {

        $form =
            (new Form($action, $id))
                ->setGridId("gridDomains")
                ->route(\URL::route('superadmin.nomenclatoare.domains-do-action', ['action' => $action, 'id' => $id]))
        ;
        return \Response::json([
            'caption' => $form->caption(),
            'form'    => $form->render(),
            'action'  => $form->actionCaption(),
            'route'   => $form->getRoute(),
            'impact' => $action,
        ]);
    }

    /**
     * do Action - se face adaugarea/modificarea/stergere
     */
    public function doAction( $action, $id = NULL )
    {
        $action = (new Action($action, $id))->data(Input::get('data'));
        return \Response::json($action->commit());
    }

    public function all()
    {
        if(access()->user()->view_common_domains === 1) {
            $domains = collect([]);
         } else {
            $domains = Domain::where('visible',1)->where('user_id',0)->get();
        }
        $user_domains = Domain::where('user_id',access()->user()->id)->where('available',1)->get();
        $all = $domains->merge($user_domains);
        $random = $all->shuffle();
        return success(['data' => $random]);
    }

    public function generate()
    {
        $domain = Domain::find(Input::get('domain'));
        $post   = Post::find(Input::get('post'));

        if(strlen(auth()->user()->subdomain) == 0){
            return error('Please set your subdomain');
        }
        $fileExtension = File::extension($post->location);
        $newName = str_random(30).'.'.$fileExtension;
        $baseUrl = public_path(). '/uploads/' . access()->user()->id;
        $baseLocation = asset('uploads'). '/' . access()->user()->id . '/' .$newName;;

        if (! File::isDirectory($baseUrl))
        {
            File::makeDirectory($baseUrl,  0775, true);
        }
        $newPathWithName = $baseUrl . '/' .$newName;
//        if(! UserPost::where('post_id',$post->id)->where('user_id',access()->user()->id)->first()){

        if ( count($post->location) > 0 && File::exists($post->path) && File::copy($post->location , $newPathWithName)) {

        }

//        }
        $user_post = UserPost::create([
            'user_id'   => access()->user()->id,
            'post_id'   => $post->id,
            'location'  => $baseLocation,
            'domain_id' => $domain->id,
            'base_path' => 'uploads'.'/'.access()->user()->id . '/'. $newName
        ]);

        if(access()->user()->hasDomain($domain)){
            $location = 'http://'. $domain->title . '/client/'.\Hashids::encode(access()->user()->id) . '/'. \Hashids::encode($post->id) . '/'.\Hashids::encode($domain->id) .'/'.\Hashids::encode($user_post->id);
        }else{
            $location = 'http://'. access()->user()->subdomain . '.'. $domain->title . '/client/'.\Hashids::encode(access()->user()->id) . '/'. \Hashids::encode($post->id) . '/'.\Hashids::encode($domain->id) .'/'.\Hashids::encode($user_post->id);
        }
        $user_post->link = $location;
        $user_post->save();
        return success(['location' => $location]);
    }


    public function createHtml($post,$domain)
    {
        $file_name = auth()->user()->id;
        $file_name .= '-'.$post->id;
        $file_name .= '-'.$domain->id;
        $file_name .= '-'.str_replace(' ','_',$post->title);
        $file_name .= '.html';
        $user_dir   = $domain->location . '/'.auth()->user()->id;
        if (!File::isDirectory($user_dir))
        {
            File::makeDirectory($user_dir, 0777, true);
        }
        $location = $user_dir . '/'. $file_name;
        if(1 || ! file_exists($location) )
        {
            $myfile = fopen($location, "w");
            fwrite($myfile, view('intermediar._post')->with([
                'id'           => $post->id,
                'title'         => $post->title,
                'description'   => $post->description
            ])->render() );
            $uri = $myfile['uri'];
            fclose($myfile);
        }
        return $location;
    }

}