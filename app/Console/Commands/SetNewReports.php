<?php

namespace App\Console\Commands;

use App\Models\Access\User\User;
use App\Models\ArticlesEarning;
use App\Models\EarningsChart;
use App\Models\NewReferral;
use App\Models\NewReports;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SetNewReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newreports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Will set actual data from areports to newreports table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where('status',1)->where('confirmed', 1)->get();
        $yesterday = Carbon::yesterday()->toDateString();
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        foreach($users as $user) {
            \Log::info(PHP_EOL.'Newreports for user '.$user->id.PHP_EOL);
            $data = DB::table('areports')
                ->select(DB::raw('
                FLOOR(count(1)) as clicks,
                ROUND(count(1) * avg(quality),0) as clicks_calitate,
                ROUND(avg(quality) * 100,2) as quality,
                ROUND(sum(price),3) as earnings,
                ROUND((sum(price) / (count(1) * avg(quality))),3) as ecpc,
                ROUND((sum(price) * 1000 / (count(1) * avg(quality))),3)  as ecpm,
                DATE_FORMAT(created_at, "%Y-%m-%d") as date,
                user_id,
                created_at,
                updated_at
            '))
                ->where('user_id', '=', $user->id)
                ->whereDate('created_at','=',$yesterday)
                ->groupBy('date')
                ->get();

            NewReports::insert($data);
            $user->last_job = Carbon::now();
            $user->save();
        }
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $this->setEarningsChart($users);
        $this->setNewReferrals($users);
        $this->setArticlesEarning($users);
        $this->comment(PHP_EOL.'New areports a fost actualizat'.PHP_EOL);
        return;

    }

    public function setEarningsChart($users)
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $yesterday = Carbon::yesterday()->toDateString();
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        foreach ($users as $user) {
            \Log::info(PHP_EOL.'Earnings chart for user: '.$user->id.PHP_EOL);
            $weekChart = DB::table('areports')
                ->select(DB::raw('
                sum(price) as price,
                device as country_id,
                areports.created_at,
                DATE_FORMAT(areports.created_at, "%Y-%m-%d") as data,
                user_id
            '))
                ->where('user_id', '=', $user->id)
                ->whereDate('created_at', '=', $yesterday)
                ->groupBy('country_id')
                ->groupBy('data')
                ->orderBy('data')
                ->get();
            DB::setFetchMode(\PDO::FETCH_ASSOC);

            EarningsChart::insert($weekChart);
        }
        $this->comment(PHP_EOL.'Earnings chart a fost actualizat'.PHP_EOL);
    }

    public function setNewReferrals($users)
    {

        $yesterday = Carbon::yesterday()->toDateString();
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        foreach($users as $user) {
            \Log::info(PHP_EOL.'Newreferrals for user: '.$user->id.PHP_EOL);

            $dbData = DB::table('areferrals')
                ->leftJoin('users', 'users.id', '=', 'areferrals.invited_id')
                ->select(DB::raw('
                    sum(earning) as earnings,
                    DATE_FORMAT(areferrals.created_at, "%Y-%m-%d") as date,
                    users.name,
                    areferrals.invited_id as connection_id,
                    areferrals.invited_by_id as user_id
                '))
                ->where('areferrals.invited_by_id', '=',$user->id)
                ->whereDate('areferrals.created_at','=',$yesterday)
                ->orderBy('date')
                ->groupBy('invited_id', 'date')
                ->get();

            DB::setFetchMode(\PDO::FETCH_ASSOC);
            NewReferral::insert($dbData);
        }
    }

    public function setArticlesEarning($users)
    {
        $yesterday = Carbon::yesterday()->toDateString();
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        foreach($users as $user) {
            \Log::info(PHP_EOL . 'Article earnings for user ' . $user->id . PHP_EOL);
            $data = DB::table('user_posts')
                ->leftJoin('posts', 'user_posts.post_id', '=', 'posts.id')
                ->leftJoin('areports', 'user_posts.id', '=', 'areports.user_post_id')
                ->select(DB::raw('
                user_posts.id as user_post_id,
                user_posts.user_id as user_id,
                user_posts.created_at as user_post_created_at,
                posts.id as post_id,
                posts.location as photo,
                posts.title as title,
                posts.created_at as post_created_at,
                FLOOR(count(1)) as clicks,
                ROUND(count(1) * avg(quality),0) as clicks_calitate,
                ROUND(avg(quality) * 100,2) as quality,
                ROUND(sum(price),3) as earnings,
                IFNULL(ROUND((sum(price) / (count(1) * avg(quality))),3), 0) as ecpc,
                IFNULL(ROUND((sum(price) * 1000 / (count(1) * avg(quality))),3) , 0) as ecpm,
                areports.created_at as reports_created_at,
                CURRENT_TIMESTAMP() as created_at,
                CURRENT_TIMESTAMP() as updated_at
                
            '))
                ->where('user_posts.user_id', '=', $user->id)
                ->where('areports.user_id', '=', $user->id)
                ->whereDate('user_posts.created_at', '=', $yesterday)
                ->whereDate('areports.created_at', '=', $yesterday)
                ->groupBy('user_posts.post_id')
                ->orderBy('user_posts.created_at', 'DESC')
                ->get();
            ArticlesEarning::insert($data);
        }

    }

}
