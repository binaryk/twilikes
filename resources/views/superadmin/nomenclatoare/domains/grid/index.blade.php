@extends('__layouts.pages.datatable.index')
@section('css')
	@parent
	<link rel="stylesheet" href="{!! asset('custom/css/file-icons.css') !!}">
@stop

@section('custom-javascript-files')
	@parent
	<!-- includ js-ul pentru datatable creat dinamic -->
	{!! 
	App\Comptechsoft\Ui\Html\Scripts::make([
		'custom/js/domains/grid',
		'custom/js/simple_grids/index',
	])->render()
	!!}
@stop

@section('jquery-document-ready')
	@parent
	var grid  = new gridDomains();
	var index = new indexGrid({
		grid       : grid,
		toolbar    : '{!! $toolbar !!}',
		_token     : '{{ csrf_token() }}',
		form_width : 12
	}).init();
@stop
