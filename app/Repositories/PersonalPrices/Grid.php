<?php namespace App\Repositories\Superadmin\Nomenclatoare\PersonalPrices;

use \App\Comptechsoft\Datatable\Column;
use \App\Comptechsoft\Datatable\Header;
use \App\Comptechsoft\Datatable\Ajax;
use \App\Comptechsoft\Datatable\Order;
use \App\Comptechsoft\Datatable\Filter;

class Grid extends \App\Comptechsoft\Datatable\Grid
{

	public function __construct()
	{

		$this->noGlobalFilter();
		/*
		 * II asociez ID la grid
		 */
		$this->id('gridPersonalPrices');

		/*
		 * In ce fisier sa se genereze js-ul pentru .DataTable({...});
		 */
		$this->js( str_replace('\\', '/', public_path('custom/js/personalprices/grid.js') ));

        /*
		 * Care coloana contine ordinea initiala
		 */
        $this->order( (new Order())->setColumn(1)->setDirection('asc') );

        /*
         * Definirea coloanelor
         **/
        $this->addColumns([

            'rec-no' =>
                (new Column())
                    ->withHeader(
                        (new Header())
                            ->with(['caption' => '#', 'width' => 5])
                    )
                    ->alignRight(),

            'id' =>
                (new Column())
                    ->withHeader(
                        (new Header())
                            ->with(['caption' => 'ID', 'width' => 5])
                    )
                    ->alignCenter()
                    ->orderable(),

            'country' =>
                (new Column())
                    ->withHeader(
                        (new Header())
                            ->with(['caption' => 'Country', 'width' => 25])
                    )->orderable(),

            'mobile_price' =>
                (new Column())
                    ->withHeader(
                        (new Header())
                            ->with(['caption' => 'Mobile price ($)', 'width' => 25])
                    )->orderable(),

            'desktop_price' =>
                (new Column())
                    ->withHeader(
                        (new Header())
                            ->with(['caption' => 'Desktop price ($)', 'width' => 25])
                    )->orderable(),

            'actions' =>
                (new Column())
                    ->withHeader(
                        (new Header())
                            ->with(['caption' => 'Actions'])
                    ),



        ]);


	}

	public function settings()
	{
        /*
         * De la ce ruta vine raspunsul cu randuri
         */
		$this->ajax( (new Ajax())->setUrl(\URL::route('superadmin.nomenclatoare.personalprices.data-source',['user_id' => $this->getParameter('user_id')]))->setExtra(['a' => 1, 'b' => 2]) );
        return $this;
	}
}