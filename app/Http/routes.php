<?php

Route::group(['middleware' => 'web'], function() {
    Route::group(['namespace' => 'Language'], function () {
        require (__DIR__ . '/Routes/Language/Language.php');
    });
    Route::group(['namespace' => 'Frontend'], function () {
        require (__DIR__ . '/Routes/Frontend/Frontend.php');
        require (__DIR__ . '/Routes/Frontend/Access.php');
    });
});
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'middleware' => 'admin'], function () {
    require (__DIR__ . '/Routes/Backend/Dashboard.php');
    require (__DIR__ . '/Routes/Backend/Access.php');
    require (__DIR__ . '/Routes/Backend/LogViewer.php');
});

Route::group(['middleware' => 'web'], function() {

    Route::group(['namespace' => 'Superadmin', 'middleware' => 'admin'], function () {

        Route::get('superadmin/nomenclatoare/send-emails', 'EmailsController@getGrid')
            ->name('superadmin.nomenclatoare.emails.index');

        Route::post('superadmin/nomenclatoare/send-emails-source', 'EmailsController@dataSource')
            ->name('superadmin.nomenclatoare.emails.data-source');

        Route::post('superadmin/nomenclatoare/send-emails-get-action-form/{action}/{id?}', 'EmailsController@getActionForm')
            ->name('superadmin.nomenclatoare.emails.get-action-form');

        Route::post('superadmin/nomenclatoare/send-emails-action/{action}/{id?}', 'EmailsController@doAction')
            ->name('superadmin.nomenclatoare.emails-do-action');
    });

    foreach(Config::get('routes.files') as $i => $file)
    {
        require( str_replace('\\', '/', app_path()) . '/Http/Routes/' . $file) ;
    }
});
