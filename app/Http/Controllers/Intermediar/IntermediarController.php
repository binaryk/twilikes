<?php namespace App\Http\Controllers\Intermediar;

use App\Jobs\IntermediarAgent;
use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use App\Models\Domain;
use App\Models\Nomenclatoare\Post;
use App\Models\UserPost;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Jenssegers\Agent\Agent;

/**
 * Class LanguageController
 * @package App\Http\Controllers
 */
class IntermediarController extends Controller
{
    use IntemediarTrait;

    public function index($user_id, $post_id,$domain_id, $user_post_id)
    {
        $agent = new Agent();
        $request_ip = $this->getRequestIp();
//        if(@\Hashids::decode($user_id)[0] == 47){
//            dd('HTTP_X_FORWARDED_FOR',@$_SERVER['HTTP_X_FORWARDED_FOR'],'HTTP_X_REAL_IP', @$_SERVER['HTTP_X_REAL_IP'],'REMOTE_ADDR', @$_SERVER['REMOTE_ADDR'], 'ret', $this->getRequestIp(), 'HTTP_X_SUCURI_CLIENTIP',@$_SERVER['HTTP_X_SUCURI_CLIENTIP']);
//        }
        try{
//            \Log::notice(\URL::current());
            $tmp_user_id = @\Hashids::decode($user_id)[0];
            $tmp_subdomain  = @User::find($tmp_user_id)->subdomain;
            $tmp_title      =   @Domain::find(\Hashids::decode($domain_id)[0])->title;
            if(Domain::find(\Hashids::decode($domain_id)[0])->already_have){
                 $link   = 'http://'. $tmp_title . '/client/'.$user_id . '/'. @$post_id . '/'.@$domain_id . '/' . @$user_post_id;
             }else{
                 $link   = 'http://'. $tmp_subdomain . '.'. $tmp_title . '/client/'.$user_id . '/'. @$post_id . '/'.@$domain_id . '/' . @$user_post_id;
             }

            $user_id   = \Hashids::decode($user_id)[0];
            $post_id = \Hashids::decode($post_id)[0];

            $domain_id = \Hashids::decode($domain_id)[0];
            $user_post_id = \Hashids::decode($user_post_id)[0];
            $post   = Post::find($post_id);
            $domain = Domain::find($domain_id);
            $user   = User::find($user_id);

            $user_post = UserPost::find($user_post_id);
            if(! $user_post){
                throw new \Exception('User post not found');
                dd('Aici', $user_post_id);
            }

            $test_path = $user_post->base_path;

            if(! file_exists(public_path($test_path))) {
                if(file_exists($post->path)) {
                    $test_path = $post->path;
                } else {
                    $test_path = 'uploads/1/Tu7GXa78KpwZIpoomDaFGK5B0l8gU7.jpg';
                }
            }

            $test_user_id = $user->id;
            if( env('APP_ENV') === 'dev' ){
                $intermediar_img_location = null;
            } else {
                $intermediar_img_location = $this->resizeImage($test_path,$test_user_id, $agent->isMobile());
            }
//            \Log::info('visitor for user: '.$user->email.', '.$user->name);
            $view = "intermediar.mobile._post";
            if($agent->isDesktop()){
                $view = "intermediar.desktop._post";
            }

        $destination = $post->link . '?utm_campaign=twilikes&utm_source='.$user_id.'&utm_medium=referral&utm_content=twilikes';
            if(env('APP_ENV') == 'dev'){
                $cors = "http://localhost/twilikes/public/";
            }else{
                $cors = $domain->already_have ? 'http://'.$domain->title . "/" : 'http://'.$user->subdomain.'.'.$domain->title . "/";
            }

            if($user_id != null && $user->personal == 1){
                $job = (new IntermediarAgent($agent, $request_ip, $post_id, $user_id, null, $domain_id, $user_post_id, true ));//->onQueue('personals');
                $this->dispatch($job);
            }
            if( env('APP_ENV') === 'dev' || (access()->user() && access()->user()->id == 47)) {
//                dd($cors);
            }
            return view($view)->with([
                'title' => $post ? $post->title : '',
                'description' => $post ? $post->description : '',
                'domain' => $domain,
                'author' => $user,
                'subdomain' => $domain->already_have ? $domain->title : $user->subdomain,
                'link' => $link,
                'user_post' => $user_post,
                'og_image' => $domain->already_have ? 'http://'.@$domain->title . '/' . @$user_post->base_path : 'http://'.@$user->subdomain.'.'.@$domain->title . '/' . @$user_post->base_path,
                'destination' => $destination,
                'intermediar_location' => $intermediar_img_location,
                'request' => [
                    'user_id' => $user_id,
                    'post_id' => $post_id,
                    'domain_id' => $domain_id,
                    'user_post_id' => $user_post_id,
                    'cors' =>  $cors,
                    'referrer' => '$referrer',
                ],
            ]);
            logquery('class: '.__CLASS__.', function: '.__FUNCTION__.'; line: '.__LINE__);
        }catch(\Exception $e){
            \Log::error('URL: '.\URL::current().'; EROR: ' .$e->getMessage().';Line:'.$e->getLine().'; Agent:'.$agent->browser() . '; is robot: '.$agent->isRobot().';Platform:'.$agent->platform());
//            return 'URL: '.\URL::current().'; EROR: ' .$e->getMessage().';Line:'.$e->getLine().'; Agent:'.$agent->browser() . '; is robot: '.$agent->isRobot().';Platform:'.$agent->platform();
//            return redirect()->to('http://funny.cancrisocial.us/client/lejRe/b2vj1/e5y3x/XMDZg');
            return "<h2>Something went wrong</h2>";
            $user_post = UserPost::where('user_id', $user_id)->get();
            $founded = null;
            foreach($user_post as $post) {
                if(file_exists(public_path($user_post->base_path))) {
                    $founded = $post;
                }
            }
            if($founded ){
                return redirect()->to($founded->link);
            } else {
                return "<h2>Something went wrong</h2>";
            }

        }
    }


    public function isNotAPersonalAccount($user_id)
    {
        return $user_id != null && User::find($user_id)->personal != 1;
    }

    public function isPersonalWithout100Account($user_id)
    {
        return $user_id != null && User::find($user_id)->personal == 2;
    }
/*
 * Aici e de calitate
 * */
    public function agent()
    {
        $time_start = microtime(true);
        $agent      = new Agent();
        $post_id    = Input::get('post_id');
        $request_ip = $this->getRequestIp();
        $user_id    = Input::get('user_id');
        $referrer   = Input::get('referrer');
        $domain_id  = Input::get('domain_id');
        $user_post_id = Input::get('user_post_id');

        if($user_id == 38){
        }


        if($this->isNotAPersonalAccount($user_id)){
            $job = (new IntermediarAgent($agent, $request_ip, $post_id, $user_id, $referrer, $domain_id, $user_post_id, false));
            $this->dispatch($job);
        }
        

        $time_end = microtime(true);
        $time = $time_end - $time_start;
//        \Log::critical('Agent time: '.$time);
        return success(['report' => '']);
    }

    public function getRequestIp()
    {
        /*if(env('SOURCE_IP')) {
            return $_SERVER[env('SOURCE_IP')];
        }*/
        /*if(env('ATAK')) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }*/
        if(array_key_exists('HTTP_X_SUCURI_CLIENTIP', $_SERVER)) {
            $request_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            if(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)){
                $request_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }else{
                if(array_key_exists('HTTP_X_REAL_IP', $_SERVER)){
                    $request_ip = $_SERVER['HTTP_X_REAL_IP'];
                }else{
                    $request_ip = $_SERVER['REMOTE_ADDR'];
                }
            }
        }

        return $request_ip;

        //dd($_SERVER['SERVER_ADDR'] == "107.170.70.182");
        if (env('APP_ENV') == 'dev' || $_SERVER['SERVER_ADDR'] == "107.170.70.182") {
//            $request_ip = '108.162.244.49';
            $request_ip = Request::ip();
        } else {
            //$request_ip = Request::ip();

            $request_ip = $_SERVER['HTTP_X_REAL_IP'];
        }

        return $request_ip;
    }

    public function alive()
    {
        try{
            $time_start = microtime(true);

            $post_id = Input::get('post_id');
            $user_id = Input::get('user_id');
            $user_post_id = Input::get('user_post_id');
            $domain_id = Input::get('domain_id');
            $request_ip = $this->getRequestIp();
            $post   = Post::find($post_id);
            $referrer   = Input::get('referrer');
        if($user_id == 38){
//            \Log::debug('Pas 1 - alive:: ip: '.$request_ip);
        }


        $alreadyClicked = $this->selfTodayClicked($request_ip, $post_id, $user_id );

        if(!  $alreadyClicked && $this->isNotAPersonalAccount($user_id) ){
            $post->clicks = $post->clicks + 1;
            $post->save();
            $this->updateVisitorsChart($user_id, $request_ip);
            $this->updateReportTable($referrer, $user_id, $post_id, $domain_id, $user_post_id, $request_ip);
        }

        $time_end = microtime(true);
        $time = $time_end - $time_start;

//        \Log::alert('Alive time: '.$time);
        return success(['alreadyVisit' => $alreadyClicked,]);
        }catch(\Exception $e){
//            \Log::warning('URL: '.\URL::current().'; EROR: ' .$e->getMessage().';Line:'.$e->getLine().'; Agent:');
            return success(['alreadyVisit' => true,]);
        }
    }



}