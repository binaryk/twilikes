<?php

Route::group(['namespace' => 'UserPosts', 'middleware' => 'auth'], function () {

    Route::get('my-articles', 'UserPostsController@getGrid')
        ->name('user_posts.index');

    Route::post('my-articles-source', 'UserPostsController@dataSource')
        ->name('user_posts.data-source');

    /*
     * Sa se incarce formularul de actiun (actions form)
     */
    Route::post('my-articles-get-action-form/{action}/{id?}', 'UserPostsController@getActionForm')
        ->name('user_posts.get-action-form');

    /*
     * ce se intampla la Adauga/Salveaza/Sterge
     */
    Route::post('my-articles-action/{action}/{id?}', 'UserPostsController@doAction')
        ->name('user_posts-do-action');


    Route::post('api/apiUploadPhotoFromUrl', 'UserPostsController@apiUploadPhotoFromUrl')
        ->name('apiUploadPhotoFromUrl');


    Route::post('api/apiUploadPhotoFrom', 'UserPostsController@apiUploadPhotoFrom')
        ->name('apiUploadPhotoFrom');
});