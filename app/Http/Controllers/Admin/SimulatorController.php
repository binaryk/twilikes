<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\Visit;
use Illuminate\Support\Facades\Input;

/**
 * Class LanguageController
 * @package App\Http\Controllers
 */
class SimulatorController extends Controller
{
    public function index()
    {

        return view('admin.cruds.index');
    }

    public function store()
    {
        $data = Input::all();
        Visit::create($data);
        return redirect()->back()->withFlashSuccess("With success");
        dd($data);
    }

}