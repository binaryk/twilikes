<?php

namespace App\Console;

use App\Console\Commands\SetNewReports;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

/**
 * Class Kernel
 * @package App\Console
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
         Commands\Inspire::class,
         SetNewReports::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $orologiu = Carbon::now()->hour == 0;
        $schedule->command('newreports')->hourly()->when(function() use ($orologiu){
            return $orologiu;
        });

//         $schedule->command('newreports')->daily()->at('24:01');
        // $schedule->command('inspire')->hourly();

        /**
         * Laravel Backup Commands
         */
        // $schedule->command('backup:clean')->daily()->at('01:00');
        // $schedule->command('backup:run')->daily()->at('02:00');
    }
}
