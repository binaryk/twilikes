<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function(Blueprint $t){
            $t->increments('id');
            $t->integer('category');
            $t->string('title');
            $t->text('description');
            $t->string('link');
            $t->string('path');
            $t->string('location');
            $t->string('extention');
            $t->float('storage');
            $t->integer('author');
            $t->string('img_name');
            $t->softDeletes();
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
