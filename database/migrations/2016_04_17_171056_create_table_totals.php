<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTotals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('totals', function(Blueprint $t){
            $t->increments('id');
            $t->integer('user_id');
            $t->float('yesterday_incomes');
            $t->float('month_incomes');
            $t->float('overall_incomes');
            $t->float('overall_visits');
            $t->softDeletes();
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('totals');
    }
}
