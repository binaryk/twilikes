@extends('template.~template')
@section('content')
    @if($hasFbFields)
      <h3> {!! $message !!}</h3>
    @else
        <div class="panel panel-dark panel-flat" id="pending-confirmation">
            <div class="panel-heading text-center">
                <a href="#">
                    <img src="img/logo.png" alt="Image" class="block-center img-rounded">
                </a>
            </div>
            <div class="panel-body">
                <p class="text-center pv">Security Check.</p>
                {!! Form::open(['url' => 'security-check', 'class' => 'mb-lg', 'files' => 'true', 'id' => 'pending-form']) !!}
                <div class="alert alert-danger" id="messages" style="display: none;">
                    <p id="fbPageError">Fanpage URL field is required.</p>
                    <p id="fbPrintScreenError">Print Screen field is required.</p>
                </div>
                <div class="form-group has-feedback" >
                    <label for="fbPage" class="text-muted">Fanpage URL</label>
                    {!! Form::input('fbPage', 'fbPage', null, ['class' => 'form-control', 'placeholder' => 'Facebook Page Url', 'id' => 'fbPage']) !!}
                </div>
                <div class="form-group has-feedback">
                    <label for="fbPrintScreen" class="text-muted">Fanpage Activity Log Print Screen</label>
                    {!! Form::file('fbPrintScreen', ['class' => 'form-control','id' => 'fbPrintScreen']) !!}
                </div>
                <input type="hidden" name="user_id" value="{!! $user_id !!}">
                <input type="hidden" name="hash" value="{!! $hash !!}">
                <button type="button" class="btn btn-block btn-primary mt-lg" id="submit-button">Save</button>
                {!! Form::close() !!}
            </div>
        </div>
    @endif
@endsection
@section('custom-javascript-files')
    <script src="{!! asset('custom/js/vue/pending-confirmation/pending-confirmation.js') !!}"></script>
@endsection