<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceChart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_chart', function (Blueprint $t) {
            $t->bigIncrements('id');
            $t->bigInteger('user_id');
            $t->string('data');
            $t->integer('phone');
            $t->integer('tablet');
            $t->integer('desktop');
            $t->softDeletes();
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('device_chart');
    }
}
