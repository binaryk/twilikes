<?php
Route::group(['namespace' => 'Superadmin', 'middleware' => 'admin'], function () {

    Route::get('superadmin/nomenclatoare/payment', 'PaymentController@getGrid')
        ->name('superadmin.nomenclatoare.payment.index');

    Route::post('superadmin/nomenclatoare/payment-source', 'PaymentController@dataSource')
        ->name('superadmin.nomenclatoare.payment.data-source');

    Route::get('superadmin/nomenclatoare/refresh/{user_id?}', 'PaymentController@refresh')
        ->name('superadmin.nomenclatoare.payment.refresh');

    Route::get('superadmin/nomenclatoare/refresh-from-to/{from_id}/{to_id}', 'PaymentController@refreshFrom')
        ->name('superadmin.nomenclatoare.payment.refresh_from_to');


    Route::get('superadmin/nomenclatoare/diff/{user_id?}', 'PaymentController@diff')
        ->name('superadmin.nomenclatoare.payment.diff');

    /*
     * Sa se incarce formularul de actiun (actions form)
     */
    Route::post('superadmin/nomenclatoare/payment-get-action-form/{action}/{id?}', 'PaymentController@getActionForm')
        ->name('superadmin.nomenclatoare.payment.get-action-form');

    /*
     * ce se intampla la Adauga/Salveaza/Sterge
     */
    Route::post('superadmin/nomenclatoare/payment-action/{action}/{id?}', 'PaymentController@doAction')
        ->name('superadmin.nomenclatoare.payment-do-action');

    Route::get('api/payment', 'PaymentController@all')
        ->name('api.payment.all');

    Route::post('api/payment/generate', 'PaymentController@generate')
        ->name('api.payment.generate');
});

