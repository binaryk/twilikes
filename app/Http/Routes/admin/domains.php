<?php

Route::group(['namespace' => 'Superadmin', 'middleware' => 'admin'], function () {

    Route::get('superadmin/nomenclatoare/domains', 'DomainsController@getGrid')
        ->name('superadmin.nomenclatoare.domains.index');

    Route::post('superadmin/nomenclatoare/domains-source', 'DomainsController@dataSource')
        ->name('superadmin.nomenclatoare.domains.data-source');

    /*
     * Sa se incarce formularul de actiun (actions form)
     */
    Route::post('superadmin/nomenclatoare/domains-get-action-form/{action}/{id?}', 'DomainsController@getActionForm')
        ->name('superadmin.nomenclatoare.domains.get-action-form');

    /*
     * ce se intampla la Adauga/Salveaza/Sterge
     */
    Route::post('superadmin/nomenclatoare/domains-action/{action}/{id?}', 'DomainsController@doAction')
        ->name('superadmin.nomenclatoare.domains-do-action');


});


