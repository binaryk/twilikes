@extends('template.~template')
@section('content')
    <div class="x_panel">
        <div class="x_title">
            <h2>Click simulator</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br>
            <form id="demo-form2" data-parsley-validate="" method="POST" action="{!! route('admin.cruds.store') !!}" class="form-horizontal form-label-left" novalidate="" _lpchecked="1">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">From
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="from" name="from" required="required" class="form-control col-md-7 col-xs-12" data-parsley-id="0217"><ul class="parsley-errors-list" id="parsley-id-0217"></ul>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Value <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="amount" name="amount" required="required" class="form-control col-md-7 col-xs-12" data-parsley-id="3649"><ul class="parsley-errors-list" id="parsley-id-3649"></ul>
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
@section('js')
    <script src="{!! asset('custom/js/vue/cruds/index.js') !!}"></script>
@endsection