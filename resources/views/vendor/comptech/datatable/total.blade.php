<script>
    var total = function ( row, data, start, end, display ) {
        var cols = JSON.parse('{!! $footerTotal !!}');
        var api = this.api(), data;
        function injectColumnTotal(column){
            // Total over all pages
            total = api.column( column ).data().reduce( function (a, b) {
                return parseFloat(a) + parseFloat(b);
            }, 0 );
            // Total over this page
            pageTotal = api.column( column, { page: 'current'} ).data().reduce( function (a, b) {
                return parseFloat(a) + parseFloat(b);
            }, 0 );
            console.log( pageTotal );
            $( api.column( column ).footer() ).html(pageTotal);
        }

        cols.forEach( function(el) {
            injectColumnTotal(el);
        });
    }
</script>