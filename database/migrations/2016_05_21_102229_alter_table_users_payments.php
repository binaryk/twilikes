<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUsersPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function(Blueprint $t){
            $t->integer('payment_method')->default(0);
            $t->string('payment_status')->default('pending');
            $t->string('paypal_name');
            $t->string('bank_name');
            $t->string('bank_street');
            $t->string('iban');
            $t->string('holder_name');
            $t->string('bank_city');
            $t->string('bank_zip');
            $t->string('swift_code');
            $t->float('unpaid')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users',function(Blueprint $t){
            $t->dropColumn([
                'payment_method',
                'payment_status',
                'paypal_name',
                'bank_name',
                'bank_street',
                'iban',
                'holder_name',
                'bank_city',
                'bank_zip',
                'swift_code',
                'unpaid',
            ]);
        });
    }
}
