<!DOCTYPE html>
<html lang="en" ng-app="app" ng-controller="MainCtrl">
@include('template.~head')
<body class="nav-md" @yield('ctrl')>
    <div class="container body">
        <div class="main_container">
            @include('template.~sidebar')
            @include('template.~top')
            @include('template.~page')
        </div>
    </div>
    @yield('modal')
    @include('template.~js')
</body>
</html>
