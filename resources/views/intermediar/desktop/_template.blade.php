@include('intermediar.desktop._head')
<body>

<nav class="header_navbar navbar navbar-inverse navbar-fixed-top top-header">
    <div class="container">
        <h3 class="text-center title-top">{!! @$subdomain !!}</h3>
    </div>
</nav>
<div style="clear:both;height: 20px;"></div>

    @yield('content')

@include('intermediar.desktop._footer')