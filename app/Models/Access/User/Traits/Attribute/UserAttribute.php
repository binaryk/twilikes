<?php

namespace App\Models\Access\User\Traits\Attribute;
use App\Models\Bill;
use App\Models\NewReferral;
use App\Models\NewReports;
use App\Models\Referral;
use App\Models\Areport;
use App\Models\UserPost;
use Carbon\Carbon;

/**
 * Class UserAttribute
 * @package App\Models\Access\User\Traits\Attribute
 */
trait UserAttribute
{

    /**
     * @return mixed
     */
    public function canChangeEmail()
    {
        return config('access.users.change_email');
    }

    /**
     * @return bool
     */
    public function canChangePassword()
    {
        return ! app('session')->has(config('access.socialite_session_name'));
    }

    /**
     * @return string
     */
    public function getConfirmedLabelAttribute()
    {
        if ($this->isConfirmed())
            return "<label class='label label-success'>".trans('labels.general.yes')."</label>";
        return "<label class='label label-danger'>".trans('labels.general.no')."</label>";
    }

    /**
     * @return string
     */
    public function getPersonalLabelAttribute()
    {
        if ($this->personal)
            return "<label class='label label-success'>".trans('labels.general.yes')."</label>";
        return "<label class='label label-danger'>".trans('labels.general.no')."</label>";
    }

    /**
     * @return mixed
     */
    public function getPictureAttribute()
    {
        return $this->getPicture();
    }

    /**
     * @param bool $size
     * @return mixed
     */
    public function getPicture($size = false)
    {
        if (! $size) $size = config('gravatar.default.size');
        return gravatar()->get($this->email, ['size' => $size]);
    }

    /**
     * @param $provider
     * @return bool
     */
    public function hasProvider($provider)
    {
        foreach ($this->providers as $p) {
            if ($p->provider == $provider) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isActive() {
        return $this->status == 1;
    }

    /**
     * @return bool
     */
    public function isConfirmed() {
        return $this->confirmed == 1;
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        if (access()->allow('edit-users')) {
            return '<a href="' . route('admin.access.users.edit', $this->id) . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getChangePasswordButtonAttribute()
    {
        if (access()->allow('change-user-password')) {
            return '<a href="' . route('admin.access.user.change-password', $this->id) . '" class="btn btn-xs btn-info"><i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.backend.access.users.change_password') . '"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->status) {
            case 0:
                if (access()->allow('reactivate-users')) {
                    return '<a href="' . route('admin.access.user.mark', [$this->id, 1]) . '" class="btn btn-xs btn-success"><i class="fa fa-play" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.backend.access.users.activate') . '"></i></a> ';
                }

                break;

            case 1:
                if (access()->allow('deactivate-users')) {
                    return '<a href="' . route('admin.access.user.mark', [$this->id, 0]) . '" class="btn btn-xs btn-warning"><i class="fa fa-pause" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.backend.access.users.deactivate') . '"></i></a> ';
                }

                break;

            default:
                return '';
                // No break
        }

        return '';
    }

    /**
     * @return string
     */
    public function getConfirmedButtonAttribute()
    {
        if (! $this->isConfirmed()) {
            if (access()->allow('resend-user-confirmation-email')) {
                return '<a href="' . route('admin.account.confirm.resend', $this->id) . '" class="btn btn-xs btn-success"><i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title=' . trans('buttons.backend.access.users.resend_email') . '"></i></a> ';
            }
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        if (access()->allow('delete-users')) {
            return '<a href="' . route('admin.access.users.destroy', $this->id) . '"
                 data-method="delete"
                 data-trans-button-cancel="'.trans('buttons.general.cancel').'"
                 data-trans-button-confirm="'.trans('buttons.general.crud.delete').'"
                 data-trans-title="'.trans('strings.backend.general.are_you_sure').'"
                 class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getLoginButtonAttribute()
    {
            return '<a href="' . route('admin.access.users.loginAs', $this->id) . '"
                 data-trans-button-confirm="Login as"
                 data-trans-title="'.trans('strings.backend.general.are_you_sure').'"
                 class="btn btn-xs btn-default"><i class="fa fa-user" data-toggle="tooltip" data-placement="top" title="Login as"></i></a>';
        return '';
    }

    /**
     * @return string
     */
    public function getLinkButtonAttribute()
    {
            return '<a data-href="' . route('admin.access.users.links', $this->id) . '"
                 @click="referrers($event, '.$this->id.')"
                 data-trans-button-confirm="Login as"
                 data-trans-title="'.trans('strings.backend.general.are_you_sure').'"
                 class="btn btn-xs btn-info"><i class="fa fa-list" data-toggle="tooltip" data-placement="top" title="Referrals"></i></a>';
        return '';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return $this->getEditButtonAttribute() .
        $this->getChangePasswordButtonAttribute() . ' ' .
        $this->getStatusButtonAttribute() .
        $this->getConfirmedButtonAttribute() .
        $this->getDeleteButtonAttribute() .
        $this->getLoginButtonAttribute()
            ;
    }

    public function getEarningsAttribute()
    {
        $user_posts = Areport::where('user_id',$this->id)->whereDate('created_at', '=',Carbon::now()->toDateString())->get();

        $summ = 0;
        foreach($user_posts as $post){
            $summ = floatval($summ) + floatval($post->price);
        }
        $summ = number_format($summ, 4);
        return floatval($summ);
    }

    public function getYesterdayAttribute()
    {
        $user_posts = NewReports::where('user_id',$this->id)->whereDate('created_at', '=',Carbon::yesterday()->toDateString())->get();

        $summ = 0;
        foreach($user_posts as $post){
            $summ = floatval($summ) + floatval($post->earnings);
        }
        $summ = number_format($summ, 4);
        return floatval($summ);
    }

    public function getMonthAttribute()
    {
        $user_posts = NewReports::where('user_id',$this->id)->whereDate('created_at', '>=',Carbon::now()->subMonth()->toDateString())->get();
        $summ = 0;
        foreach($user_posts as $post){
            $summ = floatval($summ) + floatval($post->earnings);
        }
        $summ = number_format($summ, 4);
        return floatval($summ);
    }

    public function getReferralAttribute()
    {
        $user_posts = Referral::where('invited_by_id',$this->id)->get();
        $summ = 0;
        foreach($user_posts as $post){
            $summ = floatval($summ) + floatval($post->earning);
        }
        $summ = number_format($summ, 4);
        return floatval($summ);
    }



    /**
     * @return string
     */
    public function getPaymentLabelAttribute()
    {
        $normal = $this->getMonthAttribute() + $this->getReferralAttribute() - $this->paid;
        $normal = floatval($normal);
        $normal = number_format($normal, 4);
        if ($normal <= floatval($this->unpaid))
            return "<label class='label label-success'>".$this->getMonthAttribute(). " + " . $this->getReferralAttribute() . " - ".$this->paid ." = " . $normal."</label>";
        return "<label class='label label-danger'>".$this->getMonthAttribute(). " + " . $this->getReferralAttribute() . " - ".$this->paid ." = " . $normal."</label>";
    }


    public function getRealUnpaidAttribute()
    {
        /*
         *
         * Aici e suma care se plateste fizic de Emy, adica pana ieri inclusiv -> pt ca cronjob-ul merge pana ieri,
         * deci sumele $summ si $referral sunt pana ieri
         * */
        $last_bill = Bill::where('user_id',$this->id)->orderBy('id','DESC')->first();
        $start_date = $this->created_at;

        if($last_bill){
            $start_date = $last_bill->created_at;
        }

        $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $start_date->year . '-' . $start_date->month . '-' . $start_date->day . ' 00:00:00');

        $summ = \DB::table('newreports')
            ->where('user_id', '=', $this->id)
            ->whereBetween('created_at',[$start_date, Carbon::now()])
            ->sum('earnings');

        $referral = NewReferral::where('user_id', $this->id)
            ->whereBetween('date',[$start_date, Carbon::now()])
            ->sum('earnings');

        if(! $summ) {
            $summ = 0;
        }

        if(!$referral) {
            $referral = 0;
        }


        return floatval(number_format($summ + $referral, 4,'.',''));
    }

    public function getRealWithTodayUnpaidAttribute()
    {
        $last_bill = Bill::where('user_id',$this->id)->orderBy('id','DESC')->first();
        $start_date = $this->created_at;

        if($last_bill){
            $start_date = $last_bill->created_at;
        }

        $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $start_date->year . '-' . $start_date->month . '-' . $start_date->day . ' 00:00:00');


        $summ = \DB::table('newreports')
            ->where('user_id', '=', $this->id)
//            ->whereDate('created_at', '>=', $start_date->subDay())
//            ->whereDate('created_at', '<=', Carbon::now()->toDateString())
            ->whereBetween('created_at',[$start_date, Carbon::now()])
            ->sum('earnings');

        $referral = NewReferral::where('user_id', $this->id)
//            ->whereDate('created_at', '>=', $start_date)
//            ->whereDate('created_at', '<=', Carbon::now()->toDateString())
            ->whereBetween('created_at',[$start_date, Carbon::now()])
            ->sum('earnings');

        $reports = Areport::where('user_id',$this->id)
            ->whereRaw('Date(created_at) = ?', [Carbon::now()->format('Y-m-d')] )
            ->sum('price');

        if(! $summ) {
            $summ = 0;
        }
        if(! $referral) {
            $referral = 0;
        }
        if(! $reports) {
            $reports = 0;
        }



        return floatval(number_format($summ + ($referral ? $referral : 0) + ($reports ? $reports : 0), 4,'.',''));
    }
}
