<?php
Route::group(['namespace' => 'App', 'middleware' => 'auth'], function () {
    Route::group(['middleware' => 'cors'], function () {
        Route::get('/', 'DashboardController@index')
            ->name('frontend.index');


        Route::get('api/today-visitors-device', 'DashboardController@apiTodayVisitorsDevice')
            ->name('api.todayVisitors');

        Route::get('api/weekly-earnings', 'DashboardController@apiWeeklyEarnings')
            ->name('api.weekleyEarnings');

        Route::get('api/today-visitors', 'DashboardController@apiTodayVisitor')
            ->name('api.todayVisitors');

        Route::post('api/tile-dashboard', 'DashboardController@tile')
            ->name('api.tileDashboard');
    });
});