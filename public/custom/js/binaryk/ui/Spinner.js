var App;
(function (App) {
    class Spinner {
        constructor(subiect) {
            this.spinner = $(subiect);
        }
        show() {
            this.spinner.show();
        }
        hide(time, callback) {
            const _that = this;
            if (time) {
                setTimeout(function () {
                    _that.spinner.hide();
                    callback();
                }, time);
            }
            else {
                _that.spinner.hide();
            }
        }
    }
    App.Spinner = Spinner;
})(App || (App = {}));
//# sourceMappingURL=Spinner.js.map