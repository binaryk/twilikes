<?php

namespace App\Services\Access\Traits;

use App\Events\Frontend\Auth\UserRegistered;
use App\Http\Requests\Frontend\Auth\RegisterRequest;

/**
 * Class RegistersUsers
 * @package App\Services\Access\Traits
 */
trait RegistersUsers
{
    use RedirectsUsers;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegistrationForm()
    {
        return view('frontend.auth.register');
    }

    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(RegisterRequest $request)
    {
        $invited_by = \Cookie::get('invited_by');
        $invited_by = $invited_by ? \Crypt::decrypt($invited_by) : 0;
        $data = $request->all();
        if($request->hasFile('fbPrintScreen') && $request->file('fbPrintScreen')->isValid()){
            $result = $request->file('fbPrintScreen')->move(public_path('uploads/fbPrintScreens'), str_random(20).'_'.$request->file('fbPrintScreen')->getClientOriginalName());
            $fileLocation = 'uploads/fbPrintScreens/'.$result->getFilename();
            $data['invited_by'] = $invited_by;
            $data['fbPrintScreen'] = $fileLocation;
            if (config('access.users.confirm_email')) {
                $user = $this->user->create($data);
                event(new UserRegistered($user));
                return redirect()->route('auth.login')->withFlashSuccess(trans('exceptions.frontend.auth.confirmation.created_confirm'));
            } else {
                auth()->login($this->user->create($data));
                event(new UserRegistered(access()->user()));
                return redirect($this->redirectPath());
            }
        } else {
            return redirect()->back()->withInput();
        }
    }
}