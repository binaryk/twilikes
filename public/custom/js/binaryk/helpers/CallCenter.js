var App;
(function (App) {
    var Helpers;
    (function (Helpers) {
        var Ajax;
        (function (Ajax) {
            class CallCenter {
                constructor() {
                    this.objectToArray = (obj) => {
                        var out = [];
                        var array = $.map(obj, function (value, index) {
                            out.push(value);
                            return out;
                        });
                        return out;
                    };
                }
            }
            Ajax.CallCenter = CallCenter;
        })(Ajax = Helpers.Ajax || (Helpers.Ajax = {}));
    })(Helpers = App.Helpers || (App.Helpers = {}));
})(App || (App = {}));
//# sourceMappingURL=CallCenter.js.map