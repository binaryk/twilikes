<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Foundation\Auth\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\Intermediar\IntemediarTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
class BanEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $email;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $email )
    {
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
*/
    public function handle()
    {
        $email = $this->email;
        $message = \DB::table('default_messages')->orderBy('id','DESC')->first();
        if($message){

            Mail::send('superadmin.nomenclatoare.emails.send.send', ['content' => $message->message], function ($message) use ($email) {
                $message->subject('Ban notification');
                $message->to($email);
            });

        }
        
    }

}
