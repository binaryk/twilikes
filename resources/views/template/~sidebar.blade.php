<div class="col-md-3 left_col">
    <div class="left_col scroll-view">

        <div class="clearfix"></div>
        <div class="profile">
            <div class="profile_pic">
                <img src="{!! asset('img/avatar-icon.png') !!}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                @if(access()->user())
                    <h2>{!! access()->user()->name !!}</h2>
                @endif
            </div>
        </div>
        <!-- /menu prile quick info -->
        <br />
        @if(access()->user())
            @include('template.~menu')
        @endif
        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings" href="{!! route('frontend.user.dashboard') !!}">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" href="{!! url('logout') !!}" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
<!-- /menu footer buttons -->
</div>
</div>
