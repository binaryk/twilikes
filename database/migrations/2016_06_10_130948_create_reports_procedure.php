<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
         DROP PROCEDURE IF EXISTS one_report;
        CREATE PROCEDURE one_report(IN _user_id INTEGER, IN _start_date DATE, IN _end_date DATE, OUT visitors INTEGER, OUT clicks INTEGER, OUT calitate DOUBLE, OUT earnings DOUBLE, OUT ecpc DOUBLE, OUT ecmp DOUBLE)
        BEGIN
          SET @ecpc_tmp = 0;
          SELECT count(1) into visitors FROM `reports` WHERE user_id=_user_id AND DATE(created_at) >= DATE(_start_date) AND DATE(created_at) <= DATE(_end_date);
          SELECT count(1) into clicks FROM `reports` WHERE user_id=_user_id AND quality = 1 AND DATE(created_at) >= DATE(_start_date) AND DATE(created_at) <= DATE(_end_date);
          SELECT avg(quality) into calitate FROM `reports` WHERE user_id=_user_id AND DATE(created_at) >= DATE(_start_date) AND DATE(created_at) <= DATE(_end_date);
          SELECT sum(price) into earnings FROM `reports` WHERE user_id=_user_id AND DATE(created_at) >= DATE(_start_date) AND DATE(created_at) <= DATE(_end_date);
          SELECT sum(price) / count(1) into @ecpc_tmp FROM `reports` WHERE user_id=_user_id AND quality = 1  AND DATE(created_at) >= DATE(_start_date) AND DATE(created_at) <= DATE(_end_date);
          SET ecmp = @ecpc_tmp * 1000;
          SET ecpc = @ecpc_tmp;
        END
SQL;
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP PROCEDURE IF EXISTS reports";
        DB::connection()->getPdo()->exec($sql);
    }
}
