<?php namespace App\Repositories\Superadmin\Nomenclatoare\Emails;

use App\Models\Email;

class Form extends \App\Comptechsoft\Form\Form
{

	public function __construct($action, $id)
	{
		parent::__construct($action, $id);
		
		$this->substantiv = 'Email';

		$this->captions = [
	        'insert' => 'Adăugare ' . $this->substantiv,
	        'update' => 'Modificare ' . $this->substantiv . ' (#' . $this->id . ')',
	        'delete' => 'Ştergere ' . $this->substantiv,
	    ];

	    if( $id )
	    {
	    	$this->record(Email::find( (int) $id ));

			$this->setParameter('file','<div id="insert"></div>');
	    }

	    $this->view('superadmin.nomenclatoare.emails.form.index');


	}
}