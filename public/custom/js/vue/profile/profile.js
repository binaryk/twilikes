var config = {
    //base: "http://dashboard.twilikes.com/"
    //base: "http://localhost/twilikes/public/"
    //base: "http://supertare.theviralityhey.me/"
}



var Profile =  new Vue({
    el: '#profile',
    ready: function(){
    },
    data:{
        base: null,
        subdomain: '',
        errMsg: null,
        successMsg: null
    },
    methods: {
        showModal: function(){
            $('#myModal').modal('show');

        },
        showNotice: function() {
             $('#todayNotice').modal('show');
        },
        submit: function(){
            var self = this;

            $.ajax({
                url: self.base + "/api/profile/subdomain",
                type: 'post',
                dataType: 'json',
                async: false,
                data: {
                    subdomain: self.subdomain
                },
                success: function (data) {
                    if(data.code == 200){
                        self.errMsg = null;
                        self.successMsg = data.msg;
                        self.errMsg = null;
                        //$('#myModal').modal('hide');
                        //location.href = self.base;
                    }else{
                        self.errMsg = data.msg;
                        self.successMsg = null;
                    }

                }
            });
        },
        close: function(){
            self.errMsg = null;
            self.successMsg = null;
            $('#myModal').modal('hide');
            $('#todayNotice').modal('hide');
        },
        agent:function(link){
            var self = this;
            $.ajax({
                url: self.cors + 'api/client-agent',
                type: 'get',
                dataType: 'json',
                async: false,
                headers: { 'Access-Control-Allow-Origin' : '*' },
                data: {
                    user_id: self.user_id,
                    post_id: self.post_id,
                    domain_id: self.domain_id,
                    user_post_id: self.user_post_id,
                },
                success: function (data) {
                    location.href = link;
                },
                complete:function(){
                    //location.href = link;
                }
            });
        }
    }
})