@include('dashboard.tile')
<dashboard></dashboard>

<div class="col-md-8 col-sm-8 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Announcements</h2>
            <ul class="nav navbar-right panel_toolbox">
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <ul class="list-unstyled timeline">
                @foreach($announcements as $announcement)
                    <li>
                        <div class="block">
                            <div class="tags">
                                <a href="" class="tag">
                                    <span>News</span>
                                </a>
                            </div>
                            <div class="block_content">
                                <h2 class="title">
                                    <a>{!! $announcement->title !!}</a>
                                </h2>
                                <div class="byline">
                                </div>
                                <p class="excerpt">{!! $announcement->description !!}</a>
                                </p>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>