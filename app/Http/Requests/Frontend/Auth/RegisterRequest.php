<?php

namespace App\Http\Requests\Frontend\Auth;

use App\Http\Requests\Request;

/**
 * Class RegisterRequest
 * @package App\Http\Requests\Frontend\Access
 */
class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'fbPrintScreen' => 'required|image',
            'fbPage' => ['required','regex:/^((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?$/'],
        ];
    }

    public function messages()
    {
        return [
            'fbPage.required' => 'Fanpage URL field is required.',
            'fbPage.regex' => 'Fanpage URL field must be an url.',
            'fbPrintScreen.required' => 'Fanpage Activity Log Print Screen field is required.',
            'fbPrintScreen.image' => 'Fanpage Activity Log Print Screen must be an image.'
        ];
    }
}
