<?php

Route::group(['namespace' => 'Intermediar', 'middleware' => 'web'], function () {

    Route::get('client/{user_id}/{post_id}/{domain_id}/{user_post_id}', 'IntermediarController@index')
        ->name('intermediar.index');

});

Route::group(['namespace' => 'Intermediar', 'middleware' => 'cors'], function () {

    Route::get('api/client-agent', 'IntermediarController@agent')
        ->name('api.intermediar.agent');

    Route::get('api/client-alive', 'IntermediarController@alive')
        ->name('api.intermediar.alive');


});