@extends('template.~template')

@section('content')
<div class="row" id="profile">
    <div class="col-md-10 col-md-offset-1">

        <div class="panel panel-default">
            <div class="panel-heading">{{ trans('navs.frontend.dashboard') }}</div>

            <div class="panel-body">
                <div role="tabpanel">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">{{ trans('navs.frontend.user.my_information') }}</a>
                        </li>
                    </ul>

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="profile">
                            <table class="table table-striped table-hover table-bordered dashboard-table">
                                <tr>
                                    <th>{{ trans('labels.frontend.user.profile.avatar') }}</th>
                                    <td><img src="{!! $user->picture !!}" class="user-profile-image" /></td>
                                </tr>
                                <tr>
                                    <th>{{ trans('labels.frontend.user.profile.name') }}</th>
                                    <td>{!! $user->name !!}</td>
                                </tr>
                                <tr>
                                    <th>{{ trans('labels.frontend.user.profile.email') }}</th>
                                    <td>{!! $user->email !!}</td>
                                </tr>
                                <tr>
                                    <th>{{ trans('labels.frontend.user.profile.country') }}</th>
                                    <td>{!! $user->country !!}</td>
                                </tr>
                                <tr>
                                    <th>{{ trans('labels.frontend.user.profile.city') }}</th>
                                    <td>{!! $user->city !!}</td>
                                </tr>
                                <tr>
                                    <th>{{ trans('labels.frontend.user.profile.address') }}</th>
                                    <td>{!! $user->address !!}</td>
                                </tr>
                                <tr>
                                    <th>{{ trans('labels.frontend.user.profile.subdomain') }}</th>
                                    <td>{!! $user->subdomain !!}</td>
                                </tr>
                                <tr>
                                    <th>{{ trans('labels.frontend.user.profile.created_at') }}</th>
                                    <td>{!! $user->created_at !!} ({!! $user->created_at->diffForHumans() !!})</td>
                                </tr>
                                <tr>
                                    <th>{{ trans('labels.frontend.user.profile.last_updated') }}</th>
                                    <td>{!! $user->updated_at !!} ({!! $user->updated_at->diffForHumans() !!})</td>
                                </tr>
                                <tr>
                                    <th>{{ trans('labels.general.actions') }}</th>
                                    <td>
                                        <a href="{!! route('frontend.user.profile.edit') !!}" class="btn btn-primary btn-xs" id="editInformation">{{ trans('labels.frontend.user.profile.edit_information') }}</a>

                                        @if ($user->canChangePassword())
                                            <a href="{!! route('auth.password.change') !!}" class="btn btn-warning btn-xs">{{ trans('navs.frontend.user.change_password') }}</a>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div><!--tab panel profile-->

                    </div><!--tab content-->

                </div><!--tab panel-->

            </div><!--panel body-->

        </div><!-- panel -->

    </div><!-- col-md-10 -->
    <!-- Modal -->
    <div class="modal fade" id="myModal" data-toggle="modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" @click="close()"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Subdomain title</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" v-show="errMsg">
                        @{{errMsg}}
                    </div>
                    <div class="alert alert-success" v-show="successMsg">
                        @{{successMsg}}
                    </div>
                   <div class="form-group" v-bind:class="{ 'has-error': errMsg }">
                       <label for="subdomain" class="control-label col-md-3">Subdomain</label>
                       <input type="text" class="form-control col-md-9" id="subdomain" v-model="subdomain" placeholder="funny...">
                   </div>

                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="save_subdomain" @click="submit()">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')
    <script src="{!! asset('components/vue/dist/vue.js') !!}"></script>
    <script src="{!! asset('custom/js/vue/profile/profile.js') !!}"></script>

    <script type="text/javascript">
        @if(@$firstLogin)
            Profile.base = "{!! url('/') !!}"
            Profile.showModal();
        @endif
    </script>
@stop