<?php namespace App\Models;


use App\Models\Access\User\User;
use App\Models\Nomenclatoare\Post;
use Carbon\Carbon;

class Report extends \App\Comptech\Model\Model
{


    protected $table = 'reports';
    protected $guarded = ['id'];

    public static function Combobox()
    {
        $result = ['0' => trans('actions.no-selection')];
        $records = self::orderBy('id')->get();
        foreach( $records as $i => $record )
        {
            $result[$record->id] = $record->categorie;
        }
        return $result;

    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function user_post()
    {

    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            self::incUserAmount($model->user_id, $model->price);
            self::incPostViews($model->post_id, $model->quality);


        });

        static::updating(function($model)
        {
            self::incPostViews($model->post_id, $model->quality);
            if( $model->price > 0){
                self::incUserAmount($model->user_id, $model->price);
            }
        });
    }

    public static function incUserAmount($user_id, $amount){

        $user = User::find($user_id);
//        $user->unpaid = floatval(number_format($amount, 4)) + floatval(number_format($user->unpaid, 4));
//        $user->unpaid = floatval(number_format($user->unpaid, 4));

        if ($user->unpaid >= 20) {
//            $user->payment_status = "pending";
        }

        //$user->save();

        if ($user->invited_by && $amount > 0) {
            $invitedByUser = User::find($user->invited_by);
            $summ = config('twilikes.referral') * $amount;
            $summ = floatval($summ);
//            $invitedByUser->unpaid = floatval($invitedByUser->unpaid) + $summ;
            $invitedByUser->save();
            Referral::create([
                'data' => Carbon::now()->toDateString(),
                'invited_by_id' => $invitedByUser->id,
                'invited_id' => $user->id,
                'earning' => $summ
            ]);
        }


    }

    public static function incPostViews($post_id, $quality)
    {
        $date = Carbon::now()->toDateString();
        $post_view = PostView::where('post_id', $post_id)
        ->where('date', $date)
        ->where('quality', $quality)
        ->first();

        if( $post_view ) {
            $post_view->views  += 1;
            $post_view->save();
            return $post_view;

        } else {

            $post_view = PostView::create([
                'post_id' => $post_id,
                'quality' => $quality,
                'date' => $date,
                'views' => 1
            ]);

            return $post_view;


        }


    }

}
