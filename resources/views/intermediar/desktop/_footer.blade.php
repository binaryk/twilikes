<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script type="text/javascript">
    var or = 1;
    $(document).ready(function(){

        if($(window).width() < 975)
        {
            order();
        }else{

            reorder();
        }
        $(window).resize(function (){
            if($(window).width() < 975)
            {
                order();
            }else{
                reorder();
            }

        });
    });

    function order()
    {
        if(or == 1){
            var title = $('.title_subtitle').html();
            var image = $('.image').html();
            $('.title_subtitle').html(image).css('padding', '0');
            $('.image').html(title).css({'padding' : '15px', 'color' : 'green'});
            $('.view_gallery').parent().hide();
            $('#start_button').hide();
            or = 2;
        }

    }

    function reorder()
    {
        if(or == 2){
            var title = $('.title_subtitle').html();
            var image = $('.image').html();
            $('.title_subtitle').html(image).css('padding', '15px');
            $('.image').html(title).css({'padding' : '0px', 'color' : 'black'});
            $('.view_gallery').parent().show();
            $('#start_button').show();
            or = 1;
        }
    }
</script>

{!!Html::script('//cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.9.4/spin.min.js')!!}
{!!Html::script('//cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.9.4/ladda.min.js')!!}
@include('intermediar._vue_vars')
</body>
</html>
