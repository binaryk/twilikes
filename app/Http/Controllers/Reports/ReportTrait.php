<?php
/**
 * Created by PhpStorm.
 * User: lupac
 * Date: 5/20/2016
 * Time: 5:39 PM
 */

namespace App\Http\Controllers\Reports;

use App\Models\Areport;
use Carbon\Carbon;
use DateTime;

trait ReportTrait
{

    public function obtainDataPerDays($start_date, $end_date)
    {
        $cStartDate = Carbon::createFromFormat('Y-m-d',$start_date);
        $cEndDate = Carbon::createFromFormat('Y-m-d',$end_date);
        $diff = $cEndDate->diffInDays($cStartDate) + 1 ;
        $data = [];

        if($diff > 0){

            for($i = 0; $i < $diff; $i++){
                $currentData = new Carbon($cStartDate);
                $currentData->toDateString();
                $tmp = [];
                $tmp['date'] = $currentData;
                $tmp['data'] = Areport::where('user_id',access()->user()->id)->whereDate( 'created_at','=',$currentData->toDateString())->get();
                $data[] = $tmp;
                $cStartDate->addDay();
            }
        }

        return $data;
    }

}