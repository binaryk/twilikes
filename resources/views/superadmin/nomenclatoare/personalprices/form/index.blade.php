<div class="row">

    <div class="col-md-12 form-group">
        {!!
        App\Comptechsoft\Ui\Controls\Selectbox::make()
        ->label('Country')
        ->icon('')
        ->wrapper('col-sm-10')
        ->lclass('col-sm-2')
        ->name('country_id')
        ->value($action != 'insert' ? $record->country_id : '')
        ->addClass('form-control form-data-source')
        ->withAttribute('data-field', 'country_id')
        ->withAttribute('data-type', 'combobox')
        ->disabled($action == 'delete')
        ->options(App\Models\Country::Combobox())
        ->render()
        !!}
    </div>

    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Textbox::make()
            ->name( $field = 'mobile_price')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Mobile price($)' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->mobile_price : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => '15.5',
                'data-field'  => $field,
                'data-type'   => 'textbox'
            ])
            ->help('')
            ->render()
        !!}
	</div>

    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Textbox::make()
            ->name( $field = 'desktop_price')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Desktop price($)' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->desktop_price : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => '12.4',
                'data-field'  => $field,
                'data-type'   => 'textbox'
            ])
            ->help('')
            ->render()
        !!}
	</div>
  </div>
</div>