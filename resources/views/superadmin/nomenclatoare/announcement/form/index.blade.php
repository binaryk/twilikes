<div class="row">

    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Textbox::make()
            ->name( $field = 'title')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Name' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->title : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => 'Title...',
                'data-field'  => $field,
                'data-type'   => 'textbox'
            ])
            ->help('')
            ->render()
        !!}
	</div>

    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Editbox::make()
            ->name( $field = 'description')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Description' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->description : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => 'Description....',
                'data-field'  => $field,
                'data-type'   => 'textbox'
            ])
            ->help('')
            ->render()
        !!}
	</div>
  </div>
</div>