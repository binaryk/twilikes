<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use App\Models\ArticlesEarning;
use App\Models\NewReports;
use App\Models\Areport;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Jenssegers\Agent\Agent;

/**
 * Class LanguageController
 * @package App\Http\Controllers
 */
class ReportsController extends Controller
{
    use ReportTrait;
    /**
     * @param $lang
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index()
    {
        return view('reports.index')->withDate(Carbon::now());
    }

    public function data()
    {
        $agent = new Agent();
        $start_date = Input::get('startDate');
        $end_date   = Input::get('endDate');
        $end_carbon_client_date = Carbon::createFromFormat('Y-m-d',$end_date);
        $server_now = Carbon::now();

        if($server_now->lte($end_carbon_client_date)) {

            /*
             * Aici suntem in situatia de a returna si ziua curenta din reports live
             * am pus <= fiindca poate fi asa ca ora userului sa fie gen: 12:30, data de 11 octombrie, dar serverul sa aiba ora 10:30 data de 10 octombrie
             * now -> ora serverului
             * ->$end_carbon_client_date - ora clientului
             * */
/*            $today_data = Areport::select('price')->where('user_id', '=', access()->user()->id)
//                ->whereDate('created_at','>',Carbon::yesterday()->toDateString())
                ->whereDate('created_at','=',$end_date)
                ->get();
            $today['clicks'] = $today_data->count();
            $today['clicks_calitate'] = $today_data->filter(function($item){
                return $item->price > 0;
            })->count();
            $today['quality'] = $today['clicks_calitate'] / $today['clicks'];
            $today['earnings'] = $today_data->sum('price');

            if( $today['quality']){
                $today['ecpc'] = $today['earnings'] / ($today['clicks'] * $today['quality']);
            } else {
                $today['ecpc'] = 0;
            }

            $today['ecpm'] = $today['ecpc'] * 1000;
            $today['date'] = Carbon::now()->format('Y-m-d');*/
            DB::setFetchMode(\PDO::FETCH_ASSOC);


            $today_data = DB::table('areports')
                ->select(DB::raw('
                FLOOR(count(1)) as clicks,
                ROUND(count(1) * avg(quality),0) as clicks_calitate,
                ROUND(avg(quality) * 100,2) as quality,
                ROUND(sum(price),3) as earnings,
                ROUND((sum(price) / (count(1) * avg(quality))),3) as ecpc,
                ROUND((sum(price) * 1000 / (count(1) * avg(quality))),3)  as ecpm,
                created_at,
                date(created_at) as date
            '))
                ->where('user_id', '=', access()->user()->id)
//                ->whereDate('created_at','>',Carbon::yesterday()->toDateString())
                ->whereDate('created_at','=',$end_date)
                ->get();



            DB::setFetchMode(\PDO::FETCH_ASSOC);

            $data = NewReports::where('user_id', access()->user()->id)
                ->whereDate('created_at','>=',$start_date)
                ->whereDate('created_at','<=',Carbon::createFromFormat('Y-m-d',$end_date)->subDay()->toDateString())
                ->get();

            if($today_data && array_key_exists(0, $today_data)){
                $object_with_today_report = new NewReports($today_data[0]);
                $data->push($object_with_today_report);
            }



        } else {
            $data = NewReports::where('user_id', access()->user()->id)
                ->whereDate('created_at','>=',$start_date)
                ->whereDate('created_at','<=',$end_date)
                ->get();
        }

        $totals['clicks'] = 0;
        $totals['clicks_calitate'] = 0;
        $totals['quality'] = 0;
        $totals['earnings'] = 0;
        $totals['ecpc'] = 0;
        $totals['ecpm'] = 0;

        foreach($data as $item) {
            $totals['clicks'] += $item->clicks;
            $totals['clicks_calitate'] += $item->clicks_calitate;
            $totals['quality'] += $item->quality;
            $totals['earnings'] += $item->earnings;
            $totals['ecpc'] += $item->ecpc;
            $totals['ecpm'] += $item->ecpm;
        }

        if(count($data) > 0){
            $totals['ecpc'] = $totals['ecpc']  / count($data);
            $totals['ecpm'] = $totals['ecpm']  / count($data);
            $totals['quality'] = $totals['quality'] / count($data);
            $totals['ecpc'] = number_format($totals['ecpc'], 3);
            $totals['ecpm'] = number_format($totals['ecpm'], 3);
            $totals['quality'] = number_format($totals['quality'], 3);
        }


        if(! $totals){
            $totals['clicks'] = 0;
            $totals['clicks_calitate'] = 0;
            $totals['quality'] = 0;
            $totals['earnings'] = 0;
            $totals['ecpc'] = 0;
            $totals['ecpm'] = 0;
        }

        return success(['data' => $data, 'totals' => $totals, 'isMobile' => $agent->isMobile()]);

    }

    public function articles()
    {

        $start_date = Input::get('startDate');
        $end_date   = Input::get('endDate');
        $LIMIT = 5;

        $data = ArticlesEarning::select(DB::raw('
                photo,
                title,
                sum(clicks) as clicks,
                sum(clicks_calitate) as clicks_calitate,
                ROUND(sum(clicks_calitate) * 100 / sum(clicks) ,2) as quality,
                ROUND(sum(earnings),3) as earnings,
                IFNULL( sum(ecpc) / count(1), 0) as ecpc,
                IFNULL( sum(ecpc) * 1000 / count(1) ,0 ) as ecpm,
                user_post_created_at as created_at,
                DATE_FORMAT(user_post_created_at, "%m-%d-%Y") as date
            '))
            ->where('user_id',access()->user()->id)
            ->whereDate('user_post_created_at','>=',$start_date)
            ->whereDate('user_post_created_at','<=',$end_date)
            ->whereDate('reports_created_at','>=',$start_date)
            ->whereDate('reports_created_at','<=',$end_date)
            ->groupBy('post_id')
            ->orderBy('user_post_created_at','DESC')
            ->paginate($LIMIT);

        $totals['clicks'] = 0;
        $totals['clicks_calitate'] = 0;
        $totals['quality'] = 0;
        $totals['earnings'] = 0;
        $totals['ecpc'] = 0;
        $totals['ecpm'] = 0;

        foreach($data->items() as $item) {
            $totals['clicks'] += $item->clicks;
            $totals['clicks_calitate'] += $item->clicks_calitate;
            $totals['quality'] += $item->quality;
            $totals['earnings'] += $item->earnings;
            $totals['ecpc'] += $item->ecpc;
            $totals['ecpm'] += $item->ecpm;
        }

        if(count($data) > 0) {
            $totals['ecpc'] = $totals['ecpc']  / count($data);
            $totals['ecpm'] = $totals['ecpm']  / count($data);
            $totals['quality'] = $totals['quality'] / count($data);
            $totals['ecpc'] = number_format($totals['ecpc'], 3);
            $totals['ecpm'] = number_format($totals['ecpm'], 3);
            $totals['quality'] = number_format($totals['quality'], 3);
        }



        return success(['data' => $data, 'totals' => $totals]);
    }



}