<div class="row">

    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Textbox::make()
            ->name( $field = 'title')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Name' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->title : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => 'Title...',
                'data-field'  => $field,
                'data-type'   => 'textbox'
            ])
            ->help('')
            ->render()
        !!}
	</div>

    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Editbox::make()
            ->name( $field = 'sidebar_code')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Sidebar code' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->sidebar_code : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => 'Title...',
                'data-field'  => $field,
                'data-type'   => 'textbox'
            ])
            ->help('')
            ->render()
        !!}
	</div>

    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Editbox::make()
            ->name( $field = 'bottom_code')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Bottom code' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->bottom_code : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => 'Title...',
                'data-field'  => $field,
                'data-type'   => 'textbox'
            ])
            ->help('')
            ->render()
        !!}
	</div>

    <div class="col-md-12 form-group">
        {!!
        App\Comptechsoft\Ui\Controls\Selectbox::make()
        ->label('Visible for users')
        ->icon('')
        ->wrapper('col-sm-10')
        ->lclass('col-sm-2')
        ->name('visible')
        ->value(@$record->visible)
        ->addClass('form-control form-data-source')
        ->withAttribute('data-field', 'visible')
        ->withAttribute('data-type', 'combobox')
        ->disabled(false)
        ->options(@\App\Models\Nomenclatoare\Domain::available())
        ->render()
        !!}
    </div>
@if($action == 'update')
        <div class="col-md-12 form-group">
            {!!
            App\Comptechsoft\Ui\Controls\Selectbox::make()
            ->label('Available')
            ->icon('')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->name('payment_status')
            ->value(@$record->available)
            ->addClass('form-control form-data-source')
            ->withAttribute('data-field', 'available')
            ->withAttribute('data-type', 'combobox')
            ->disabled(false)
            ->options(@\App\Models\Nomenclatoare\Domain::available())
            ->render()
            !!}
        </div>
@endif
  </div>
</div>