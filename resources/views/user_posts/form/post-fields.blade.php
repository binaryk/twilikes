<div class="">
    <div class="row center">
        <div class="col-lg-6 col-md-8 col-sm-12 form-group" v-bind:class="{'has-error' : post.title.length === 0 && validationIsActivated}">

            {!!
                App\Repositories\Ui\Controls\Textbox::make()
                ->name( $field = 'title')
                ->class('form-group mb')
                ->wrapper('col-sm-10')
                ->lclass('col-sm-2')
                ->caption( 'Title' )
                ->disabled($action == 'delete')
                ->icon(NULL)
                ->value( $action != 'insert' ? $record->title : '')
                ->attributes([
                    'id'          => $field,
                    'class'       => 'form-control form-data-source',
                    'placeholder' => 'Title...',
                    'data-field'  => $field,
                    'data-type'   => 'textbox',
                    'v-model'     => $action === 'insert' ? 'post.title' : '',
                ])
                ->help('')
                ->render()
            !!}
            @if($action === 'insert')
                <span class="help error" v-if="post.title.length === 0 && validationIsActivated">Title field is required</span>
            @endif
        </div>
    </div>

    @if($action === 'insert')
    <div class="row center">
        <div class="col-lg-6 col-md-8 col-sm-12 form-group">
            {!!
            App\Comptechsoft\Ui\Controls\Selectbox::make()
            ->label('Category')
            ->icon('')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->name('category')
            ->value($action != 'insert' ? $record->category : '')
            ->addClass('form-control form-data-source')
            ->withAttribute('data-field', 'category')
            ->withAttribute('data-type', 'combobox')
            ->withAttribute('v-model', 'post.category')
            ->disabled($action == 'delete')
            ->options(App\Models\Nomenclatoare\Category::category())
            ->render()
            !!}
        </div>
    </div>

    <div class="row center">
        <div class="row form-group" v-bind:class="{'has-error' : post.description.length === 0 && validationIsActivated}">
            <div class="col-lg-6 col-md-8 col-sm-12" style="border-bottom: 1px dashed #eee;">
                {!!
                App\Repositories\Ui\Controls\Editbox::make()
                ->name( $field = 'description')
                ->class('form-group mb')
                ->wrapper('col-sm-10')
                ->lclass('col-sm-2')
                ->caption( 'Description' )
                ->placeholder('')
                ->icon(NULL)
                ->value( $action != 'insert' ? $record->{$field} : '')
                ->attributes([
                    'id'          => $field,
                    'class'       => 'form-control form-data-source  note-editor note-editor-margin',
                    'placeholder' => 'Description',
                    'data-field'  => $field,
                    'data-type'   => 'textbox',
                    'v-model'     => 'post.description',
                    'rows' => '3',

                ])
                ->setFlag( 'readonly', $action == 'delete' )
                ->help('')
                ->render()
            !!}
            </div>
            <span class="help error" v-if="post.description.length === 0 && validationIsActivated">Description field is required</span>
        </div>
    </div>

    <div class="row center">
        <div class="form-group col-lg-6 col-md-8 col-sm-12" id="image-upload-box" v-bind:class="{'has-error' : post.photo === false && validationIsActivated && !linkIsValid}">
            <label class="col-sm-2 control-label">Cover Image (og:image)</label>

            <div class="photo-container" v-if="! linkIsValid">
                <input type="file" name="photo" class="form-control filestyle" id="file">
            </div>

            <div class="col-md-8 col-lg-6 extern-link" v-show="! existsUploadedPhoto" v-bind:class="{'has-error' : post.photo === false && validationIsActivated && !linkIsValid, 'validLink' : linkIsValid}">
                <div class="input-group" v-if="!!! linkIsValid" v-bind:class="{'has-error' : linkIsValid === false}">
                    <input type="text" class="form-control" v-model="post.link" placeholder="Paste image URL" v-on:keyup.enter="external">
                     <span class="input-group-btn">
                            <button class="confirm-btn btn btn-primary" aria-hidden="true" type="button" @click='external'> <i class="fa fa-plus"></i></button>
                     </span>
                </div>
                <span    class="error" v-if="!! post.link && linkIsValid === false">Link is invalid</span>
                <div class="uploaded-image" v-if="linkIsValid"></div>
            </div>

            <span class="help error" v-if="post.description.length === 0 && validationIsActivated && !linkIsValid">Cover image is required</span>
        </div>
    </div>

    @endif
</div>