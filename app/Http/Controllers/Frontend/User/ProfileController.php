<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\User\UpdateProfileRequest;
use App\Models\Access\User\User;
use App\Repositories\Frontend\Access\User\UserRepositoryContract;
use Illuminate\Support\Facades\Input;

/**
 * Class ProfileController
 * @package App\Http\Controllers\Frontend
 */
class ProfileController extends Controller
{
    /**
     * @return mixed
     */
    public function edit()
    {
        return view('frontend.user.profile.edit')
            ->withUser(access()->user());
    }

    /**
     * @param  UserRepositoryContract         $user
     * @param  UpdateProfileRequest $request
     * @return mixed
     */
    public function update(UserRepositoryContract $user, UpdateProfileRequest $request)
    {
        $user->updateProfile(access()->id(), $request->all());
        return redirect()->route('frontend.user.dashboard')->withFlashSuccess(trans('strings.frontend.user.profile_updated'));
    }

    public function subdomain()
    {
        $subdomain = str_replace(" ","",Input::get('subdomain'));
        $subdomain = trim($subdomain);
        $subdomain = strtolower($subdomain);
        if(User::where('subdomain',$subdomain)->where('id','!=',access()->user()->id)->first() || strlen($subdomain) < 1){
            return error("This name is already in use, please choose another name.");
        }
        auth()->user()->subdomain = $subdomain;
        auth()->user()->save();
        return success([],"The subdomain name was successfully saved.");
    }
}