import {Dashboard} from "./dashboard/dashboard.vue";
import {Reports} from "./reports/index";
import {Campaigns} from "./campaigns/campaigns.vue";
import {Modal} from "./general/modal.vue"

Vue.config.debug = true;
