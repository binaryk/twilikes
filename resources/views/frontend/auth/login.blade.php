@extends('frontend.layouts.master')
@section('content')
    <div class="panel panel-dark panel-flat">
        <div class="panel-heading text-center">
            <a href="#">
                <img src="{!! asset('img/logo.png') !!}" alt="Image" class="block-center img-rounded">
            </a>
        </div>
        <div class="panel-body">
            <p class="text-center pv">SIGN IN TO CONTINUE.</p>
            {!! Form::open(['url' => 'login', 'role' =>"form", 'data-parsley-validate' =>"", 'novalidate' =>'', 'class' =>"mb-lg"]) !!}
                <div class="form-group has-feedback">
                    {!! Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email')]) !!}
                    <span class="fa fa-envelope form-control-feedback text-muted"></span>
                </div>
                <div class="form-group has-feedback">
                    {!! Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password')]) !!}
                    <span class="fa fa-lock form-control-feedback text-muted"></span>
                </div>
                <div class="clearfix">
                    <div class="checkbox c-checkbox pull-left mt0">
                        <label>
                            <input type="checkbox" value="" name="remember">
                            <span class="fa fa-check"></span>Remember Me</label>
                    </div>
                    <div class="pull-right"><a href="{!! url('password/reset') !!}" class="text-muted">Forgot your password?</a>
                    </div>
                </div>
                <button type="submit" class="btn btn-block btn-primary mt-lg">Login</button>
            {!! Form::close() !!}
            <p class="pt-lg text-center">Need to Signup?</p><a href="{!! url('register') !!}" class="btn btn-block btn-default">Register Now</a>
        </div>
    </div>
@stop