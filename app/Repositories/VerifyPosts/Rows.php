<?php namespace App\Repositories\Superadmin\Nomenclatoare\VerifyPosts;

use \App\Models\Nomenclatoare\Post;
use \App\Comptechsoft\Datatable\Cell;

class Rows extends \App\Comptechsoft\Datatable\Rows
{

	public function __construct()
	{
		$this->searcheableColumns([1 => 'id', 2 => 'title', 3 => 'description', 4 => 'user_id', 7 => 'view_for_all',]);
		$this->orderableColumns([1 => 'id', 2 => 'title', 3 => 'category', 4 => 'location', 5 => 'user_id', 6 => 'user_confirmation', 7 => 'view_for_all']);

		$this->addCells([
			'rec-no'     => (new Cell())->source(function($record){ return $record->properties->recCount . '.'; }),
			'id'         => (new Cell())->source(function($record){ return $record->id; }),
			'title'      => (new Cell())->source(function($record){ return $record->title; }),
			'description'=> (new Cell())->source(function($record){ return $record->description; }),
			'username'=> (new Cell())->source(function($record){ return $record->user->name; }),
			'status'=> (new Cell())->source(function($record){ return $record->status_label; }),
			'link'=> (new Cell())->source(function($record){ return $record->location; }),
			'view_for_all'=> (new Cell())->source(function($record){ return pingpong($record->view_for_all); }),
			'category'=> (new Cell())->source(function($record){ return @\App\Models\Nomenclatoare\Category::category()[$record->category]; }),
			'actions'    => (new Cell())->source(function($record){

				return 
					view('superadmin.nomenclatoare.verify_posts.grid.actions')
					->withRecord($record)
					->withUpdateRoute(\URL::route('superadmin.nomenclatoare.verify_posts.get-action-form', ['action' => 'update', 'id' => $record->id]))
					->withDeleteRoute(\URL::route('superadmin.nomenclatoare.verify_posts.get-action-form', ['action' => 'delete', 'id' => $record->id]))
					->withSourceRoute(\URL::route('superadmin.nomenclatoare.verify_posts.source', ['id' => $record->id]))
					->render();
			}),
		]);
	}

	/*
	 * Toate inregistrarile din tabela ferme 
	 */
	protected function totalRecordsCount()
	{
		return Post::where('user_id','!=',0)
			->where('successfully_submited',1)->count();
	}

	/*
	 * Toate inregistrarile obtinute in urma unei filtrari Datatable
	 */
	protected function filteredRecordsCount()
	{
		/*
		 * WhereRaw
		 */
		$where = $this->where();
		if( ! $where )
		{
			return Post::where('user_id','!=',0)
				->where('successfully_submited',1)/*->whereRaw($where)*/->count();
		}
		return Post::where('user_id','!=',0)
			->where('successfully_submited',1)->whereRaw($where)->count();
	}

	/*
	 * Setul de date care trebuie trimis catre Datatable
	 */
	protected function getDataSet()
	{
		/*
		 * OrderBy
		 */
		$order_criteria = $this->orderBy();
		$result = 
			Post::with('user')->where('user_id','!=',0)
				->where('successfully_submited',1)
				->orderBy( $order_criteria['field'], $order_criteria['direction'] )
			;
		/*
		 * WhereRaw
		 */
		$where = $this->where();
		if( ! $where )
		{
			return $result->skip($this->start())->take($this->length())->get();
		}
		return $result->whereRaw($where)->skip($this->start())->take($this->length())->get();
	}
}