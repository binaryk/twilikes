<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Message;
use Illuminate\Support\Facades\Input;

/**
 * Class LanguageController
 * @package App\Http\Controllers
 */
class MessageController extends Controller
{
    public function index()
    {
        return view('admin.message.index')->withMessage(Message::first());
    }

    public function store()
    {
        $data = Input::all();
        $message = Message::first();
        if($message){
            $message->message = $data['message'];
            $message->save();
        }else{
            Message::create($data);
        }
        return redirect()->back()->withFlashSuccess("With success");
    }

}