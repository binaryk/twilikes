<?php

/**
 * Global helpers file with misc functions
 *
 */
use Illuminate\Support\Facades\DB;

if (! function_exists('logquery')) {
    /**
     * Get the fallback locale
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    function logquery($file = '')
    {
        return true;
    }
}

/**
 * Global helpers file with misc functions
 *
 */
if (! function_exists('codecountry')) {
    /**
     * Get the fallback locale
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    function codecountry($ip)
    {
        if(env("APP_ENV") == "dev"){
            return "OTH";
        }
        $data = null;
        exec ( public_path("data/geolocation/pl/ip2location.pl") ." -datafile ".public_path('data/geolocation/IP-COUNTRY.BIN')." -ip ".$ip." -format xml", $data );
        if($data) {
            $str = $data[3];
            if ($str) {
                $xml = simplexml_load_string($data[3]);
                $json = json_encode($xml);
                $array = json_decode($json, TRUE);
                return $array[0];
            }
        }
        return null;
    }
}

/**
 * Global helpers file with misc functions
 *
 */
if (! function_exists('pingpong')) {
    /**
     * Get the fallback locale
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    function pingpong($value)
    {
        if($value){
            return '<span  class="badge badge-success" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
                        Yes
                    </span>';
        }else{
            return '<span  class="badge badge-error" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
                        No
                    </span>';
        }
    }
}

function isTransitionInterval() {
    return \Carbon\Carbon::now()->hour >= 0 && \Carbon\Carbon::now()->hour <= 2;
}
//
/**
 * Global helpers file with misc functions
 *
 */
if (! function_exists('normalFloat')) {
    /**
     * Get the fallback locale
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    function normalFloat($value, $zecimals)
    {
        return floatval(number_format($value, $zecimals,'.',''));
    }
}

/**
 * Global helpers file with misc functions
 *
 */
if (! function_exists('success')) {
    /**
     * Get the fallback locale
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    function success($data = [], $msg = 'Success')
    {
        return response()->json([
                'code' => 200,
                'msg' => $msg
            ] + $data);;
    }
}

if (! function_exists('error')) {
    /**
     * Get the fallback locale
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    function error($msg = 'Err')
    {
        return response()->json([
            'code' => 500,
            'msg' => $msg
        ]);;
    }
}

if (! function_exists('app_name')) {
    /**
     * Helper to grab the application name
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (! function_exists('access')) {
    /**
     * Access (lol) the Access:: facade as a simple function
     */
    function access()
    {
        return app('access');
    }
}

if (! function_exists('javascript')) {
    /**
     * Access the javascript helper
     */
    function javascript()
    {
        return app('JavaScript');
    }
}

if (! function_exists('gravatar')) {
    /**
     * Access the gravatar helper
     */
    function gravatar()
    {
        return app('gravatar');
    }
}

if (! function_exists('getFallbackLocale')) {
    /**
     * Get the fallback locale
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    function getFallbackLocale()
    {
        return config('app.fallback_locale');
    }
}

if (! function_exists('getLanguageBlock')) {

    /**
     * Get the language block with a fallback
     *
     * @param $view
     * @param array $data
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function getLanguageBlock($view, $data = [])
    {
        $components = explode("lang", $view);
        $current  = $components[0]."lang.".app()->getLocale().".".$components[1];
        $fallback  = $components[0]."lang.".getFallbackLocale().".".$components[1];

        if (view()->exists($current)) {
            return view($current, $data);
        } else {
            return view($fallback, $data);
        }
    }
}

function array_keys_exists($keys, $arr) {
    foreach($keys as $key ){
        if(! array_key_exists($key, $arr)) {
            return false;
        }
    }

    return true;
}