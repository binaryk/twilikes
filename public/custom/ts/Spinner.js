"use strict";
class Spinner {
    constructor(subiect) {
        this.spinner = $(subiect);
    }
    show() {
        console.log('show spinner');
        this.spinner.show();
    }
    hide(time, callback) {
        const _that = this;
        if (time) {
            setTimeout(function () {
                _that.spinner.hide();
                callback();
            }, time);
        }
        else {
            _that.spinner.hide();
        }
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Spinner;
//# sourceMappingURL=Spinner.js.map