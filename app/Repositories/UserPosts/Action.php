<?php namespace App\Repositories\UserPosts;

use App\Models\Nomenclatoare\Post;

class Action extends \App\Comptechsoft\Form\Action
{

	public function __construct($action, $id)
	{
		parent::__construct($action, $id);

		if( $action == 'insert' )
        {
            $record = new Post();
        }
        else
        {
            $record = Post::find( (int) $id);
        }

        $this->record($record);

		$this
			->addRule(['insert', 'update'], 'title', 'required', 'Title is required')
		;
	}
}