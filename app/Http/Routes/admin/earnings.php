<?php

Route::group(['namespace' => 'Superadmin', 'middleware' => 'admin'], function () {

    Route::get('superadmin/nomenclatoare/earnings', 'EarningsController@getGrid')
        ->name('superadmin.nomenclatoare.earnings.index');

    Route::post('superadmin/nomenclatoare/earnings-source', 'EarningsController@dataSource')
        ->name('superadmin.nomenclatoare.earnings.data-source');

    /*
     * Sa se incarce formularul de actiun (actions form)
     */
    Route::post('superadmin/nomenclatoare/earnings-get-action-form/{action}/{id?}', 'EarningsController@getActionForm')
        ->name('superadmin.nomenclatoare.earnings.get-action-form');

    /*
     * ce se intampla la Adauga/Salveaza/Sterge
     */
    Route::post('superadmin/nomenclatoare/earnings-action/{action}/{id?}', 'EarningsController@doAction')
        ->name('superadmin.nomenclatoare.earnings-do-action');
});