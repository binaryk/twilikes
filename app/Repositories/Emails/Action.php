<?php namespace App\Repositories\Superadmin\Nomenclatoare\Emails;

use App\Models\Email;

class Action extends \App\Comptechsoft\Form\Action
{

	public function __construct($action, $id)
	{
		parent::__construct($action, $id);

		if( $action == 'insert' )
        {
            $record = new Email();
        }
        else
        {
            $record = Email::find( (int) $id);
        }

        $this->record($record);

		$this
//			->addRule(['insert', 'update'], 'title', 'required', 'Titlul trebuie completat')
		;
	}
}