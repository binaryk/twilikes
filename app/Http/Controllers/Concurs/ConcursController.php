<?php namespace App\Http\Controllers\Concurs;

use App\Http\Controllers\Controller;
use App\Models\Areport;
use App\Models\NewReports;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
/**
 * Class LanguageController
 * @package App\Http\Controllers
 */
class ConcursController extends Controller
{

    public function top()
    {
        $start_date = Carbon::createFromDate(2016, 12, 1)->toDateString();
        $end_date   = Carbon::now()->toDateString();
        $today_data = null;



        if(Carbon::now()->day == 1) {
                $data_without_today = Areport::select(DB::raw('
                    count(1) as clicks,
                    user_id,
                    users.name
                '))
                ->leftJoin('users','areports.user_id','=','users.id')
                ->whereDate('areports.created_at','>=',$start_date)
                ->whereDate('areports.created_at','<=',Carbon::createFromFormat('Y-m-d',$end_date)->toDateString())
                ->where('quality',1)
                ->orderBy('clicks','DESC')
                ->groupBy('user_id')
                ->take(50)
                ->get()->toArray();

        } else {
            $data_without_today = NewReports::
            select(DB::raw('
                newreports.id,
                sum(newreports.clicks_calitate) as clicks,
                newreports.user_id,
                users.name
            '))
                ->leftJoin('users','newreports.user_id','=','users.id')
                ->whereDate('newreports.created_at','>=',$start_date)
                ->whereDate('newreports.created_at','<=',Carbon::createFromFormat('Y-m-d',$end_date)->subDay()->toDateString())
                ->orderBy('clicks','DESC')
                ->groupBy('newreports.user_id')
                ->take(50)
                ->get()->toArray();

            $today_data = Areport::select(DB::raw('
                    ROUND(count(1) * avg(quality),0) as clicks,
                    user_id
                '))
                ->whereDate('areports.created_at','=',$end_date)
                ->where('quality',1)
                ->orderBy('clicks','DESC')
                ->groupBy('user_id')
                ->take(50)
                ->get()->toArray();

        }



        
        foreach($data_without_today as $k => $data) {
            if($today_data) {
                $today = array_filter($today_data, function($el) use ($data) {
                    return $el['user_id'] == $data['user_id'];
                });
                if($today){
                    $data_without_today[$k]['clicks'] += intval(array_shift($today)['clicks']);
                }
            }

            $len = strlen($data_without_today[$k]['name']) - 4;
            $data_without_today[$k]['name'] = substr($data_without_today[$k]['name'],0, 2) . str_repeat('*', $len) . substr($data_without_today[$k]['name'], -2);

        }
        
        $collection = collect($data_without_today)->sortByDesc('clicks')->toArray();

        $toRetyrn = [];

        foreach($collection as $k => $coll) {
            if($k < 10) {
                $toRetyrn[] = $coll;
            }
        }

        return $toRetyrn;
    }
}
