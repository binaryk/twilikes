<?php namespace App\Repositories\Superadmin\Nomenclatoare\PersonalPrices;

use App\Models\Country;
use \App\Models\Nomenclatoare\PersonalPrice;
use \App\Comptechsoft\Datatable\Cell;

class Rows extends \App\Comptechsoft\Datatable\Rows
{

	public function __construct()
	{
		$this->searcheableColumns([1 => 'id', 2 => 'code', 3 => 'country']);
		$this->orderableColumns([1 => 'id', 2 => 'code']);

	}

	public function settings()
	{

		$this->addCells([
			'rec-no'     => (new Cell())->source(function($record){ return $record->properties->recCount . '.'; }),
			'id'         => (new Cell())->source(function($record){ return $record->id; }),
			'country'      => (new Cell())->source(function($record){ return @Country::Combobox()[$record->country_id]; }),
			'mobile_price'      => (new Cell())->source(function($record){ return $record->mobile_price; }),
			'desktop_price'      => (new Cell())->source(function($record){ return $record->desktop_price; }),
			'actions'    => (new Cell())->source(function($record){

				return
					view('__layouts.pages.datatable.cells.actions')
						->withRecord($record)
						->withUpdateRoute(\URL::route('superadmin.nomenclatoare.personalprices.get-action-form', ['action' => 'update','user_id' => $this->getParameter('user_id'), 'id' => $record->id]))
						->withDeleteRoute(\URL::route('superadmin.nomenclatoare.personalprices.get-action-form', ['action' => 'delete','user_id' => $this->getParameter('user_id'), 'id' => $record->id]))
						->render();
			}),
		]);
		return $this;
	}

	/*
	 * Toate inregistrarile din tabela ferme 
	 */
	protected function totalRecordsCount()
	{
		return PersonalPrice::count();
	}

	/*
	 * Toate inregistrarile obtinute in urma unei filtrari Datatable
	 */
	protected function filteredRecordsCount()
	{
		/*
		 * WhereRaw
		 */
		$where = $this->where();
		if( ! $where )
		{
			return PersonalPrice::count();
		}
		return PersonalPrice::whereRaw($where)->count();
	}

	/*
	 * Setul de date care trebuie trimis catre Datatable
	 */
	protected function getDataSet()
	{
		/*
		 * OrderBy
		 */
		$order_criteria = $this->orderBy();
		$result = 
			PersonalPrice::where('user_id', $this->getParameter('user_id'))->orderBy( $order_criteria['field'], $order_criteria['direction'] )
			;
		/*
		 * WhereRaw
		 */
		$where = $this->where();
		if( ! $where )
		{
			return $result->skip($this->start())->take($this->length())->get();
		}
		return $result->whereRaw($where)->skip($this->start())->take($this->length())->get();
	}
}