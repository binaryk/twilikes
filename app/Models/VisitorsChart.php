<?php namespace App\Models;


class VisitorsChart extends \App\Comptech\Model\Model
{


    protected $table = 'visitors_chart';
    protected $guarded = ['id'];


    public function countries()
    {
        return $this->belongsTo('\App\Models\Country', 'country_id');
    }

}
