<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function(Blueprint $t){
            $t->increments('id');
            $t->integer('post_id');
            $t->string('title');
            $t->text('description');
            $t->string('link');
            $t->string('path');
            $t->string('location');
            $t->string('extension');
            $t->float('storage');
            $t->integer('author');
            $t->string('img_name');
            $t->timestamps();
            $t->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
