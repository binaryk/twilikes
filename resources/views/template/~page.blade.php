<!-- page content -->
<div class="right_col" role="main">
    @include('includes.partials.messages')
    @include('template.~page-head')
    @yield('content')
    @include('template.~footer')
</div>
<!-- /page content -->