<?php namespace App\Models\Nomenclatoare;


class Category extends \App\Comptech\Model\Model
{


    protected $table = 'categories';
    protected $guarded = ['id'];

    public static function Combobox()
    {
        $result = ['0' => trans('actions.no-selection')];
        $records = self::orderBy('id')->get();
        foreach( $records as $i => $record )
        {
            $result[$record->id] = $record->categorie;
        }
        return $result;

    }


    public static function category()
    {
        return [
            '' => ' -- Select -- ',
            '1' => 'Fun',
            '2' => 'Celebrities',
            '3' => 'Life',
            '4' => 'Sexy',
            '5' => 'Health',
            '6' => 'General',
            '7' => 'Design, Art & Beauty',
        ];
    }
}
