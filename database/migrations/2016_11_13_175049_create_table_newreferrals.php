<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNewreferrals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('newreferrals', function(Blueprint $t){
           $t->increments('id');
           $t->date('date')->index();
           $t->float('earnings',8, 5);
           $t->string('name');
           $t->integer('connection_id');
           $t->integer('user_id')->index();
           $t->timestamps();
           $t->softDeletes();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('newreferrals');
    }
}
