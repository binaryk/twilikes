<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="Bootstrap Admin App + jQuery">
<meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
<title>Twilikes</title>
<link rel="stylesheet" href="{!! asset('angle/vendor/fontawesome/css/font-awesome.min.css')!!}">
<!-- SIMPLE LINE ICONS-->
<link rel="stylesheet" href="{!! asset('angle/vendor/simple-line-icons/css/simple-line-icons.css') !!}">
<!-- =============== BOOTSTRAP STYLES ===============-->
<link rel="stylesheet" href="{!! asset('angle/css/bootstrap.css') !!}" id="bscss">
<!-- =============== APP STYLES ===============-->
<link rel="stylesheet" href="{!! asset('angle/css/app.css') !!}" id="maincss">
<script type="text/javascript">
    head.load('javascript/jquery.js', 'javascript/twitterFetcher.js', 'javascript/scripts.js', 'javascript/mobile.js', 'javascript/rewalk.js');
</script>
<script>
    window.intercomSettings = {
        app_id: "fbbftczv"
    };
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/fbbftczv';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
<body>
    <div class="wrapper">
        @include('includes.partials.messages')
        <div class="block-center mt-xl wd-xl">
            <!-- START panel-->
            @yield('content')
            <!-- END panel-->
            <div class="p-lg text-center">
                <span>&copy;</span>
                <span>2016</span>
                <span>-</span>
                <span>Twilikes</span>
                <br>
                <span></span>
            </div>
        </div>
    </div>
    </body>

<script src="{!! asset('angle/vendor/modernizr/modernizr.custom.js') !!}"></script>
<!-- JQUERY-->
<script src="{!! asset('angle/vendor/jquery/dist/jquery.js') !!}"></script>
<!-- BOOTSTRAP-->
<script src="{!! asset('angle/vendor/bootstrap/dist/js/bootstrap.js') !!}"></script>
<!-- STORAGE API-->
<script src="{!! asset('angle/vendor/jQuery-Storage-API/jquery.storageapi.js') !!}"></script>
<!-- PARSLEY-->
<script src="{!! asset('angle/vendor/parsleyjs/dist/parsley.min.js') !!}"></script>
<script src="{!! asset('angle/vendor/app.js') !!}"></script>
</html>
