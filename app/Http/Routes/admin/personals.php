<?php
Route::group(['namespace' => 'Superadmin', 'middleware' => 'admin'], function () {

    Route::get('superadmin/nomenclatoare/personals', 'PersonalsController@getGrid')
        ->name('superadmin.nomenclatoare.personals.index');

    Route::post('superadmin/nomenclatoare/personals-source', 'PersonalsController@dataSource')
        ->name('superadmin.nomenclatoare.personals.data-source');

    /*
     * Sa se incarce formularul de actiun (actions form)
     */
    Route::post('superadmin/nomenclatoare/personals-get-action-form/{action}/{id?}', 'PersonalsController@getActionForm')
        ->name('superadmin.nomenclatoare.personals.get-action-form');

    /*
     * ce se intampla la Adauga/Salveaza/Sterge
     */
    Route::post('superadmin/nomenclatoare/personals-action/{action}/{id?}', 'PersonalsController@doAction')
        ->name('superadmin.nomenclatoare.personals-do-action');
});

