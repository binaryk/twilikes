<?php namespace App\Http\Controllers\UserPosts;

use \App\Http\Controllers\Basic\BaseController;
use App\Models\Item;
use App\Models\Nomenclatoare\Post;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

use App\Repositories\UserPosts\Grid;
use App\Repositories\UserPosts\Rows;
use App\Repositories\UserPosts\Form;
use App\Repositories\UserPosts\Action;
use Intervention\Image\Facades\Image;

class UserPostsController extends BaseController
{

    /**
     * $this->data() - vine din BaseController cu chestii comune (breadcrumb, meniu, ....)
     */
    public function getGrid()
    {

        $this->clearAllUnsubmitedPosts();
        $this->makeNavigation();
        return
            view('user_posts.grid.index')
                ->with( $this->data('Articles', 'filters') )
                ->withGrid( (new Grid())->parameters( Input::all()) )
                ->withToolbar( str_replace([Chr(13), Chr(10), Chr(9)], '', view('user_posts.grid.toolbar')->withActionRoute(\URL::route('user_posts.get-action-form', ['action' => 'insert']))->render()) )
                ->withIcon(NULL)
                ->withDescription(NULL)
            ;
    }

    /*
     * Generarea inregistrari pentru Grid
     */
    public function dataSource()
    {
        return (new Rows())->parameters(Input::all())->response();
    }

    /*
     * Trimite catre index formularul "actions"
     */
    public function getActionForm($action, $id = NULL)
    {

        $form =
            (new Form($action, $id))
                ->setGridId("gridUserPosts")
                ->route(\URL::route('user_posts-do-action', ['action' => $action, 'id' => $id]))
        ;
        return \Response::json([
            'caption' => $form->caption(),
            'form'    => $form->render(),
            'action'  => $form->actionCaption(),
            'route'   => $form->getRoute(),
            'impact' => $action,
        ]);
    }

    /**
     * do Action - se face adaugarea/modificarea/stergere
     */
    public function doAction( $action, $id = NULL )
    {



        if($action === 'clear') {
//            clear post and images
            return $this->clear(Input::get('post'));
        }

        $action = $action == 'delete' ? 'delete' : 'update';

        if($action === 'delete') {

            $actionObject = (new Action($action, $id))->data(Input::all());
            $res = $actionObject->commit();
            return \Response::json($res);

        } else {

            $this->updateItem(Input::get('post')['items']);
            $data['title'] = Input::get('post')['title'];
            $data['description'] = Input::get('post')['description'];
            $data['category'] = Input::get('post')['category'];
            $data['successfully_submited'] = 1;

            if(array_key_exists('location',Input::get('post')) && strlen(Input::get('post')['location']) > 0) {
                $data['location'] = Input::get('post')['location'];
            }

            $action = (new Action($action, Input::get('post')['id']))->data($data);
            return \Response::json($action->commit());

        }


    }


    public function updateItem($items)
    {

        foreach($items as $item){

            $data = [
                'title' => $item['title'],
                'description' => $item['description'],
            ];

            if(array_key_exists('location', $item) && strlen($item['location']) > 0) {
                $data['location'] = $item['location'];
            }

            Item::where('id',$item['id'])->update($data);
        }

    }

    public function apiUploadPhotoFromUrl()
    {

        $forPost = Input::get('item_id') === null;


        try{
            $post   = $this->createOrGetPost(Input::get('post_id'));
            if(! $forPost) {
                $item   = $this->createOrGetItem(Input::get('item_id'), $post->id);
            }
        }catch(\Exception $e){
            return error($e->getMessage());
        }

        try{
            $source = Input::get('url');
            $image = Image::make($source);
            if($forPost) {
                $base_dir = 'uploads/users_campaigns/'.access()->user()->id.'/post_'.$post->id.'/';
            } else {
                $base_dir = 'uploads/users_campaigns/'.access()->user()->id.'/post_'.$post->id.'/items/';
            }
            $base_file = basename($source);
            $path = $this->resolveDirectory(public_path($base_dir)) . $base_file;
            $image->save($path);
        }catch (\Exception $e){
            $this->deleteTmpDirContent(access()->user()->id);
            return error('Error on upload: '.$e->getMessage());
        }

        $data['author']     = access()->user()->id;
        $data['img_name']   = $base_file;
        $data['path']       = $path;
        $data['location']    = asset($base_dir . $base_file);

        if(! $forPost) {
            $item->update($data);
        } else {
            $post->update($data);
        }

        return success([
            'post' => $post,
            'item' => !$forPost ? $item : null
        ]);

    }

    public function apiUploadPhotoFrom()
    {
        try{
            $post   = $this->createOrGetPost(Input::get('post_id'));
            $forPost = Input::get('forPost') === "true";

            if( !$forPost ){
                // ajax-ul nu este doar pentru poza de la posts
                $item   = $this->createOrGetItem(Input::get('item_id'), $post->id);
            } else {
                $item = null;
            }



        }catch(\Exception $e){
            return error($e->getMessage());
        }

        $file               = Input::file('photo');
        if($forPost) {
            $base_dir           = 'uploads/users_campaigns/'.access()->user()->id.'/post_'.$post->id.'/';
        } else {
            $base_dir           = 'uploads/users_campaigns/'.access()->user()->id.'/post_'.$post->id.'/items/';
        }
        $base_file          =  basename($file->getClientOriginalName());
        $res                = $file->move($this->resolveDirectory($base_dir), $base_file);
        $data['author']     = access()->user()->id;
        $data['img_name']   = $base_file;
        $data['path']       = $res->getPathName();
        $data['storage']    = $res->getSize();
        $data['location']    = asset($base_dir . $base_file);

        if($forPost){
            $post->update($data);
        } else {
            $item->update($data);
        }


        return success([
            'post' => $post,
            'item' => $item
        ]);
    }

    public function resolveDirectory($destination_folder)
    {
        if( ! File::isDirectory($destination_folder)){
            File::makeDirectory($destination_folder, 0775, true);
        }
        return $destination_folder;
    }

    public function deleteTmpDirContent($user_id)
    {
        $dir = public_path('uploads/users_campaigns/'.$user_id.'/tmp');
        if( File::isDirectory($dir)){
            File::cleanDirectory($dir);
        }

        return true;
    }

    public function createOrGetPost($post_id)
    {

        if( $post = Post::find($post_id)) {

            if( $post->user_id !== access()->user()->id ) {
                throw new \Exception("This client doesn't owner of post.");
            }

            return $post;

        } else {
            return Post::create([
                'user_id' => access()->user()->id
            ]);
        }

    }

    public function createOrGetItem($item_id, $post_id)
    {

        if( $item = Item::find($item_id)) {

            if(! Post::find($item->post_id)){
                throw new \Exception("This item doesn't have an parent post.");
            }

            return $item;

        } else {
            return Item::create([
                'post_id' => $post_id
            ]);
        }

    }

    public function clear($postData)
    {
        $post = Post::find($postData['id']);

        if(!$post) {
            return \Response::json(['success' => true]);
        }


        $path_to_photos = public_path('uploads/users_campaigns/'.$post->author.'/post_'.$post->id);

        if( File::isDirectory($path_to_photos)){
            File::deleteDirectory($path_to_photos);
        }

        foreach($post->items()->get() as $item){
           $item->delete();
        }

        $post->delete();
        return \Response::json(['success' => true]);
    }

    public function clearAllUnsubmitedPosts()
    {
        foreach(Post::where('user_id', access()->user()->id)->where('successfully_submited', 0)->get() as $post){
            $this->clear(['id' => $post->id]);
        }
    }

}