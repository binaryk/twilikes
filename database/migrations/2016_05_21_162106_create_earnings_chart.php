<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEarningsChart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('earnings_chart', function(Blueprint $t){
            $t->bigIncrements('id');
            $t->bigInteger('user_id');
            $t->float('price',8,4);
            $t->integer('country_id');
            $t->string('data');
            $t->softDeletes();
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('earnings_chart');
    }
}
