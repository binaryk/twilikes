<?php

Route::group(['namespace' => 'Payment', 'middleware' => 'auth'], function () {

    Route::get('payment-index', 'PaymentController@index')
        ->name('payment.index');

    Route::get('api/payment-methods', 'PaymentController@apiMethods')
        ->name('api.payments.methods');

    Route::get('api/user-data', 'PaymentController@apiGetUserData')
        ->name('api.user.data');

    Route::get('api/payment-history', 'PaymentController@apiPaymentHistory')
        ->name('api.history.data');

    Route::post('api/update-payment-method', 'PaymentController@apiUpdatePaymentMethod')
        ->name('api.post.user.data');
});
