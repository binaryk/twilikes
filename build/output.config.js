'use strict';
var path = require('path');

module.exports = {
    path         : __dirname + '/../public/assets',
    filename     : 'bundle.js'
}