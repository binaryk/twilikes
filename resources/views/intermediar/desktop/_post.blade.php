    @extends('intermediar.desktop._template')
    @section('content')
        <div class="container" id="intermediar">
            <div class="col-lg-1"></div>
            <div class="col-lg-7 col-md-7 col-xs-12">
                <div class="post">
                    <div class="title_subtitle" style="padding: 15px;">
                        <p class="col-md-9" style="padding: 0;">
                            <span class="post_title">{!! @$title !!}<br /></span>
                            <span class="post_subtitle">{!! @$description !!}</span>
                        </p>
                        <a v-cloak rel="nofollow" @click="toLink('{!! $destination !!}')" id="start_button" class="hidden-sm button long_button" :class="loading ? 'isLoadingBtn'  : ''" style="cursor: pointer
                        ;">@{{ startButton }} <span v-if="!loading">▶ </span></a>
                        <div style="clear:both;"></div>
                        <a href="" class="hidden-md hidden-lg start_gallery" :class="loading ? 'isLoadingBtn'  : ''">START GALLERY <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                    </div>
                    <div class="image" style="position:relative;">
                        <img src="{!! @$intermediar_location !!}" style="width:100%;" alt="">
                        <div class="button_border">
                            <a v-cloak class="btn btn-xs view_gallery" :class="loading ? 'isLoadingBtn'  : ''" @click="toLink('{!! $destination !!}')">@{{ laddaText }}</a>
                            <div class="sk-circle" style="position: absolute; top: -71px; right: 40px;" v-show="loading">
                                <div class="sk-circle1 sk-child"></div>
                                <div class="sk-circle2 sk-child"></div>
                                <div class="sk-circle3 sk-child"></div>
                                <div class="sk-circle4 sk-child"></div>
                                <div class="sk-circle5 sk-child"></div>
                                <div class="sk-circle6 sk-child"></div>
                                <div class="sk-circle7 sk-child"></div>
                                <div class="sk-circle8 sk-child"></div>
                                <div class="sk-circle9 sk-child"></div>
                                <div class="sk-circle10 sk-child"></div>
                                <div class="sk-circle11 sk-child"></div>
                                <div class="sk-circle12 sk-child"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="like">
                    <h4>You Might Like:</h4>
                    {!! $domain->bottom_code!!}
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-xs-12" style="border: 1px solid #ccc; min-height: 600px;">
                {!! $domain->sidebar_code  !!}
            </div>
        </div><!-- /.container -->
    @stop


