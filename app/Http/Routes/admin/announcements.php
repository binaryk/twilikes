<?php
Route::group(['namespace' => 'Superadmin', 'middleware' => 'admin'], function () {

    Route::get('superadmin/nomenclatoare/announcement', 'AnnouncementController@getGrid')
        ->name('superadmin.nomenclatoare.announcement.index');

    Route::post('superadmin/nomenclatoare/announcement-source', 'AnnouncementController@dataSource')
        ->name('superadmin.nomenclatoare.announcement.data-source');

    /*
     * Sa se incarce formularul de actiun (actions form)
     */
    Route::post('superadmin/nomenclatoare/announcement-get-action-form/{action}/{id?}', 'AnnouncementController@getActionForm')
        ->name('superadmin.nomenclatoare.announcement.get-action-form');

    /*
     * ce se intampla la Adauga/Salveaza/Sterge
     */
    Route::post('superadmin/nomenclatoare/announcement-action/{action}/{id?}', 'AnnouncementController@doAction')
        ->name('superadmin.nomenclatoare.announcement-do-action');

    Route::get('api/announcement', 'AnnouncementController@all')
        ->name('api.announcement.all');

    Route::post('api/announcement/generate', 'AnnouncementController@generate')
        ->name('api.announcement.generate');
});

