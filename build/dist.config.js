'use strict';

module.exports = {
    entry: require('./devEntry.config'),
    output: require('./output.config'),
    module: {
        loaders: require('./loaders.config')
    },
    // plugins: require('./plugins.config'),
    resolve: {
        extensions: ['', '.js', '.jsx', '.ts', '.tsx', '.web.js', '.webpack.js']
    }
}

