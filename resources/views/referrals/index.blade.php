@extends("template.~template")

@section('content')
        <referral></referral>
@stop

@section('js')
    @parent
    <script type="text/javascript" src="{!! asset('theme/js/moment/moment.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('theme/js/datepicker/daterangepicker.js') !!}"></script>
@stop
@section('css')
    @parent
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/2.0.0/toaster.min.css">
@stop