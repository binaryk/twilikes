import angular from "angular";
import * as ld from 'lodash';

var dms = angular.module('dms', ['dms.controllers','dms.directives','dms.services'])
    .config(function($interpolateProvider, $locationProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
       /* $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });*/
    })
    .run(function($rootScope){
        $rootScope.config = _config;
    })