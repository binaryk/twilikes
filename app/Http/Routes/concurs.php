<?php

Route::group(['namespace' => 'Concurs', 'middleware' => 'cors'], function () {

    Route::get('get-top', 'ConcursController@top')
    ->name('concurs.top.index');
});
