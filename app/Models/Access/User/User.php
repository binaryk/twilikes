<?php

namespace App\Models\Access\User;

use App\Models\Access\User\Traits\UserAccess;
use App\Models\UserPost;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Access\User\Traits\Attribute\UserAttribute;
use App\Models\Access\User\Traits\Relationship\UserRelationship;

/**
 * Class User
 * @package App\Models\Access\User
 */
class User extends Authenticatable
{

    use SoftDeletes, UserAccess, UserAttribute, UserRelationship;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function posts()
    {
        return $this->belongsToMany('App\Models\Nomenclatoare\Post', 'user_posts', 'user_id', 'post_id');
    }

    public function getConnectionsAttribute()
    {
        return self::where('invited_by', auth()->user()->id)->get();
    }

    public function reports()
    {
        return $this->hasMany('App\Models\Report', 'user_id');
    }

    public function domains()
    {
        return $this->hasMany(\App\Models\Domain::class, 'user_id');
    }

    public function hasDomain($domain)
    {
        return $this->domains()->get()->contains($domain);
    }

    public static function personal()
    {
        return [
            0 => 'No',
            1 => 'Yes',
            2 => 'Yes, no 100%',
        ];
    }

    public static function confirmed()
    {
        return [
            0 => 'No',
            1 => 'Yes',
        ];
    }

    public static function view_common_domains()
    {
        return [
            0 => 'No',
            1 => 'Yes',
        ];
    }

    public static function postedIds()
    {
        $ids = UserPost::where('user_id', access()->user()->id)->lists('user_id');
        return $ids;
    }

}
