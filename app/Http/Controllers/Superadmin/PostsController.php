<?php namespace App\Http\Controllers\Superadmin;

use \App\Http\Controllers\Basic\BaseController;
use Illuminate\Support\Facades\Input;

use App\Repositories\Superadmin\Nomenclatoare\Posts\Grid;
use App\Repositories\Superadmin\Nomenclatoare\Posts\Rows;
use App\Repositories\Superadmin\Nomenclatoare\Posts\Form;
use App\Repositories\Superadmin\Nomenclatoare\Posts\Action;

class PostsController extends BaseController
{

    /**
     * $this->data() - vine din BaseController cu chestii comune (breadcrumb, meniu, ....)
     */
    public function getGrid()
    {
        $this->makeNavigation();
        return
            view('superadmin.nomenclatoare.posts.grid.index')
                ->with( $this->data('Posts', 'pentru filtrare') )
                ->withGrid( (new Grid())->parameters( Input::all()) )
                ->withToolbar( str_replace([Chr(13), Chr(10), Chr(9)], '', view('superadmin.nomenclatoare.posts.grid.toolbar')->withActionRoute(\URL::route('superadmin.nomenclatoare.posts.get-action-form', ['action' => 'insert']))->render()) )
                ->withIcon(NULL)
                ->withDescription(NULL)
            ;
    }

    /*
     * Generarea inregistrari pentru Grid
     */
    public function dataSource()
    {
        return (new Rows())->parameters(Input::all())->response();
    }

    /*
     * Trimite catre index formularul "actions"
     */
    public function getActionForm($action, $id = NULL)
    {

        $form =
            (new Form($action, $id))
                ->setGridId("gridPosts")
                ->route(\URL::route('superadmin.nomenclatoare.posts-do-action', ['action' => $action, 'id' => $id]))
        ;
        return \Response::json([
            'caption' => $form->caption(),
            'form'    => $form->render(),
            'action'  => $form->actionCaption(),
            'route'   => $form->getRoute(),
            'impact' => $action,
        ]);
    }

    /**
     * do Action - se face adaugarea/modificarea/stergere
     */
    public function doAction( $action, $id = NULL )
    {
        $data = Input::except('file');
        if(Input::hasFile('file')){
            $file               = Input::file('file');
            $tmp_name = str_random(30).'_'.str_replace(' ','_',$file->getClientOriginalName());
            $res                = $file->move(public_path('uploads'), $tmp_name);
            $data['author']     = access()->user()->id;
            $data['img_name']   = $tmp_name;
            $data['path']       = $res->getPathName();
            $data['extention']  = $res->getExtension();
            $data['storage']    = $res->getSize();
            $data['location']   = asset('uploads/' . $tmp_name);
            $this->log('Utilizatorul:'.$this->current_user->name.' a incarcat fisierul:'.$data['path'].'.', 'info');

        }
        $actionObject = (new Action($action, $id))->data($data);
        $res = $actionObject->commit();
        return \Response::json($res);

        $action = (new Action($action, $id))->data(Input::get('data'));
        return \Response::json($action->commit());
    }

}