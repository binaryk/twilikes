<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('reports', function(Blueprint $t){
           $t->bigIncrements('id');
           $t->integer('post_id');
           $t->integer('user_id');
           $t->datetime('last_click');
           $t->string('visitor_ip');
           $t->string('device');
           $t->integer('domain_id');
           $t->integer('user_post_id');
           $t->tinyInteger('phone')->default(0);
           $t->tinyInteger('tablet')->default(0);
           $t->tinyInteger('desktop')->default(0);
           $t->timestamps();
           $t->softDeletes();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports');
    }
}
