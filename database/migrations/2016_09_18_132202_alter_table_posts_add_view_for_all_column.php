<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePostsAddViewForAllColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function(Blueprint $t) {
            $t->tinyInteger('view_for_all')->default(0);
            $t->tinyInteger('successfully_submited')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function(Blueprint $t) {
            $t->dropColumn('view_for_all');
            $t->dropColumn('successfully_submited');
        });
    }
}
