<table class="table">
    <thead>
        <th>User id</th>
        <th>Earning</th>
    </thead>
    <tbody>
        @foreach($data as $date)

            <tr>
                <td>{!! $date->user_id !!}</td>
                <td>{!! $date->earnings !!}</td>
            </tr>

        @endforeach
    </tbody>
</table>