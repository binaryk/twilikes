import referrer from "./table.vue";
export default new  Vue({
    el: "#admin_referrer",
    data: {
        links: [],
        xxxxx: "eduard",
        userModals : []
    },
    components:{
        referrer
    },
    methods: {
        referrers: function(id, user_id){
            var element = $(id.target)
            if(element[0].nodeName == 'I'){
                element = $(element).parent();
            }

            const self = this;
            const Chref = element.data('href');

            $.ajax({
                url: Chref,
                type: 'get',
                dataType: 'json',
                async: false,
                success: function (data) {
                    if(data.code == 200){
                        self.$broadcast('dataReceive', data.links);
                        self.links = data.links;
                    }else{
                        self.links = [];
                    }
                }
            });
        }
    },
    ready: function(){
    }
});