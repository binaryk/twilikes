<?php namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Reports\ReportTrait;
use App\Models\Access\User\User;
use App\Models\Country;
use App\Models\EarningsChart;
use App\Models\NewReports;
use App\Models\Nomenclatoare\Announcement;
use App\Models\Nomenclatoare\Earning;
use App\Models\Areport;
use App\Models\Visit;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Stevebauman\Location\Facades\Location;

/**
 * Class LanguageController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    use ReportTrait;
    use DashboardTrait;

    public function index()
    {

        $first_login = Input::get('first_login') == "1";

        return view('dashboard.index')
            ->with(
                [
                    'announcements' => Announcement::orderBy('id','DESC')->limit(5)->get(),
                    'firstLogin'    => $first_login
                ]);
    }

    public function tile()
    {

        if(! config('app.localStorage')){
            return success(['data' => $this->amounts(), 'serverTime' => Carbon::now()->toDateTimeString(), 'refresh' => true, 'user' => access()->user()->id ]);
        }

        $localStorage = Input::get('localStorage') === "false" ? null : Input::get('localStorage');
        // Daca nu am local storage sau nu am setat pe el timpul cand a fost facut storage-ul returnez date actualizate
        if(! $localStorage || ! $localStorage['timestamp'] || ! array_keys_exists(['data', 'user'], $localStorage) || access()->user()->id != $localStorage['user'] ) {
            return success(['data' => $this->amounts(), 'serverTime' => Carbon::now()->toDateTimeString(), 'refresh' => true, 'user' => access()->user()->id ]);
        }

        // Daca am storage verific daca nu a trecut o zis
        $serverTimeFromStorage = Carbon::createFromFormat('Y-m-d H:i:s', $localStorage['timestamp']);
        $anotherDay = Carbon::now()->diffInDays( $serverTimeFromStorage )  > 0;

        if( $anotherDay ) {
            return success(['data' => $this->amounts(), 'serverTime' => Carbon::now()->toDateTimeString(), 'refresh' => true, 'user' => access()->user()->id ]);
        } else {
            // aceeasi zi, returnez doar ziua de azi din storage
            $dataToReturnWithActualDay = $localStorage['data'];
            $dataToReturnWithActualDay['today'] = $this->amounts('today');
            return success(['data' => $dataToReturnWithActualDay, 'serverTime' => Carbon::now()->toDateTimeString(), 'refresh' => false, 'user' => access()->user()->id ]);
        }



    }

    public function amounts($filter = null)
    {

//        $result = DB::select('call today_price(?)',array(access()->user()->id));
        $todayDate = Carbon::now();
        switch($filter) {
            case 'today' :
                return number_format(Areport::where('user_id',access()->user()->id)
                    ->whereDate('created_at', '=',$todayDate->toDateString())
                    ->sum('price'), 2);
                break;
            default :
                $today_revenue     =  Areport::whereDate('created_at', '=',$todayDate->toDateString())
                        ->where('user_id',access()->user()->id)
                        ->sum('price');
                $month_revenue     = NewReports::whereDate('created_at', '>=',$todayDate->subMonth()->toDateString())
                    ->where('user_id',access()->user()->id)
                    ->sum('earnings');
//                $month_revenue = 0;
//                foreach($month_data as $day) {
//                    $month_revenue += $day->earnings;
//                }

//                $yesterday_revenue = $month_data->filter(function($value, $k) {
//                    return $value->date == Carbon::yesterday();
//                });
//                dd(Carbon::now());
                
                if(isTransitionInterval()) {
                }
                $yesterday_revenue = NewReports::where('user_id',access()->user()->id)
                    ->whereDate('created_at', '=',Carbon::now()->subDay()->toDateString())
                    ->first();


                if($yesterday_revenue) {
                    $yesterday_revenue = $yesterday_revenue->earnings;
                }


                $out['today']      = number_format($today_revenue, 2);
                $out['yesterday']  = number_format($yesterday_revenue, 2);
                $out['month']      = number_format($month_revenue, 2);
                $out['incomes']    = auth()->user()->paid;
                $out['incomes']    = number_format($out['incomes'], 2);
                $out['visits']     = NewReports::where('user_id',access()->user()->id)->sum('clicks_calitate');
                return $out;
        }



    }

    public function getPricesByData($startDate, $endDate, $data)
    {
        $summ = 0;
        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);
        foreach($data as $k => $yesterdayData){
            $current = strtotime($yesterdayData['date']->toDateString());

            if($current - $startDate >= 0 && $current - $endDate <= 0){
                foreach($yesterdayData['data'] as $t){
                    if($t->quality){
                        $summ += $t->price;
                    }
                }
            }
        }

        return $summ;
    }

    public function getLastMonth()
    {

    }

    public function apiTodayVisitorsDevice()
    {
        $localStorage = Input::get('localStorage') === "false" ? null : Input::get('localStorage');
        if(! config('app.localStorage')) {
            return success(['data' => $this->getTodayVisitorsDeviceDataOnly(), 'serverTime' => Carbon::now()->toDateTimeString(), 'refresh' => true, 'user' => access()->user()->id ]);

        }

        // Daca nu am local storage sau nu am setat pe el timpul cand a fost facut storage-ul returnez date actualizate
        if(false && ! $localStorage || ! $localStorage['timestamp'] || ! array_keys_exists(['data', 'user'], $localStorage) || access()->user()->id != $localStorage['user'] ) {
            return success(['data' => $this->getTodayVisitorsDeviceDataOnly(), 'serverTime' => Carbon::now()->toDateTimeString(), 'refresh' => true, 'user' => access()->user()->id ]);
        }


        $serverTimeFromStorage = Carbon::createFromFormat('Y-m-d H:i:s', $localStorage['timestamp']);
        $anotherDay = Carbon::now()->diffInMinutes( $serverTimeFromStorage )  > 30;

        if( $anotherDay ) {
            return success(['data' => $this->getTodayVisitorsDeviceDataOnly(), 'serverTime' => Carbon::now()->toDateTimeString(), 'refresh' => true, 'user' => access()->user()->id ]);
        } else {
            // aceeasi zi, returnez doar ziua de azi din storage
            $dataToReturnWithActualDay = $localStorage['data'];
            return success(['data' => $dataToReturnWithActualDay, 'serverTime' => Carbon::now()->toDateTimeString(), 'refresh' => false, 'user' => access()->user()->id ]);
        }


    }

    public function getTodayVisitorsDeviceDataOnly()
    {
        $todayDate = Carbon::now()->toDateString();
        $data = $this->obtainDataPerDays($todayDate, $todayDate);
        $sum = [
            'phone' => 0,
            'tablet' => 0,
            'desktop' => 0,
        ];
        foreach($data as $yesterdayData){
            foreach($yesterdayData['data'] as $t){
                if($t->phone){
                    $sum['phone'] += 1;
                }
                if($t->tablet){
                    $sum['tablet'] += 1;
                }
                if($t->desktop){
                    $sum['desktop'] += 1;
                }
            }
        }
        $total = $sum['phone'] + $sum['tablet'] + $sum['desktop'];
        if($total != 0){
            $sum['phone'] = $sum['phone'] * 100 / $total;
            $sum['tablet'] = $sum['tablet'] * 100 / $total;
            $sum['desktop'] = $sum['desktop'] * 100 / $total;
        }

        $sum['phone'] = round($sum['phone'], 2);
        $sum['tablet'] = round($sum['tablet'], 2);
        $sum['desktop'] = round($sum['desktop'], 2);

        return $sum;
    }

    public function apiWeeklyEarnings()
    {
        return $this->getEarningsChart();
    }


    public function getLocalCountryId($visitor_ip)
    {
        // returnez id-ul tarii din eargnings (cele adaugate de admin)
        $location = Location::get($visitor_ip);
        $country = Country::where('code', strtoupper($location->countryCode))->first();
        $earnings = null;
        if($country){
            $earnings = Earning::where('country_id', $country->id)->first();
        }else{
            $earnings = Earning::where('country_id', 9999)->first();
        }
        if($earnings){
            return $earnings->country_id;
        }
        return 9999;
    }


    public function sampleMonthName($monthNumber, $length = 3)
    {
        $monthNumber--;
        $months = [
            0 => 'January',
            1 => 'February',
            2 => 'March',
            3 => 'April',
            4 => 'May',
            5 => 'June',
            6 => 'July',
            7 => 'August',
            8 => 'September',
            9 => 'October',
            10 => 'November',
            11 => 'December',
        ];

        return substr($months[$monthNumber],0, $length);
    }

    public function apiTodayVisitor()
    {

        return $this->apiTodayVisitorTrait();
    }

}