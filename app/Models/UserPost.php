<?php namespace App\Models;

use App\Comptech\Model\Model;
use Carbon\Carbon;
class UserPost extends Model
{
    protected $table = "user_posts";
    protected $guarded = ['_token'];

}