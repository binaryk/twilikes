@extends('intermediar.mobile._template')
@section('content')
    <a id="main_content_link" style="display:block; padding: 10px; border-bottom: 1px solid #ddd; background: #f0f0f0; text-decoration: none;" href="#">
        <img class="main_thumb" src="{!! @$intermediar_location !!}" />
        <div class="content">
            <h1 style="font-size: 23px; line-height: 25px; color: #000000; font-weight: bold; text-shadow: 0 1px 1px #fff">{!! @$title !!}</h1>
        </div>
        <center style="clear: both" v-cloak>
            <div data-style="expand-right" :class="loading ? 'isLoadingBtn'  : ''" data-size="xl" class="button blue_button ladda-button" style="font-size: 20px; font-weight: 700; padding: 15px; margin: 10px auto 0; position: relative;" @click="toLink('{!! $destination !!}')">
                 <span class="start_button_text"  id="start_button_node" @click="toLink('{!! $destination !!}')">@{{ laddaText  }}</span>
                <div class="sk-circle" style="position: absolute; top: -90px; right: 40px;" v-show="loading">
                    <div class="sk-circle1 sk-child"></div>
                    <div class="sk-circle2 sk-child"></div>
                    <div class="sk-circle3 sk-child"></div>
                    <div class="sk-circle4 sk-child"></div>
                    <div class="sk-circle5 sk-child"></div>
                    <div class="sk-circle6 sk-child"></div>
                    <div class="sk-circle7 sk-child"></div>
                    <div class="sk-circle8 sk-child"></div>
                    <div class="sk-circle9 sk-child"></div>
                    <div class="sk-circle10 sk-child"></div>
                    <div class="sk-circle11 sk-child"></div>
                    <div class="sk-circle12 sk-child"></div>
                </div>
            <div class="clearfix"></div>
            </template>
            </div>
        </center>
    </a>
@stop


