<?php

Route::group(['namespace' => 'Reports', 'middleware' => 'auth'], function () {

    Route::get('reports-index', 'ReportsController@index')
    ->name('reports.index');

    Route::get('api/report-range', 'ReportsController@data')
    ->name('api.reports.data');

    Route::get('api/articles-range', 'ReportsController@articles')
    ->name('api.reports.articles');
});
