var elixir = require('laravel-elixir');
var gulp   = require('gulp');
var shell = require("gulp-shell");
var watch = require('gulp-watch');

var controllers = [
    'angular/controllers/controllers.js',
    'angular/controllers/MainCtrl.js',
];

var directives = [
    'angular/directives/directives.js',
];

var services = [
    'angular/services/services.js',
];

var config = [
    'angular/_config.js',
];

elixir(function(mix) {
    mix
        .babel(config.concat(controllers,services,directives), 'public/assets/angular.js')
        .version(["public/assets/angular.js"])
        /*        .sass([
         'site/grid.scss',
         'dataTables.comptech.scss',
         'galonline.scss',
         'modal.comptech.scss'
         ],'public/custom/css/main.css')*/
    ;
});