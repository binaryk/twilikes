<?php namespace App\Models;


class Payment extends \App\Comptech\Model\Model
{


//    protected $table = '';
    protected $guarded = ['id'];

    public static function Combobox()
    {
        $result = ['0' => trans('actions.no-selection')];
        $records = self::orderBy('id')->get();
        foreach( $records as $i => $record )
        {
            $result[$record->id] = $record->categorie;
        }
        return $result;

    }

    public static function methods()
    {
        return [
            ['id' => '0','title' => '-',],
            ['id' => '1','title' => 'Paypal',],
            ['id' => '2','title' => 'Wire transfer'],
            ['id' => '3','title' => 'Skrill'],
        ];
    }

    public static function methods2()
    {
        return [
            '0' => '-',
            '1' => 'Paypal',
            '2' => 'Wire transfer',
            '3' => 'Skrill',
        ];
    }

    public static function status()
    {
        return [
            ['id' => 'pending','title' => 'Pending',],
            ['id' => 'paid','title' => 'Paid'],
        ];
    }

    public static function status2()
    {
        return [
            'pending' => 'Pending',
            'paid' => 'Paid',
        ];
    }
}
