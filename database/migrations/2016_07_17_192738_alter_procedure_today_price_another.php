<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProcedureTodayPriceAnother extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*42.2900*/
        $sql = <<<SQL
         DROP PROCEDURE IF EXISTS today_price;
        CREATE PROCEDURE today_price(IN _user_id INTEGER)
        SELECT * from (

            (SELECT if(sum(price),sum(price),0) as today FROM `reports` FORCE INDEX (created_at) WHERE user_id=_user_id AND DATE_FORMAT(created_at,'%m-%d-%Y') = DATE_FORMAT(NOW(),'%m-%d-%Y')) as today,
            (SELECT if(sum(price),sum(price),0)  as yesterday FROM `reports` FORCE INDEX (created_at) WHERE user_id=_user_id AND DATE_FORMAT(created_at,'%m-%d-%Y') = DATE_FORMAT(subdate(NOW(), 1),'%m-%d-%Y')) as yesterday,
            (SELECT if(sum(price),sum(price),0) as month_my FROM `reports` FORCE INDEX (created_at) WHERE user_id=_user_id AND DATE(created_at) > DATE(subdate(NOW(), 30))) as thisMonth )
            ;
        END
SQL;
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP PROCEDURE IF EXISTS today_price";
        DB::connection()->getPdo()->exec($sql);
    }
}
