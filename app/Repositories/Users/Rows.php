<?php namespace App\Repositories\Superadmin\Nomenclatoare\Users;

use App\Models\Access\User\User;
use \App\Comptechsoft\Datatable\Cell;
use App\Models\Payment;

class Rows extends \App\Comptechsoft\Datatable\Rows
{

	public function __construct()
	{
		$this->searcheableColumns([1 => 'id', 2 => 'name', 3 => 'email', '4' => 'confirmed']);
		$this->orderableColumns([1 => 'id', 2 => 'name', 3 => 'email',4 => 'confirmed']);

		$this->addCells([
			'rec-no'     => (new Cell())->source(function($record){ return $record->properties->recCount . '.'; }),
			'id'         => (new Cell())->source(function($record){ return $record->id; }),
			'name'      => (new Cell())->source(function($record){ return $record->name; }),
			'email'      => (new Cell())->source(function($record){ return $record->email; }),
            'confirmed'      => (new Cell())->source(function($record){ return pingpong($record->confirmed); }),
            'fbPage'      => (new Cell())->source(function($record){ return view('superadmin.nomenclatoare.posts.grid.filters.location')
                ->with(['name' => $record->fbPage,'location' => asset($record->fbPage)])->render(); }),
            'fbPrintScreen'      => (new Cell())->source(function($record){ return view('superadmin.nomenclatoare.posts.grid.filters.location')->with(['name' => 'Click here','location' => asset($record->fbPrintScreen)])->render(); }),
            'created_at'      => (new Cell())->source(function($record){ return $record->created_at->toDateTimeString(); }),
			'actions'    => (new Cell())->source(function($record){

				return 
					view('superadmin.nomenclatoare.users.grid.actions')
					->withRecord($record)
					->withUpdateRoute(\URL::route('superadmin.nomenclatoare.users.get-action-form', ['action' => 'update', 'id' => $record->id]))
//					->withDeleteRoute(\URL::route('superadmin.nomenclatoare.users.get-action-form', ['action' => 'delete', 'id' => $record->id]))
					->render();
			}),
		]);
	}

	/*
	 * Toate inregistrarile din tabela ferme 
	 */
	protected function totalRecordsCount()
	{
		return User::count();
	}

	/*
	 * Toate inregistrarile obtinute in urma unei filtrari Datatable
	 */
	protected function filteredRecordsCount()
	{
		/*
		 * WhereRaw
		 */
		$where = $this->where();
		if( ! $where )
		{
			return User::count();
		}
		return User::whereRaw($where)->count();
	}

	/*
	 * Setul de date care trebuie trimis catre Datatable
	 */
	protected function getDataSet()
	{
		/*
		 * OrderBy
		 */
		$order_criteria = $this->orderBy();
		$result = User::where('confirmed','!=',3)->where('emailConfirmed',1)->where('fbPage','!=','')->where('fbPrintScreen','!=','')->orderBy( $order_criteria['field'], $order_criteria['direction'] );
		/*
		 * WhereRaw
		 */
		$where = $this->where();
		if( ! $where )
		{
			return $result->skip($this->start())->take($this->length())->get();
		}
		return $result->whereRaw($where)->skip($this->start())->take($this->length())->get();
	}
}