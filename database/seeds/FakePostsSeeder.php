<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class FakePostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for($i = 0; $i < 50; $i++){
            \App\Models\Nomenclatoare\Post::create([
                'title' => $faker->title,
                'description' => $faker->text(10),
                'path' => '/Applications/XAMPP/xamppfiles/htdocs/twilikes/public/uploads/2wN1R7uCNS6jPrEgw3nVjfXMqoUsgs_Administrator_USE_CASE_(1).png',
                'location' => 'http://localhost/twilikes/public/uploads/2wN1R7uCNS6jPrEgw3nVjfXMqoUsgs_Administrator_USE_CASE_(1).png',
                'img_name' => '2wN1R7uCNS6jPrEgw3nVjfXMqoUsgs_Administrator_USE_CASE_(1).png',
            ]);
        }
    }
}
