<?php namespace App\Repositories\Superadmin\Nomenclatoare\Domains;

use \App\Comptechsoft\Datatable\Column;
use \App\Comptechsoft\Datatable\Header;
use \App\Comptechsoft\Datatable\Ajax;
use \App\Comptechsoft\Datatable\Order;
use \App\Comptechsoft\Datatable\Filter;

class Grid extends \App\Comptechsoft\Datatable\Grid
{

	public function __construct()
	{

		$this->noGlobalFilter();
		/*
		 * II asociez ID la grid
		 */
		$this->id('gridDomains');

		/*
		 * In ce fisier sa se genereze js-ul pentru .DataTable({...});
		 */
		$this->js( str_replace('\\', '/', public_path('custom/js/domains/grid.js') ));

		/*
		 * De la ce ruta vine raspunsul cu randuri
		 */
		$this->ajax( (new Ajax())->setUrl(\URL::route('superadmin.nomenclatoare.domains.data-source'))->setExtra(['a' => 1, 'b' => 2]) );
		
		/*
		 * Care coloana contine ordinea initiala
		 */
		$this->order( (new Order())->setColumn(1)->setDirection('asc') );

		/*
		 * Definirea coloanelor
		 **/
		$this->addColumns([

			'rec-no' => 
				(new Column())
				->withHeader( 
					(new Header())
					->with(['caption' => '#', 'width' => 5])
				)
				->alignRight(),

			'id' => 
				(new Column())
				->withHeader( 
					(new Header())
					->with(['caption' => 'ID', 'width' => 5])
				)
				->alignCenter()
				->orderable(),

			'title' =>
				(new Column())
				->withHeader( 
					(new Header())
					->with(['caption' => 'Title', 'width' => 20])
					->withFilter( (new Filter())->view('superadmin.nomenclatoare.domains.grid.filters.categorie') )
				)->orderable(),


			'user' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Owner', 'width' => 20])
				)->orderable(),

			'available' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Available', 'width' => 20])
				)->orderable(),

			'personal' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Personal', 'width' => 20])
				)->orderable(),

			'actions' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Actions'])
				),


		
		]);

	}
}