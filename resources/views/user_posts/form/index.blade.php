<div class="form-my-campaigns">


        @include('user_posts.form.post-fields')

        <div class="alert alert-info alert-zone white-color">
            <p class="text-left">Terms and Conditions</p>
            <br>
            <p>- Minimum 4 subtitles</p>
            <p>- Do not use erotic, sexual, innapropiate or misleading content</p>
            <p>- Cover image (og:image) will be modified by our staff in order to get a higher click through rate</p>
            <p>- The article can be modified by our staff in order to comply with TOS</p>
            <p>- Article can be rejected for non-compliance</p>
            </ul>
        </div>

        <hr>

        <div class="col-md-12">
            <h3>Article items</h3>
            <div class="col-md-12 col-lg-10">
                <post v-for="item in items" :item.sync="item" :post.sync="post" :delete="delete" :index="$index"></post>
            </div>
            <div class="clearfix"></div>
           <div class="col-md-8">
               <button class="btn btn-success btn-xs pull-right btn-add" type="button" @click="create">Add more subtitles &nbsp;<i class="fa fa-plus"></i></button>
           </div>
        </div>
    <div class="clearfix"></div>

    @if($action === 'insert')
        @include('user_posts.form.item-template')


        {!!
            App\Comptechsoft\Ui\Html\Scripts::make([
                'custom/js/vue/user_posts/user_post',
            ])->render()
        !!}
    @endif


</div>