<?php namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;

/**
 * Class LanguageController
 * @package App\Http\Controllers
 */
class LocationController extends Controller
{

    public function getCountryCode()
    {

        return codecountry($this->getRequestIp());
    }

    public function getRequestIp()
    {
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $request_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            if (array_key_exists('HTTP_X_REAL_IP', $_SERVER)) {
                $request_ip = $_SERVER['HTTP_X_REAL_IP'];
            } else {
                $request_ip = $_SERVER['REMOTE_ADDR'];
            }
        }

        return $request_ip;
    }

}