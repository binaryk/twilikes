<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>

        <ul class="nav side-menu">
            <li>
                <a href="{!! url('/') !!}"><i class="fa fa-home"></i> Home </a>
            </li>
            <li>
                <a href="{!! route('reports.index') !!}"><i class="fa fa-table"></i> Reports </a>
            </li>
            @role('Administrator')
            <li><a><i class="fa fa-home"></i> Administration <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{!! route('superadmin.nomenclatoare.posts.index') !!}">Posts</a>
                    </li>
                    <li><a href="{!! route('superadmin.nomenclatoare.verify_posts.index') !!}">Verify Posts</a>
                    </li>
                    <li><a href="{!! route('superadmin.nomenclatoare.domains.index') !!}">Domains</a>
                    </li>
                    <li><a href="{!! route('superadmin.nomenclatoare.earnings.index') !!}">Earning</a>
                    </li>
                    <li><a href="{!! route('superadmin.nomenclatoare.announcement.index') !!}">Announcements</a>
                    </li>
                    <li><a href="{!! route('superadmin.nomenclatoare.payment.index') !!}">Payment</a>
                    </li>
                    <li><a href="{!! route('superadmin.nomenclatoare.personals.index') !!}">Personals config</a>
                    </li>
                    <li><a href="{!! url('superadmin/nomenclatoare/send-emails') !!}">Emails</a>
                    </li>
                    <li><a href="{!! route('admin.users_earnings',['days' => '1']) !!}">Demo UsersEarnings</a>
                    </li>
                    <li><a href="{!! route('superadmin.nomenclatoare.users.index') !!}">Account security check</a>
                    </li>
                </ul>
            </li>
            @endauth
            @role('SubAdmin')
            <li><a><i class="fa fa-home"></i> Administration <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu" style="display: none">
                    <li><a href="{!! route('superadmin.nomenclatoare.verify_posts.index') !!}">Verify Posts</a></li>
                </ul>
            </li>
            @endauth
            <li>
                <a href="{!! route('campaigns.index') !!}"><i class="fa fa-windows"></i> Campaigns </a>
            </li>
            @if(! (new \Jenssegers\Agent\Agent())->isMobile() && false)
                <li>
                    <a href="{!! route('user_posts.index') !!}"><i class="fa fa-windows"></i>New Campaigns </a>
                </li>
            @endif

            <li>
                <a href="{!! route('payment.index') !!}"><i class="fa fa-dollar"></i> Payments </a>
            </li>
            <li>
                <a href="{!! route('referrals.index') !!}"><i class="fa fa-dollar"></i> Referral earnings </a>
            </li>
            <li>
                <a href="{!! route('domains.index') !!}"><i class="fa fa-link"></i> Custom domain </a>
            </li>
        </ul>

    </div>
</div>