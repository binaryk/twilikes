<?php namespace App\Repositories\Superadmin\Nomenclatoare\PersonalPrices;

use App\Models\Nomenclatoare\PersonalPrice;

class Form extends \App\Comptechsoft\Form\Form
{

	public function __construct($action, $id)
	{
		parent::__construct($action, $id);
		
		$this->substantiv = 'PersonalPrice';

		$this->captions = [
	        'insert' => 'Adăugare ' . $this->substantiv,
	        'update' => 'Modificare ' . $this->substantiv . ' (#' . $this->id . ')',
	        'delete' => 'Ştergere ' . $this->substantiv,
	    ];

	    if( $id )
	    {
	    	$this->record(PersonalPrice::find( (int) $id ));

			$this->setParameter('file','<div id="insert"></div>');
	    }

	    $this->view('superadmin.nomenclatoare.personalprices.form.index');


	}
}