module.exports = {
    devtool: 'source-map',
    entry: require('./devEntry.config'),
    resolve: {
        extensions: ['', '.js', '.jsx', '.ts', '.tsx', '.web.js', '.webpack.js']
    },
    output: require('./output.config'),
    //plugins: require('./plugins.config'),
    module: {
        loaders: require('./loaders.config')
    },
/*    devServer: {
        publicPath: '/assets',
        filename: 'bundle.js',
        port: 8080,
        host: '0.0.0.0'
    }*/
}

