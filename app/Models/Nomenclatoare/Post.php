<?php namespace App\Models\Nomenclatoare;


use App\Models\Access\User\User;
use App\Models\Item;
use App\Models\PostView;

class Post extends \App\Comptech\Model\Model
{


    protected $table = 'posts';
    protected $guarded = ['id'];

    public static function Combobox()
    {
        $result = ['0' => trans('actions.no-selection')];
        $records = self::orderBy('id')->get();
        foreach( $records as $i => $record )
        {
            $result[$record->id] = $record->categorie;
        }
        return $result;
    }

    public function uniqueClicks()
    {
        return $this->hasMany('\App\Models\Report','post_id');
    }

    public function post_views()
    {
        return $this->hasMany(PostView::class, 'post_id');
    }

    public function items()
    {
        return $this->hasMany(Item::class, 'post_id');
    }

    public function getStatusLabelAttribute()
    {

        if( (access()->user()->hasRole('SubAdmin') || access()->user()->hasRole('Administrator')) && $this->user_confirmation === 3) {
            return '<span  class="badge badge-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
                        Pre Approved
                    </span>';
        }

        if($this->user_confirmation === 0 || $this->user_confirmation === 3) {
            return '<span  class="badge badge-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
                        Pending
                    </span>';
        }

        if($this->user_confirmation === 1) {
            return '<span  class="badge badge-success" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
                        Approved
                    </span>';
        }

        if($this->user_confirmation === 2) {
            return '<span  class="badge badge-error" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
                        Rejected
                    </span>';
        }

    }

    public static function statuses()
    {
        if(access()->user()->hasRole('Administrator')) {
            return [ '0' => 'Pending', '1' => 'Approved', '2' => 'Rejected', '3' => 'Pre approved' ];
        } else {
            return [ '0' => 'Pending', '2' => 'Rejected', '3' => 'Pre approved' ];
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
