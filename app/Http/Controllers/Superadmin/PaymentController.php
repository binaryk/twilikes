<?php namespace App\Http\Controllers\Superadmin;

use \App\Http\Controllers\Basic\BaseController;
use App\Models\Access\User\User;
use App\Models\Bill;
use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

use App\Repositories\Superadmin\Nomenclatoare\Payment\Grid;
use App\Repositories\Superadmin\Nomenclatoare\Payment\Rows;
use App\Repositories\Superadmin\Nomenclatoare\Payment\Form;
use App\Repositories\Superadmin\Nomenclatoare\Payment\Action;

class PaymentController extends BaseController
{

    /**
     * $this->data() - vine din BaseController cu chestii comune (breadcrumb, meniu, ....)
     */
    public function getGrid()
    {
        $this->makeNavigation();
        return
            view('superadmin.nomenclatoare.payment.grid.index')
                ->with( $this->data('Payment', 'pentru filtrare') )
                ->withGrid( (new Grid())->parameters( Input::all()) )
                ->withToolbar( str_replace([Chr(13), Chr(10), Chr(9)], '',
                    view('superadmin.nomenclatoare.payment.grid.toolbar')
                        ->withActionRoute(\URL::route('superadmin.nomenclatoare.payment.get-action-form', ['action' => 'insert']))
                        ->withRefreshRoute(\URL::route('superadmin.nomenclatoare.payment.refresh', ['user_id' => 1]))
                        ->render()) )
                ->withIcon(NULL)
                ->withDescription(NULL)
            ;
    }

    public function refresh($user_id = null)
    {
        set_time_limit(600);
        ini_set('max_execution_time', 600);
        if($user_id){

            $user =  User::find($user_id);
            $real_unpaid                = $user->real_unpaid;


            if( $real_unpaid >= 20 ){
                $user->payment_status   = 'pending';
                $user->unpaid           = $real_unpaid;
                $user->save();
            }
            dd("Actualizare finilalizata pt acest user : ".$user_id);

        }else{
            //pentru toti
            $users = User::all();
            foreach($users as $user){
                var_dump('user id : '.$user->id);
                $real_unpaid                = $user->real_unpaid;

                if( $real_unpaid >= 20 ){
                    $user->payment_status   = 'pending';
                    $user->unpaid           = $real_unpaid;
                    $user->status           = 1;
                    $user->save();
                }
            }

            dd('Actualizare finisata');
        }
        return redirect()->back();

    }

    public function refreshFrom($from_id, $to_id)
    {
        set_time_limit(600);
        ini_set('max_execution_time', 600);
        //pentru toti
        $users = User::where('id','>=',$from_id)->where('id','<=',$to_id)->get();

        foreach($users as $user){
            var_dump('user id : '.$user->id);
            $real_unpaid                = $user->real_unpaid;
            if( $real_unpaid >= 20 ){
                $user->status           = 1;
                $user->payment_status   = 'pending';
                $user->unpaid           = $real_unpaid;
                $user->save();
            }
        }

        dd('Actualizare finisata');
        return redirect()->back();

    }


    public function diff($id)
    {
        $user = User::find($id);
        $real = $user->real_unpaid;
        dd('Banii reali : '.$real.' , banii din coloana unpaid: '. $user->unpaid.' , diferenta: '. ($real -  $user->unpaid));
    }

    /*
     * Generarea inregistrari pentru Grid
     */
    public function dataSource()
    {
        return (new Rows())->parameters(Input::all())->response();
    }

    /*
     * Trimite catre index formularul "actions"
     */
    public function getActionForm($action, $id = NULL)
    {

        $form =
            (new Form($action, $id))
                ->setGridId("gridDomains")
                ->route(\URL::route('superadmin.nomenclatoare.payment-do-action', ['action' => $action, 'id' => $id]))
        ;
        return \Response::json([
            'caption' => $form->caption(),
            'form'    => $form->render(),
            'action'  => $form->actionCaption(),
            'route'   => $form->getRoute(),
            'impact' => $action,
        ]);
    }

    /**
     * do Action - se face adaugarea/modificarea/stergere
     */
    public function doAction( $action, $id = NULL )
    {
//        dd(Input::all());
        $data = Input::get('data');
        if($data['payment_status'] == 'paid'){
            $data = $this->generateBill($data, $id);
        }

        $action = (new Action($action, $id))->data($data);
        return \Response::json($action->commit());
    }

    public function generateBill($data, $user_id)
    {
        $user = User::find($user_id);
//        $user->unpaid = $user->unpaid - floatval($data['unpaid']);
        Bill::create([
            'user_id' => $user_id,
            'amount' => $data['unpaid'],
            'payout_type' => Payment::methods2()[$data['payment_method']],
            'payout_date' => Carbon::now(),
        ]);
        $user->paid += floatval($data['unpaid']);
        $user->unpaid = 0;
        $user->save();
        $data['unpaid'] = 0;
        return $data;
    }

}