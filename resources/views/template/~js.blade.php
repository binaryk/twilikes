<script src="{!! asset('theme/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('theme/js/progressbar/bootstrap-progressbar.min.js') !!}"></script>
<script src="{!! asset('theme/js/icheck/icheck.min.js') !!}"></script>
<script src="{!! asset('theme/js/custom.js') !!}"></script>
<script src="{!! asset('theme/js/pace/pace.min.js') !!}"></script>
<script src="{!! asset('components/vue/dist/vue.js') !!}"></script>
<script src="{!! asset('components/kendo/js/kendo.all.min.js') !!}"></script>

{!! HTML::script('theme/js/sweetalert/dist/sweetalert.min.js') !!}
{!! HTML::script('custom/js/binaryk/ui/Spinner.js') !!}
{!! HTML::script('custom/js/binaryk/ui/Afirm.js') !!}
<script>
    var Spinner = new App.Spinner('#spinner')
    var token = $('meta[name="csrf_token"]').attr('content');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': token,
            'X-XSRF-TOKEN': token,
            'Access-Control-Allow-Origin' : '*',
        },
        '_token' : token,
        async    : false
    });
    var _config = {
        base: "{!! url('/') !!}"
    };
    $(document).ready(function(){
        console.log('init');
        @yield('jquery-document-ready')
    })
</script>
@yield('js')
@yield('before_b')
    <script src="{!! asset('assets/bundle.js') !!}"></script>
@yield('after_b')
@yield('custom-javascript-files')

