<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActualReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areports', function(Blueprint $t) {
                $t->increments('id');
                $t->integer('post_id')->index();
                $t->integer('user_id')->index();
                $t->datetime('last_click');
                $t->string('visitor_ip')->index();
                $t->string('device');
                $t->integer('domain_id');
                $t->integer('user_post_id');
                $t->tinyInteger('phone')->default(0);
                $t->tinyInteger('tablet')->default(0);
                $t->tinyInteger('desktop')->default(0);
                $t->timestamps();
                $t->softDeletes();
                $t->tinyInteger('quality');
                $t->float('price',8,3);
                $t->string('referrer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('areports');
    }
}
