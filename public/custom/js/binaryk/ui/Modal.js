var App;
(function (App) {
    var Ui;
    (function (Ui) {
        class Modal {
            constructor(parameters) {
                this.parameters = parameters;
                this.showMessages = (messages, type) => {
                    var html = '';
                    for (var i = 0; i < messages.length; i++) {
                        html += messages[i] + '<br/>';
                    }
                    $(this.parameters.id + ' .modal-body').append('<div class="alert alert-' + type + '">' + html + '</div>');
                };
                this.show = (title, body, footer) => {
                    if (typeof title === 'string') {
                        this.parameters.title = title;
                    }
                    if (typeof body === 'string') {
                        this.parameters.body = body;
                    }
                    if (typeof footer === 'string') {
                        this.parameters.footer = footer;
                    }
                    $(this.parameters.id + ' .modal-title').html(this.parameters.title);
                    $(this.parameters.id + ' .modal-body').html(this.parameters.body);
                    $(this.parameters.id + ' .modal-footer').html(this.parameters.footer);
                    $(this.parameters.id).modal('show');
                };
                this.hide = () => {
                    $(this.parameters.id).find('input').val('');
                    $(this.parameters.id).modal('hide');
                };
                this.listen();
            }
            listen() {
                const _that = this;
                var functions = [];
                $('button[data-modal-action]').map(function () {
                    functions.push($(this).data('modal-action'));
                    $(this).on('click', function (e) {
                        e.preventDefault();
                        let func = $(this).data('modal-action');
                        _that[func]();
                    });
                });
            }
        }
        Ui.Modal = Modal;
    })(Ui = App.Ui || (App.Ui = {}));
})(App || (App = {}));
//# sourceMappingURL=Modal.js.map