<?php namespace App\Repositories\Superadmin\Nomenclatoare\PersonalPrices;

use App\Models\Nomenclatoare\PersonalPrice;

class Action extends \App\Comptechsoft\Form\Action
{

	public function __construct($action, $id)
	{
		parent::__construct($action, $id);

		if( $action == 'insert' )
        {
            $record = new PersonalPrice();
        }
        else
        {
            $record = PersonalPrice::find( (int) $id);
        }

        $this->record($record);

		$this
//			->addRule(['insert'], 'country_id', 'unique:earnings', 'Country must by unique')
			->addRule(['insert', 'update'], 'desktop_price', 'required', 'Price is require')
//			->addRule(['insert', 'update'], 'title', 'max:128', 'Numărul maxim de caractere este 128')
		;
	}
}