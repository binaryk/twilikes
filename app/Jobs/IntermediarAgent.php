<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Referral;
use Carbon\Carbon;
use App\Models\Access\User\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\Intermediar\IntemediarTrait;
use Illuminate\Support\Facades\DB;

class IntermediarAgent extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, IntemediarTrait;
    protected $agent;
    protected $request_ip;
    protected $post_id;
    protected $user_id;
    protected $referrer;
    protected $domain_id;
    protected $user_post_id;
    protected $incrementMoney;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $agent, $request_ip, $post_id, $user_id, $referrer, $domain_id, $user_post_id, $incrementMoney = false)
    {
        $this->agent = $agent;
        $this->request_ip = $request_ip;
        $this->post_id = $post_id;
        $this->user_id = $user_id;
        $this->referrer = $referrer;
        $this->domain_id = $domain_id;
        $this->user_post_id = $user_post_id;
        $this->incrementMoney = $incrementMoney;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::find($this->user_id);
        if($user->personal == 2){
           $this->personalUsersWithout100($user);
        }
        if($user->personal == 1){
            $this->personalUser($user);
        }else{
            $this->simpleUser($user);
        }

    }

    public function personalUser($user)
    {
//        \Log::debug('Pas 4 user_id: '.$this->user_id.', ip: '.$this->request_ip);
        if(! $this->selfTodayClicked($this->request_ip,$this->post_id, $this->user_id)){
            $price = $this->getPrice($this->request_ip, $this->agent, $user->personal, $this->user_id);
            $selfClicks = $this->updateReportTable($this->referrer, $this->user_id,$this->post_id,$this->domain_id,$this->user_post_id,$this->request_ip, $price, 1);
            $this->incUserAmount($user, $price);
//ZZZ       $this->incUserAmount($this->user_id,$price, $this->request_ip, $this->post_id,$user->personal, $selfClicks->quality);
        }

    }

    public function incUserAmount($user, $amount)
    {

//        $user->unpaid = floatval(number_format($amount, 3,'.','')) + floatval(number_format($user->unpaid, 3,'.',''));
//        $user->unpaid = floatval(number_format($user->unpaid, 3,'.',''));
        if ($user->unpaid >= 20) {
            $user->payment_status = "pending";
        }
        $user->save();
        if ($user->invited_by) {
            $invitedByUser = User::find($user->invited_by);
            $summ = config('twilikes.referral') * $amount;
            $summ = floatval($summ);
//            $invitedByUser->unpaid = floatval($invitedByUser->unpaid) + $summ;
            $invitedByUser->save();
            Referral::create([
                'data' => Carbon::now()->toDateString(),
                'invited_by_id' => $invitedByUser->id,
                'invited_id' => $user->id,
                'earning' => $summ
            ]);
        }
    }

    public function simpleUser($user)
    {
        $selfClicks = $this->selfTodayClicked($this->request_ip,$this->post_id, $this->user_id);
        $price = $this->getPrice($this->request_ip, $this->agent, $user->personal, $this->user_id);
        $price = floatval($price);
        if(! $selfClicks){
            /*aici e practic imposibil sa intri, pentru ca in @alive se creeaza instanta*/
//            $user->unpaid = floatval(number_format($price, 3)) + floatval(number_format($user->unpaid, 3,'.',''));
            if ($user->unpaid >= 20) {
                $user->payment_status = "pending";
            }
            $user->save();

            $selfClicks = $this->updateReportTable($this->referrer, $this->user_id,$this->post_id,$this->domain_id,$this->user_post_id,$this->request_ip, $price, 1);



        }

        /*aici se intra cand el a fost pe pagina dar nu a dat click*/
        if( ($selfClicks && $selfClicks->quality == 0) ){

//            $user->unpaid = floatval(number_format($price, 3,'.','')) + floatval(number_format($user->unpaid, 3,'.',''));
            if ($user->unpaid >= 20) {
                $user->payment_status = "pending";
            }
            $user->save();

            $selfClicks->quality = 1;
            $selfClicks->price = $price;
            $selfClicks->save();
        }
    }

    public function personalUsersWithout100($user)
    {
        $selfClicks = $this->selfTodayClicked($this->request_ip,$this->post_id, $this->user_id);
        $price = $this->getPrice($this->request_ip, $this->agent, $user->personal, $this->user_id);
        $price = floatval($price);
        if(! $selfClicks){
            /*aici e practic imposibil sa intri, pentru ca in @alive se creeaza instanta*/
            $selfClicks = $this->updateReportTable($this->referrer, $this->user_id,$this->post_id,$this->domain_id,$this->user_post_id,$this->request_ip, $price, 1);
            $this->incUserAmount($user, $price);
//            $this->incUserAmount($this->user_id,$price, $this->request_ip, $this->post_id,$user->personal, $selfClicks->quality);
        }

        /*aici se intra cand el a fost pe pagina dar nu a dat click*/
        if( ($selfClicks && $selfClicks->quality == 0) ){
//ZZZ       $this->incUserAmount($this->user_id,$price, $this->request_ip, $this->post_id,$user->personal, $selfClicks->quality);

            $selfClicks->quality = 1;
            $selfClicks->price = $price;
            $selfClicks->save();
            $this->incUserAmount($user, $price);

        }

    }
}
