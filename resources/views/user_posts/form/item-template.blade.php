<template id="user-post">
    <div>
        <div class="counter pull-left">
            <h3># @{{ index + 1}}</h3>
        </div>
        <div class="row item">

            <button v-if="item.canRemove" class="btn btn-xs remove-item" type="button" @click="remove(item)">Remove item &nbsp;<i class="fa fa-remove"></i></button>

            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="photo-zone col-lg-4 col-md-4 col-sm-12" v-bind:class="{'has-error' : item.photo === false && validationIsActivated}">
                    <div class="image-wrapper" v-if="! linkIsValid">
                        <input type="file" name="photo">
                    </div>


                    <div class="col-md-8 col-lg-6 extern-link" v-show="! existsUploadedPhoto" v-bind:class="{'has-error' : item.photo === false && validationIsActivated, 'validLink' : linkIsValid}">
                        <div class="input-group" v-if="!!! linkIsValid" v-bind:class="{'has-error' : linkIsValid === false}">
                            <input type="text" class="form-control" v-model="item.link" placeholder="Paste image URL" v-on:keyup.enter="external">
                     <span class="input-group-btn">
                            <button class="confirm-btn btn btn-primary" aria-hidden="true" type="button" @click='external'> <i class="fa fa-plus"></i></button>
                     </span>
                        </div>
                        <span    class="error" v-if="!! item.link && linkIsValid === false">Link is invalid</span>
                        <div class="uploaded-image" v-if="linkIsValid"></div>
                    </div>

                    <span class="help error" v-if="item.photo === false && validationIsActivated">Photo is required</span>
                </div>


                <div class="text-zone col-lg-4 col-md-8 col-sm-12">
                    <div class="form-group" v-bind:class="{'has-error' : item.title.length === 0 && validationIsActivated}">
                        <input type="text" class="form-control" placeholder="Item Title (ex. Chocolate Facials)"  :name="item.title" v-model="item.title">
                        <span class="help error" v-if="item.title.length === 0 && validationIsActivated">Subtitle field is required</span>
                    </div>
                    <div class="form-group" v-bind:class="{'has-error' : false && item.description.length === 0 && validationIsActivated}">
                    <textarea :name="item.description" v-model="item.description" id="item.description" cols="50" rows="6" class="form-control note-editor
                note-editor-margin" placeholder="Description...(optional)"></textarea>
                        <span class="help error" v-if="false && item.description.length === 0 && validationIsActivated">Description field is required</span>
                    </div>
                </div>
            </div>

        </div>

    </div>
</template>