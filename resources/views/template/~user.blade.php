<li class="">
    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        <img src="{!! asset('img/avatar-icon.png') !!}" alt="">@if(access()->user()) {!! access()->user()->name !!} @endif
        <span class=" fa fa-angle-down"></span>
    </a>
    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
        <li><a href="{!! route('frontend.user.dashboard') !!}">  Profile</a>
        </li>
        @role('Administrator')
        <li>
            <a href="{!! url('admin/dashboard') !!}">
                <span>Administration</span>
            </a>
        </li>
        @endauth
        <li><a href="{!! route('auth.logout') !!}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
        </li>
    </ul>
</li>