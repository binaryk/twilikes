var $ = require('jquery');
var Vue = require('vue');
/*var config = {
    //base: "http://dashboard.twilikes.com/"
    //base: "http://localhost/twilikes/public/"
    //base: "http://supertare.theviralityhey.me/"
}*/
window.Intermediar =  new Vue({
    el: '#intermediar',
    ready: function(){
        //this.sendAlive();
    },
    data:{
        user_id: null,
        post_id: null,
        domain_id: null,
        user_post_id: null,
        cors: null,
        referrer: '',
        loading: false,
        laddaText: 'START GALLERY',
        startButton:  'Start',
        photo: ''
    },
    watch: {
        cors: function(val){
            this.sendAlive()
        }
    },
    methods: {
        sendAlive: function(){
            var self = this;
            $.ajax({
                url: self.cors + 'api/client-alive',
                type: 'get',
                dataType: 'json',
                async: true,
                headers: { 'Access-Control-Allow-Origin' : '*' },
                data: {
                    user_id: self.user_id,
                    post_id: self.post_id,
                    domain_id: self.domain_id,
                    user_post_id: self.user_post_id
                },
                success: function (data) {
                },
                complete:function(){
                }
            });
        },
        toLink: function(link){
            this.agent(link);
        },
        agent:function(link){
            var self = this;
            this.laddaText = "Loading...";
            this.startButton = "Loading...";
            this.loading = true;
            $.ajax({
                url: self.cors + 'api/client-agent',
                type: 'get',
                dataType: 'json',
                async: true,
                headers: { 'Access-Control-Allow-Origin' : '*' },
                data: {
                    user_id: self.user_id,
                    post_id: self.post_id,
                    domain_id: self.domain_id,
                    user_post_id: self.user_post_id
                },
                success: function (data) {
                    //self.loading = false;
                    location.href = link;
                },
                complete:function(){
                    //location.href = link;
                }
            });
        }
    }
})