<?php namespace App\Http\Controllers\Superadmin;

use \App\Http\Controllers\Basic\BaseController;
use App\Models\Access\User\User;
use App\Models\Bill;
use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

use App\Repositories\Superadmin\Nomenclatoare\Personals\Grid;
use App\Repositories\Superadmin\Nomenclatoare\Personals\Rows;
use App\Repositories\Superadmin\Nomenclatoare\Personals\Form;
use App\Repositories\Superadmin\Nomenclatoare\Personals\Action;

class PersonalsController extends BaseController
{

    /**
     * $this->data() - vine din BaseController cu chestii comune (breadcrumb, meniu, ....)
     */
    public function getGrid()
    {
        $this->makeNavigation();
        return
            view('superadmin.nomenclatoare.personals.grid.index')
                ->with( $this->data('Payment', 'pentru filtrare') )
                ->withGrid( (new Grid())->parameters( Input::all()) )
                ->withToolbar( str_replace([Chr(13), Chr(10), Chr(9)], '',
                    view('superadmin.nomenclatoare.personals.grid.toolbar')->render()) )
                ->withIcon(NULL)
                ->withDescription(NULL)
            ;
    }

    /*
     * Generarea inregistrari pentru Grid
     */
    public function dataSource()
    {
        return (new Rows())->parameters(Input::all())->response();
    }

    /*
     * Trimite catre index formularul "actions"
     */
    public function getActionForm($action, $id = NULL)
    {

        $form =
            (new Form($action, $id))
                ->setGridId("gridUsers")
                ->route(\URL::route('superadmin.nomenclatoare.personals-do-action', ['action' => $action, 'id' => $id]))
        ;
        return \Response::json([
            'caption' => $form->caption(),
            'form'    => $form->render(),
            'action'  => $form->actionCaption(),
            'route'   => $form->getRoute(),
            'impact' => $action,
        ]);
    }

    /**
     * do Action - se face adaugarea/modificarea/stergere
     */
    public function doAction( $action, $id = NULL )
    {
        $data = Input::get('data');
        $action = (new Action($action, $id))->data($data);
        return \Response::json($action->commit());
    }

}