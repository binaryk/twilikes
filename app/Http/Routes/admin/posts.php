<?php

Route::group(['namespace' => 'Superadmin', 'middleware' => 'admin'], function () {

    Route::get('superadmin/nomenclatoare/posts', 'PostsController@getGrid')
        ->name('superadmin.nomenclatoare.posts.index');

    Route::post('superadmin/nomenclatoare/posts-source', 'PostsController@dataSource')
        ->name('superadmin.nomenclatoare.posts.data-source');

    /*
     * Sa se incarce formularul de actiun (actions form)
     */
    Route::post('superadmin/nomenclatoare/posts-get-action-form/{action}/{id?}', 'PostsController@getActionForm')
        ->name('superadmin.nomenclatoare.posts.get-action-form');

    /*
     * ce se intampla la Adauga/Salveaza/Sterge
     */
    Route::post('superadmin/nomenclatoare/posts-action/{action}/{id?}', 'PostsController@doAction')
        ->name('superadmin.nomenclatoare.posts-do-action');
});