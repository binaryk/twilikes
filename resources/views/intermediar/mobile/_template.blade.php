@include('intermediar.mobile._header')
<!--[if lte IE 9 ]> <body class="ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <body class="not_ie "> <!--<![endif]-->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-85842922-1', 'auto');
    ga('send', 'pageview');

</script>

<div id="intermediar" class="gallery_page no_header custom_profile_wrapper ">
    <div id="custom_mini_header" class="balloon" style="font-size:20px;  background-color: #F6F6F6;" >
        <a href="javascript:void(0);" id="stylized_title" style="color: #000;"> {!! @$subdomain !!} </a>
    </div>



    <div id="thepage_inner" style="background: #f0f0f0;" >

@yield('content')

@include('intermediar.mobile._footer')