<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexesOnReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports', function(Blueprint $t){
//            $t->index('user_id');
//            $t->index('created_at');
//            $t->index('price');
//            $t->index('quality');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports', function(Blueprint $t){
//            $t->dropIndex('user_id');
//            $t->dropIndex('created_at');
//            $t->dropIndex('price');
//            $t->dropIndex('quality');
        });
    }
}
