@extends('backend.layouts.master')
@section ('title')
    Mesaj standard pentru banare
@stop

@section('page-header')
    <h1>
        Mesaj standard pentru banare
    </h1>
@endsection
@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Adauga aici mesajul</h3>

        </div><!-- /.box-header -->
        </div><!-- /.box-header -->

    <div class="box-body">

            <form id="demo-form2" data-parsley-validate="" method="POST" action="{!! route('admin_message.store') !!}" class="form-horizontal form-label-left" novalidate="" _lpchecked="1">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <input type="hidden" id="{!! @$message->id !!}">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Message
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea type="text" name="message" class="form-control col-md-7 col-xs-12" rows="7" > {!! @$message->message !!}</textarea>
                        <ul class="parsley-errors-list" id="parsley-id-0217"></ul>
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
    </div><!-- /.box-header -->

@stop