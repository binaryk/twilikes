<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreferralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areferrals', function(Blueprint $t){
            $t->bigIncrements('id');
            $t->string('data');
            $t->integer('invited_by_id')->index();
            $t->integer('invited_id')->index();
            $t->float('earning',8,4)->default(0);
            $t->timestamps();
            $t->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('areferrals');
    }
}
