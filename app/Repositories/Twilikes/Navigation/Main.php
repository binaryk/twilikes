<?php namespace App\Repositories\Twilikes\Navigation;

use App\Models\Archive;
use App\Models\Director;
use App\Models\File;

class Main
{

	protected $menu = NULL;
	protected $user = NULL;


    protected function createMenu()
    {
		$this->menu->addOption('main-gali',
			\App\Repositories\Ui\Navigation\Option::make()
				->class(\Request::is('/') ? 'active' : '')
				->url(\URL::to('/'))
				->icon('icon fa icon-home')
				->caption( trans('sidebar.home') )
				->show(true)
		);

    }

	public function __construct( \App\Repositories\Ui\Navigation\Sidemenu $menu, \App\Models\Access\User\User $user = NULL)
	{
		$this->user = $user;
		$this->menu = $menu;
		$this->createMenu();
	}

}
