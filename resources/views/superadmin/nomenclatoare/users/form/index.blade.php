<div class="row">

    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Textbox::make()
            ->name( $field = 'name')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Name' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->name : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => 'Name...',
                'data-field'  => $field,
                'data-type'   => 'textbox',
                'disabled' => 'disabled',
            ])
            ->help('')
            ->render()
        !!}
	</div>

    <div class="col-md-12 form-group">
        {!!
        App\Comptechsoft\Ui\Controls\Selectbox::make()
        ->label('Confirmed ?')
        ->icon('')
        ->wrapper('col-sm-10')
        ->lclass('col-sm-2')
        ->name('confirmed')
        ->value(@$record->confirmed)
        ->addClass('form-control form-data-source')
        ->withAttribute('data-field', 'confirmed')
        ->withAttribute('data-type', 'combobox')
        ->disabled(false)
        ->options(\App\Models\Access\User\User::confirmed())
        ->render()
        !!}
    </div>

</div>