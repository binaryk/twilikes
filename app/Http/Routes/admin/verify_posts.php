<?php

Route::group(['namespace' => 'Superadmin', 'middleware' => 'admin'], function () {

    Route::get('superadmin/nomenclatoare/verify_posts', 'VerifyPostsController@getGrid')
        ->name('superadmin.nomenclatoare.verify_posts.index');

    Route::get('superadmin/nomenclatoare/verify_posts-source/{post_id}', 'VerifyPostsController@source')
        ->name('superadmin.nomenclatoare.verify_posts.source');

    Route::post('superadmin/nomenclatoare/verify_posts-source', 'VerifyPostsController@dataSource')
        ->name('superadmin.nomenclatoare.verify_posts.data-source');

    /*
     * Sa se incarce formularul de actiun (actions form)
     */
    Route::post('superadmin/nomenclatoare/verify_posts-get-action-form/{action}/{id?}', 'VerifyPostsController@getActionForm')
        ->name('superadmin.nomenclatoare.verify_posts.get-action-form');

    /*
     * ce se intampla la Adauga/Salveaza/Sterge
     */
    Route::post('superadmin/nomenclatoare/verify_posts-action/{action}/{id?}', 'VerifyPostsController@doAction')
        ->name('superadmin.nomenclatoare.verify_posts-do-action');
});