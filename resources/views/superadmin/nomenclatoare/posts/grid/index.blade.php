@extends('__layouts.pages.datatable.index')
@section('css')
	@parent
	<link rel="stylesheet" href="{!! asset('vendor\file-input\css\fileinput.min.css') !!}">
	<link rel="stylesheet" href="{!! asset('custom/css/file-icons.css') !!}">
	<style>
	</style>
	@stop

@section('custom-javascript-files')
	@parent
	<!-- includ js-ul pentru datatable creat dinamic -->
	{!! 
	App\Comptechsoft\Ui\Html\Scripts::make([
		'custom/js/posts/grid',
		'custom/js/posts/File',
		'custom/js/grids/index',
	])->render()
	!!}

	{!! HTML::script('vendor/file-input/js/fileinput.min.js') !!}
@stop

@section('jquery-document-ready')
	@parent
	var grid  = new gridPosts();
	var index = new indexGrid({
		grid       : grid,
		toolbar    : '{!! $toolbar !!}',
		_token     : '{{ csrf_token() }}',
		form_width : 12
	}).init();

	index.afterShowform = function(impact){
		(new App.File(impact)).init();
	}
@stop
