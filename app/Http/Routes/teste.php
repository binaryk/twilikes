<?php
Route::group(['namespace' => 'Test', 'middleware' => 'admin'], function () {


    Route::get('users-by-earnings/{days}', 'TestController@getUsersByEarningsDesc')->name('admin.users_earnings');
    Route::get('images-teste', 'TestController@images')->name('admin.images');
    Route::get('posturi', 'TestController@posts')->name('admin.posts-rep');


});

    Route::get('reports-job', 'Test\TestController@reportsJob')->name('admin.reports_job');
    Route::get('/getcountry', function(){
        $data = null;
        $ip = (new \App\Http\Controllers\Intermediar\IntermediarController())->getRequestIp();
        exec ( public_path("data/geolocation/pl/ip2location.pl") ." -datafile ".public_path('data/geolocation/IP-COUNTRY.BIN')." -ip ".$ip." -format xml", $data );
//        $code = codecountry("24.114.29.162");
//        dd($code);
        if(! $code = codecountry($ip) ){
            $code = "OTH";
        }
        dd($code, 'final tara', (new \App\Http\Controllers\Intermediar\IntermediarController())->getCountryId($ip));
        return null;
    });

Route::get('/edutest', function() {

    $posts = \App\Models\Nomenclatoare\Post::orderBy('id','desc')->get();
    $arr = '';
    foreach($posts as $post) {
        if(! file_exists($post->path)){
            $arr .= ', '.$post->id;
        }
    }
    dd($arr);
    
    die();

});

    Route::group(['namespace' => 'Superadmin', 'middleware' => 'admin'], function () {


        Route::get('loginas/{id}', '\App\Http\Controllers\Backend\Access\User\UserController@loginAs');


        Route::get('superadmin/resolveunpaid', function(){
            set_time_limit(600);
            ini_set('max_execution_time', 600);

            $platiti = [208, 195, 321, 259, 262, 119, 68, 49, 200, 45, 43, 19, 17, 23, 16, 14, 12, 361, 39, 15];
//            $start_date = \Carbon\Carbon::now()->subDays(8);
            $start_date = \Carbon\Carbon::createFromDate(2016, 7, 24);

            $users = \App\Models\Access\User\User::skip(600)->take(400)->get();

            foreach($users as $user){
                if(! in_array($user->id, $platiti)){
//                    $user = \App\Models\Access\User\User::find(339);
                    $data = DB::table('areports')
                        ->where('user_id', '=', $user->id)
                        ->whereDate('created_at','>=',$start_date)
                        ->sum('price');
//                        ->get();


                    $hasFactura = DB::table('bills')
                        ->where('user_id', '=', $user->id)
                        ->whereDate('created_at','>=',$start_date)
                        ->first();

                    if($hasFactura){
                        $user->unpaid = $data - $hasFactura->amount;
                    }else{
                        $user->unpaid = $data;
                    }

                    $user->save();

                }
            }

            // 25 - 8 din reports - (are plata in 1 august scade plata aia)

            dd('done');
        });
    });