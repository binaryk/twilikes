@extends('template.~template')
@section('page-right')

    <div class="alert alert-info text-center info-top" style="font-size: 15px;">
        To generate link or preview campaign, put mouse over image.
    </div>
@stop


@section('content')
    <campaigns></campaigns>

@stop

@section('css')
    @parent
@stop
@section('js')
    @parent
@stop