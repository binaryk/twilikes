<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Foundation\Auth\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\Intermediar\IntemediarTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
class EmailUsers extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $email;
    protected $path;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $email, $path )
    {
        $this->email = $email;
        $this->path = $path;
    }

    /**
     * Execute the job.
     *
     * @return void
*/
    public function handle()
    {
        $email = $this->email;
        if($email->destination == "null") {
            $this->sendToAll();
        } else {
            $this->sendToSelected();
        }
    /*  Mail::send('superadmin.nomenclatoare.emails.send.send', ['content' => $this->email->content], function ($message) use ($email, $path) {
            $message->subject($email->title);
            $message->to("lupacescueduard@yahoo.com");

            if ($path) {
                $message->attach($path);
            }
        });*/
    }

    public function sendToSelected()
    {
        $email = $this->email;
        $path  = $this->path;
        $destinations = explode(",",$this->email->destination);
        foreach($destinations as $dest) {
            Mail::send('superadmin.nomenclatoare.emails.send.send', ['content' =>$email->content], function ($message) use ($email, $path, $dest) {
                $message->subject($email->title);
                $message->to($dest);
                if ($path) {
                    $message->attach($path);
                }
            });
        }
    }

    public function sendToAll()
    {
        $email = $this->email;
        $path  = $this->path;
        foreach(User::all() as $user) {
            if($user->status > 0 && $user->confirmed > 0){

                Mail::send('superadmin.nomenclatoare.emails.send.send', ['content' => $email->content], function ( $message ) use ( $email, $user, $path ) {
                    $message->subject( $email->title );
                    $message->to( $user->email );

                    if ( $path ) {
                        $message->attach( $path );
                    }
                });
                
            }
        }
    }
}
