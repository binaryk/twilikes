var App;
(function (App) {
    class Afirm {
        constructor() {
        }
        success(message) {
            swal("Succes!", message, "success");
        }
        warning(message) {
            swal("Atenție!", message, "warning");
        }
        error(message) {
            swal("Eroare!", message, "error");
        }
        question(title, message, type) {
            const _that = this;
            swal({
                title: title,
                text: message,
                type: type ? type : "success",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Da!",
                closeOnConfirm: true
            }, function (isConfirm) {
                if (isConfirm) {
                    _that.onConfirm();
                }
                else {
                    _that.onCancel();
                }
            });
        }
        onCancel() {
            console.log('cancel');
        }
        onConfirm() {
            console.log('confirm');
        }
    }
    App.Afirm = Afirm;
})(App || (App = {}));
//# sourceMappingURL=Afirm.js.map