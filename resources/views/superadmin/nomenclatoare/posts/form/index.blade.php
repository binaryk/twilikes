<div class="row">

    <div class="form-group col-xs-12" id="image-upload-box">
        <label class="col-sm-2 control-label">Încarcă fișierul</label>
        <input type="file" name="file" class="form-control filestyle" id="file">
    </div>

    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Textbox::make()
            ->name( $field = 'title')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Title' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->title : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => 'Title...',
                'data-field'  => $field,
                'data-type'   => 'textbox'
            ])
            ->help('')
            ->render()
        !!}
	</div>

    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Textbox::make()
            ->name( $field = 'link')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Link poza' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->link : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => 'http://...',
                'data-field'  => $field,
                'data-type'   => 'textbox'
            ])
            ->help('')
            ->render()
        !!}
	</div>

    <div class="col-md-12 form-group">
            {!!
            App\Comptechsoft\Ui\Controls\Selectbox::make()
            ->label('Category')
            ->icon('')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->name('category')
            ->value($action != 'insert' ? $record->category : '')
            ->addClass('form-control form-data-source')
            ->withAttribute('data-field', 'category')
            ->withAttribute('data-type', 'combobox')
            ->disabled($action == 'delete')
            ->options(App\Models\Nomenclatoare\Category::category())
            ->render()
            !!}
    </div>

  <div class="row form-group">
      <div class="col-xs-12" style="border-bottom: 1px dashed #eee;">
          {!!
          App\Repositories\Ui\Controls\Editbox::make()
          ->name( $field = 'description')
          ->class('form-group mb')
          ->wrapper('col-sm-10')
          ->lclass('col-sm-2')
          ->caption( 'Descriere' )
          ->placeholder('')
          ->icon(NULL)
          ->value( $action != 'insert' ? $record->{$field} : '')
          ->attributes([
              'id'          => $field,
              'class'       => 'form-control form-data-source  note-editor note-editor-margin',
              'placeholder' => 'Descrierea postului...',
              'data-field'  => $field,
              'data-type'   => 'textbox'
          ])
          ->setFlag( 'readonly', $action == 'delete' )
          ->help('')
          ->render()
      !!}
      </div>
  </div>
</div>