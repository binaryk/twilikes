<div class="row">

    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Textbox::make()
            ->name( $field = 'name')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Name' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->name : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => 'Title...',
                'data-field'  => $field,
                'data-type'   => 'textbox',
                'disabled' => 'disabled',
            ])
            ->help('')
            ->render()
        !!}
	</div>

    <div class="col-md-12 form-group">
        {!!
        App\Comptechsoft\Ui\Controls\Selectbox::make()
        ->label('Payment method')
        ->icon('')
        ->wrapper('col-sm-10')
        ->lclass('col-sm-2')
        ->name('payment_status')
        ->value($record->payment_method)
        ->addClass('form-control form-data-source')
        ->withAttribute('data-field', 'payment_method')
        ->withAttribute('data-type', 'combobox')
        ->disabled(true)
        ->options(\App\Models\Payment::methods2())
        ->render()
        !!}
    </div>

    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Textbox::make()
            ->name( $field = 'unpaid')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Unpaid' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->unpaid : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => 'Title...',
                'data-field'  => $field,
                'data-type'   => 'textbox',
            ])
            ->help('')
            ->render()
        !!}
    </div>
    @if(isset($record->payment_method) && $record->payment_method == 1)
        <div class="col-xs-12 form-group">
            {!!
                App\Repositories\Ui\Controls\Textbox::make()
                ->name( $field = 'paypal_email')
                ->class('form-group mb')
                ->wrapper('col-sm-10')
                ->lclass('col-sm-2')
                ->caption( 'Paypal email' )
                ->icon(NULL)
                ->value( $action != 'insert' ? $record->paypal_email : '')
                ->attributes([
                    'id'          => $field,
                    'class'       => 'form-control form-data-source',
                    'placeholder' => 'Title...',
                    'data-field'  => $field,
                    'data-type'   => 'textbox',
                    'disabled' => 'disabled',
                ])
                ->help('')
                ->render()
            !!}
        </div>

        <div class="col-xs-12 form-group">
            {!!
                App\Repositories\Ui\Controls\Textbox::make()
                ->name( $field = 'paypal_name')
                ->class('form-group mb')
                ->wrapper('col-sm-10')
                ->lclass('col-sm-2')
                ->caption( 'Paypal Name' )
                ->icon(NULL)
                ->value( $action != 'insert' ? $record->paypal_name : '')
                ->attributes([
                    'id'          => $field,
                    'class'       => 'form-control form-data-source',
                    'placeholder' => 'Title...',
                    'data-field'  => $field,
                    'data-type'   => 'textbox',
                    'disabled' => 'disabled',
                ])
                ->help('')
                ->render()
            !!}
        </div>
    @endif
    @if(isset($record->payment_method) && $record->payment_method == 3)
        <div class="col-xs-12 form-group">
            {!!
                App\Repositories\Ui\Controls\Textbox::make()
                ->name( $field = 'skrill_email')
                ->class('form-group mb')
                ->wrapper('col-sm-10')
                ->lclass('col-sm-2')
                ->caption( 'Skrill email' )
                ->icon(NULL)
                ->value( $action != 'insert' ? $record->skrill_email : '')
                ->attributes([
                    'id'          => $field,
                    'class'       => 'form-control form-data-source',
                    'placeholder' => 'Title...',
                    'data-field'  => $field,
                    'data-type'   => 'textbox',
                    'disabled' => 'disabled',
                ])
                ->help('')
                ->render()
            !!}
        </div>

        <div class="col-xs-12 form-group">
            {!!
                App\Repositories\Ui\Controls\Textbox::make()
                ->name( $field = 'skrill_name')
                ->class('form-group mb')
                ->wrapper('col-sm-10')
                ->lclass('col-sm-2')
                ->caption( 'Skrill Name' )
                ->icon(NULL)
                ->value( $action != 'insert' ? $record->skrill_name : '')
                ->attributes([
                    'id'          => $field,
                    'class'       => 'form-control form-data-source',
                    'placeholder' => 'Title...',
                    'data-field'  => $field,
                    'data-type'   => 'textbox',
                    'disabled' => 'disabled',
                ])
                ->help('')
                ->render()
            !!}
        </div>
    @endif

    @if(isset($record->payment_method) && $record->payment_method == 2)
        <div class="col-xs-12 form-group">
            {!!
                App\Repositories\Ui\Controls\Textbox::make()
                ->name( $field = 'bank_name')
                ->class('form-group mb')
                ->wrapper('col-sm-10')
                ->lclass('col-sm-2')
                ->caption( 'Bank name' )
                ->icon(NULL)
                ->value( $action != 'insert' ? $record->bank_name : '')
                ->attributes([
                    'id'          => $field,
                    'class'       => 'form-control form-data-source',
                    'placeholder' => 'Title...',
                    'data-field'  => $field,
                    'data-type'   => 'textbox',
                    'disabled' => 'disabled',
                ])
                ->help('')
                ->render()
            !!}
        </div>
        <div class="col-xs-12 form-group">
            {!!
                App\Repositories\Ui\Controls\Textbox::make()
                ->name( $field = 'bank_street')
                ->class('form-group mb')
                ->wrapper('col-sm-10')
                ->lclass('col-sm-2')
                ->caption( 'Bank street' )
                ->icon(NULL)
                ->value( $action != 'insert' ? $record->bank_street : '')
                ->attributes([
                    'id'          => $field,
                    'class'       => 'form-control form-data-source',
                    'placeholder' => 'Title...',
                    'data-field'  => $field,
                    'data-type'   => 'textbox',
                    'disabled' => 'disabled',
                ])
                ->help('')
                ->render()
            !!}
        </div>
        <div class="col-xs-12 form-group">
            {!!
                App\Repositories\Ui\Controls\Textbox::make()
                ->name( $field = 'iban')
                ->class('form-group mb')
                ->wrapper('col-sm-10')
                ->lclass('col-sm-2')
                ->caption( 'IBAN' )
                ->icon(NULL)
                ->value( $action != 'insert' ? $record->iban : '')
                ->attributes([
                    'id'          => $field,
                    'class'       => 'form-control form-data-source',
                    'placeholder' => 'Title...',
                    'data-field'  => $field,
                    'data-type'   => 'textbox',
                    'disabled' => 'disabled',
                ])
                ->help('')
                ->render()
            !!}
        </div>
        <div class="col-xs-12 form-group">
            {!!
                App\Repositories\Ui\Controls\Textbox::make()
                ->name( $field = 'holder_name')
                ->class('form-group mb')
                ->wrapper('col-sm-10')
                ->lclass('col-sm-2')
                ->caption( 'Account holder name' )
                ->icon(NULL)
                ->value( $action != 'insert' ? $record->holder_name : '')
                ->attributes([
                    'id'          => $field,
                    'class'       => 'form-control form-data-source',
                    'placeholder' => 'Title...',
                    'data-field'  => $field,
                    'data-type'   => 'textbox',
                    'disabled' => 'disabled',
                ])
                ->help('')
                ->render()
            !!}
        </div>
        <div class="col-xs-12 form-group">
            {!!
                App\Repositories\Ui\Controls\Textbox::make()
                ->name( $field = 'bank_city')
                ->class('form-group mb')
                ->wrapper('col-sm-10')
                ->lclass('col-sm-2')
                ->caption( 'Bank city' )
                ->icon(NULL)
                ->value( $action != 'insert' ? $record->bank_city : '')
                ->attributes([
                    'id'          => $field,
                    'class'       => 'form-control form-data-source',
                    'placeholder' => 'Title...',
                    'data-field'  => $field,
                    'data-type'   => 'textbox',
                    'disabled' => 'disabled',
                ])
                ->help('')
                ->render()
            !!}
        </div>
        <div class="col-xs-12 form-group">
            {!!
                App\Repositories\Ui\Controls\Textbox::make()
                ->name( $field = 'bank_zip')
                ->class('form-group mb')
                ->wrapper('col-sm-10')
                ->lclass('col-sm-2')
                ->caption( 'Bank zip' )
                ->icon(NULL)
                ->value( $action != 'insert' ? $record->bank_zip : '')
                ->attributes([
                    'id'          => $field,
                    'class'       => 'form-control form-data-source',
                    'placeholder' => 'Title...',
                    'data-field'  => $field,
                    'data-type'   => 'textbox',
                    'disabled' => 'disabled',
                ])
                ->help('')
                ->render()
            !!}
        </div>
        <div class="col-xs-12 form-group">
            {!!
                App\Repositories\Ui\Controls\Textbox::make()
                ->name( $field = 'swift_code')
                ->class('form-group mb')
                ->wrapper('col-sm-10')
                ->lclass('col-sm-2')
                ->caption( 'Swift code' )
                ->icon(NULL)
                ->value( $action != 'insert' ? $record->swift_code : '')
                ->attributes([
                    'id'          => $field,
                    'class'       => 'form-control form-data-source',
                    'placeholder' => 'Title...',
                    'data-field'  => $field,
                    'data-type'   => 'textbox',
                    'disabled' => 'disabled',
                ])
                ->help('')
                ->render()
            !!}
        </div>
        <div class="col-xs-12 form-group">
            {!!
                App\Repositories\Ui\Controls\Textbox::make()
                ->name( $field = 'wire_street')
                ->class('form-group mb')
                ->wrapper('col-sm-10')
                ->lclass('col-sm-2')
                ->caption( 'Street' )
                ->icon(NULL)
                ->value( $action != 'insert' ? $record->wire_street : '')
                ->attributes([
                    'id'          => $field,
                    'class'       => 'form-control form-data-source',
                    'placeholder' => 'Title...',
                    'data-field'  => $field,
                    'data-type'   => 'textbox',
                    'disabled' => 'disabled',
                ])
                ->help('')
                ->render()
            !!}
        </div>
        <div class="col-xs-12 form-group">
            {!!
                App\Repositories\Ui\Controls\Textbox::make()
                ->name( $field = 'wire_country')
                ->class('form-group mb')
                ->wrapper('col-sm-10')
                ->lclass('col-sm-2')
                ->caption( 'Country' )
                ->icon(NULL)
                ->value( $action != 'insert' ? $record->wire_country : '')
                ->attributes([
                    'id'          => $field,
                    'class'       => 'form-control form-data-source',
                    'placeholder' => 'Title...',
                    'data-field'  => $field,
                    'data-type'   => 'textbox',
                    'disabled' => 'disabled',
                ])
                ->help('')
                ->render()
            !!}
        </div>
        <div class="col-xs-12 form-group">
            {!!
                App\Repositories\Ui\Controls\Textbox::make()
                ->name( $field = 'wire_city')
                ->class('form-group mb')
                ->wrapper('col-sm-10')
                ->lclass('col-sm-2')
                ->caption( 'City' )
                ->icon(NULL)
                ->value( $action != 'insert' ? $record->wire_city : '')
                ->attributes([
                    'id'          => $field,
                    'class'       => 'form-control form-data-source',
                    'placeholder' => 'Title...',
                    'data-field'  => $field,
                    'data-type'   => 'textbox',
                    'disabled' => 'disabled',
                ])
                ->help('')
                ->render()
            !!}
        </div>

    @endif
    <div class="col-md-12 form-group">
        {!!
        App\Comptechsoft\Ui\Controls\Selectbox::make()
        ->label('Status')
        ->icon('')
        ->wrapper('col-sm-10')
        ->lclass('col-sm-2')
        ->name('payment_status')
        ->value($record->payment_status)
        ->addClass('form-control form-data-source')
        ->withAttribute('data-field', 'payment_status')
        ->withAttribute('data-type', 'combobox')
        ->disabled($action == 'delete')
        ->options(\App\Models\Payment::status2())
        ->render()
        !!}
    </div>
  </div>
</div>