<?php namespace App\Models;

use App\Comptech\Model\Model;
use Carbon\Carbon;
class Country extends Model
{
    protected $table = "countries";
    protected $guarded = ['_token','id'];

    public static function Combobox()
    {
        $result = ['0' => trans('actions.no-selection')];
        $records = self::orderBy('id')->get();
        foreach( $records as $i => $record )
        {
            $result[$record->id] = $record->country;
        }
        return $result;

    }

}