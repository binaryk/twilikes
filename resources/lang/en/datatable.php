<?php

return [
	'search-placeholder' 	=> 'Search...',
	'empty-table'			=> 'No data',
	'zero-records' 			=> "Data not found",

	'current-number'        => ' # ',
	'id'        		    => ' ID ',
	'actions'			    => 'Actions',

	'yes'                   => 'Yes',
	'no'                    => 'No',
];