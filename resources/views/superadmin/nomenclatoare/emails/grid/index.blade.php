@extends('__layouts.pages.datatable.index')
@section('css')
	@parent
	<link rel="stylesheet" href="{!! asset('vendor\file-input\css\fileinput.min.css') !!}">
	<link rel="stylesheet" href="{!! asset('custom/css/file-icons.css') !!}">
	<link rel="stylesheet" href="{!! asset('theme/css/select/select2.min.css') !!}">
	<style>
	</style>
	@stop

@section('custom-javascript-files')
	@parent
	<!-- includ js-ul pentru datatable creat dinamic -->
	{!! 
	App\Comptechsoft\Ui\Html\Scripts::make([
		'custom/js/emails/grid',
		'custom/js/emails/File',
		'custom/js/emails/form',
        'angle/vendor/bootstrap-wysiwyg/bootstrap-wysiwyg',
		'angle/vendor/bootstrap-wysiwyg/external/jquery.hotkeys',
	])->render()
	!!}

	{!! HTML::script('vendor/file-input/js/fileinput.min.js') !!}
	{!! HTML::script('theme/js/select/select2.full.js') !!}

@stop

@section('jquery-document-ready')
	@parent
	var grid  = new gridEmails();
	var index = new indexGrid({
		grid       : grid,
		toolbar    : '{!! $toolbar !!}',
		_token     : '{{ csrf_token() }}',
		form_width : 12
	}).init();

	index.afterShowform = function(impact){
		(new App.File(impact)).init();
        $('.wysiwyg').wysiwyg();

        $(".select2_multiple").select2({
            //maximumSelectionLength: 4,
            //placeholder: "With Max Selection limit 4",
            allowClear: true
        });
	}

@stop
