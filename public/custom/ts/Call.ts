declare var $;
interface IData{
    key: string,
    value: any
}
export default class Call{
    private data: [IData] = {};

    constructor(public url: string, public type: string, public dataType?: string){
        if(! this.dataType) this.dataType = 'json';
    }

    setData = (key: string, data: string) =>
    {
        this.data[key] = data;
        return this;
    }



    setUrl = (url: string) =>
    {
        this.url = url;
        return this;
    }

    onSuccess = (data) => {

    }

    success = (cb) => {
        this.onSuccess = cb;
    }

    request = () => {
        var that = this;
        console.log(that);
        $.ajax({
            url      : that.url,
            type     : that.type,
            data     : that.data,
            success  : function( result )
            {
                //return that.onSuccess(result);
            }
        });
    }
}

