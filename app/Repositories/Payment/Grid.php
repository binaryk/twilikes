<?php namespace App\Repositories\Superadmin\Nomenclatoare\Payment;

use \App\Comptechsoft\Datatable\Column;
use App\Comptechsoft\Datatable\Footer;
use \App\Comptechsoft\Datatable\Header;
use \App\Comptechsoft\Datatable\Ajax;
use \App\Comptechsoft\Datatable\Order;
use \App\Comptechsoft\Datatable\Filter;

class Grid extends \App\Comptechsoft\Datatable\Grid
{

	public function __construct()
	{

		$this->noGlobalFilter();
		$this->footerTotal = [ 3 ];
		/*
		 * II asociez ID la grid
		 */
		$this->id('gridPayment');

		$this->pageLength = 5;

		/*
		 * In ce fisier sa se genereze js-ul pentru .DataTable({...});
		 */
		$this->js( str_replace('\\', '/', public_path('custom/js/payment/grid.js') ));

		/*
		 * De la ce ruta vine raspunsul cu randuri
		 */
		$this->ajax( (new Ajax())->setUrl(\URL::route('superadmin.nomenclatoare.payment.data-source'))->setExtra(['a' => 1, 'b' => 2]) );
		
		/*
		 * Care coloana contine ordinea initiala
		 */
		$this->order( (new Order())->setColumn(1)->setDirection('asc') );

		/*
		 * Definirea coloanelor
		 **/
		$this->addColumns([

			'rec-no' => 
				(new Column())
				->withHeader( 
					(new Header())
					->with(['caption' => '#', 'width' => 5])
				)
				->withFooter(
					(new Footer())
						->with(['caption' => 'Total', 'width' => 5])
				)
				->alignRight(),

			'id' => 
				(new Column())
				->withHeader( 
					(new Header())
					->with(['caption' => 'ID', 'width' => 5])
				)
				->alignCenter()
				->orderable(),

			'name' =>
				(new Column())
				->withHeader( 
					(new Header())
					->with(['caption' => 'Name', 'width' => 15])
				)->orderable(),

			'unpaid' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Coloana unpaid', 'width' => 15])
				)->orderable(),


			'payment_method' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Payment Method', 'width' => 15])
				)->orderable(),

			'status' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Status', 'width' => 20])
				)->orderable(),

			'actions' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Actions'])
				),


		
		]);

	}
}