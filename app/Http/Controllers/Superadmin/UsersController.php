<?php namespace App\Http\Controllers\Superadmin;

use \App\Http\Controllers\Basic\BaseController;
use App\Models\Access\User\User;
use App\Models\Bill;
use App\Models\NewReports;
use App\Models\Payment;
use App\Models\Areport;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

use App\Repositories\Superadmin\Nomenclatoare\Users\Grid;
use App\Repositories\Superadmin\Nomenclatoare\Users\Rows;
use App\Repositories\Superadmin\Nomenclatoare\Users\Form;
use App\Repositories\Superadmin\Nomenclatoare\Users\Action;
use Illuminate\Support\Facades\Mail;

class UsersController extends BaseController
{

    /**
     * $this->data() - vine din BaseController cu chestii comune (breadcrumb, meniu, ....)
     */
    public function getGrid()
    {
        $this->makeNavigation();
        return
            view('superadmin.nomenclatoare.users.grid.index')
                ->with( $this->data('Payment', 'pentru filtrare') )
                ->withGrid( (new Grid())->parameters( Input::all()) )
                ->withToolbar( str_replace([Chr(13), Chr(10), Chr(9)], '',
                    view('superadmin.nomenclatoare.users.grid.toolbar')->render()) )
                ->withIcon(NULL)
                ->withDescription(NULL)
            ;
    }

    public function getTotals()
    {
        return [
            'today' => $this->today(),
            'yesterday' => $this->yesterday()
        ];
    }

    public function today()
    {
        $user_posts = Areport::whereDate('created_at', '=',Carbon::now()->toDateString())->get();
        $summ = 0;
        foreach($user_posts as $post){
            $summ += $post->price;
        }
        $summ = number_format($summ, 4);
        return $summ;
    }

    public function yesterday()
    {
        $user_posts = NewReports::whereDate('created_at', '=',Carbon::yesterday()->toDateString())->get();
        $summ = 0;
        foreach($user_posts as $post){
            $summ += $post->earnings;
        }
        $summ = number_format($summ, 4);
        return $summ;
    }

    /*
     * Generarea inregistrari pentru Grid
     */
    public function dataSource()
    {
        return (new Rows())->parameters(Input::all())->response();
    }

    /*
     * Trimite catre index formularul "actions"
     */
    public function getActionForm($action, $id = NULL)
    {

        $form =
            (new Form($action, $id))
                ->setGridId("gridUsers")
                ->route(\URL::route('superadmin.nomenclatoare.users-do-action', ['action' => $action, 'id' => $id]))
        ;
        return \Response::json([
            'caption' => $form->caption(),
            'form'    => $form->render(),
            'action'  => $form->actionCaption(),
            'route'   => $form->getRoute(),
            'impact' => $action,
        ]);
    }

    /**
     * do Action - se face adaugarea/modificarea/stergere
     */
    public function doAction( $action, $id = NULL )
    {
        $data = Input::get('data');
        $user = User::find($id);
        if($data['confirmed'] == '1') {
            if($user->confirmed == 0) {
                $user->confirmed = 1;
                $user->save();
                $this->sendConfirmationEmail($user, $data['confirmed']);
            }

            return \Response::json((new Action($action, $id))->data([])->commit());
        } else {
            $user->confirmed = 3;
            $user->save();
            $this->sendConfirmationEmail($user, $data['confirmed']);
            $action = (new Action($action, $id))->data([]);
            return \Response::json($action->commit());
        }
    }

    public function sendConfirmationEmail($user, $confirm)
    {
        $route = $confirm == 1 ? 'frontend.auth.emails.admin-confirm' : 'frontend.auth.emails.admin-reject';
        $subject = $confirm == 1 ? 'confirm' : 'reject';
        return Mail::send($route, [], function ($message) use ($user, $subject) {
            $message->to($user->email, $user->name)->subject(trans('exceptions.frontend.auth.confirmation.check_'.$subject));
        });
    }

}