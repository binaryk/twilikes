<?php namespace App\Models\Nomenclatoare;


class Announcement extends \App\Comptech\Model\Model
{


    protected $table = 'announcements';
    protected $guarded = ['id'];

    public static function Combobox()
    {
        $result = ['0' => trans('actions.no-selection')];
        $records = self::orderBy('id')->get();
        foreach( $records as $i => $record )
        {
            $result[$record->id] = $record->title;
        }
        return $result;

    }
}
