<?php namespace App\Repositories\Superadmin\Nomenclatoare\Announcement;

use App\Models\Nomenclatoare\Announcement;

class Action extends \App\Comptechsoft\Form\Action
{

	public function __construct($action, $id)
	{
		parent::__construct($action, $id);

		if( $action == 'insert' )
        {
            $record = new Announcement();
        }
        else
        {
            $record = Announcement::find( (int) $id);
        }

        $this->record($record);

		$this
			->addRule(['insert', 'update'], 'title', 'required', 'Titlul trebuie completat')
//			->addRule(['insert', 'update'], 'title', 'max:128', 'Numărul maxim de caractere este 128')
		;
	}
}