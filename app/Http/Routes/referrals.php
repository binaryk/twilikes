<?php

Route::group(['namespace' => 'Referrals', 'middleware' => 'web'], function () {
    Route::get('affiliate-publisher', 'ReferralsController@extern')
        ->name('extern.referrals.index');
});
Route::group(['namespace' => 'Referrals', 'middleware' => 'auth'], function () {

    Route::get('referral-index', 'ReferralsController@index')
    ->name('referrals.index');



    Route::get('api/referral-range', 'ReferralsController@data')
    ->name('api.referral.data');
});
