<?php namespace App\Repositories\Superadmin\Nomenclatoare\VerifyPosts;

use App\Models\Nomenclatoare\Post;

class Form extends \App\Comptechsoft\Form\Form
{

	public function __construct($action, $id)
	{
		parent::__construct($action, $id);
		
		$this->substantiv = 'Campaign: ';

	    if( $id )
	    {

	    	$this->record(Post::find( (int) $id ));
			$this->substantiv .= $this->record->title;

			$this->setParameter('file','<div id="insert"></div>');
	    }

		$this->captions = [
			'insert' => 'Adăugare ' . $this->substantiv,
			'update' => 'Modificare ' . $this->substantiv . ' (#' . $this->id . ')',
			'delete' => 'Ştergere ' . $this->substantiv,
		];

	    $this->view('superadmin.nomenclatoare.verify_posts.form.index');


	}
}