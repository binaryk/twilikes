<?php namespace App\Models;

use App\Comptech\Model\Model;
use Carbon\Carbon;
class Visit extends Model
{
    protected $table = "visits";
    protected $guarded = ['_token','id'];

    public static function today()
    {
        $get = 0;

        $data = self::whereDate('created_at', '=', date('Y-m-d'))->get();
        foreach($data as $dat){
            $get += $dat->amount;
        }
        return $get;
    }

    public static function yesterday()
    {
        $get = 0;
        $data = self::whereDate('created_at', '=', date('Y-m-d', strtotime("-1 day")))->get();
        foreach($data as $dat){
            $get += $dat->amount;
        }
        return $get;
    }

    public static function month()
    {

        $get = 0;
        $data = self::whereDate('created_at', '>=', date('Y-m-d', strtotime("-1 month")))->whereDate('created_at', '<=', date('Y-m-d'))->get();
        foreach($data as $dat){
            $get += $dat->amount;
        }
        return $get;
    }

    public static function incomes()
    {

        $get = 0;
        $data = self::all();
        foreach($data as $dat){
            $get += $dat->amount;
        }
        return $get;
    }

    public static function visits()
    {
        return self::all()->count();
    }



    
}