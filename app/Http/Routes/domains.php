<?php

Route::group(['namespace' => 'Domains', 'middleware' => 'auth'], function () {

    Route::get('domains-index', 'DomainsController@index')
    ->name('domains.index');
    Route::post('api/domain-available', 'DomainsController@available')
    ->name('api.domains.available');

    Route::post('api/domain-buy', 'DomainsController@buy')
    ->name('api.domains.buy');

    Route::post('api/domain-nameservers', 'DomainsController@nameservers')
    ->name('api.domains.nameservers');

    Route::get('api/domains-all', 'DomainsController@all')
    ->name('api.domains.all');
});


Route::group(['namespace' => 'Superadmin'], function () {
    Route::get('api/domains', 'DomainsController@all')
        ->name('api.domains.all');

    Route::post('api/domains/generate', 'DomainsController@generate')
        ->name('api.domains.generate');
});
