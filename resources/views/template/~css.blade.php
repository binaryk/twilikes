<link href="{!! asset('theme/css/bootstrap.min.css') !!}" rel="stylesheet">
<link href="{!! asset('theme/fonts/css/font-awesome.min.css') !!}" rel="stylesheet">
<link href="{!! asset('theme/css/animate.min.css') !!}" rel="stylesheet">
<link href="{!! asset('theme/css/custom.css') !!}" rel="stylesheet">
<link href="{!! asset('theme/css/icheck/flat/green.css') !!}" rel="stylesheet">
{!! Html::style( 'theme/js/sweetalert/dist/sweetalert.css' ) !!}
{!! Html::style( 'components/kendo/styles/web/kendo.common.css' ) !!}
{!! Html::style( 'components/kendo/styles/web/kendo.default.min.css' ) !!}
{!! Html::style( 'custom/css/compile/index.css' ) !!}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/2.0.0/toaster.min.css">
@yield('my-styles')
@yield('css')
<style>
    [v-cloak] .v-cloak--block {
        display: block;
    }
    [v-cloak] .v-cloak--inline {
        display: inline;
    }
    [v-cloak] .v-cloak--inlineBlock {
        display: inline-block;
    }
    [v-cloak] .v-cloak--hidden {
        display: none;
    }
    [v-cloak] .v-cloak--invisible {
        visibility: hidden;
    }
    .v-cloak--block,
    .v-cloak--inline,
    .v-cloak--inlineBlock {
        display: none;
    }

    .badge-primary {
        background-color: #337ab7;
    }

    .white-color {
        color: #ffffff !important;
    }
</style>
<script>
    var _config = {
        'base_url': "{!! url('/') !!}",
    }
</script>
