<?php namespace App\Repositories\Superadmin\Nomenclatoare\Users;

use \App\Comptechsoft\Datatable\Column;
use \App\Comptechsoft\Datatable\Header;
use \App\Comptechsoft\Datatable\Ajax;
use \App\Comptechsoft\Datatable\Order;
use \App\Comptechsoft\Datatable\Filter;

class Grid extends \App\Comptechsoft\Datatable\Grid
{

	public function __construct()
	{

		$this->noGlobalFilter();
		/*
		 * II asociez ID la grid
		 */
		$this->id('gridUsers');
        $this->pageLength = 5;

		/*
		 * In ce fisier sa se genereze js-ul pentru .DataTable({...});
		 */
		$this->js( str_replace('\\', '/', public_path('custom/js/users/grid.js') ));

		/*
		 * De la ce ruta vine raspunsul cu randuri
		 */
		$this->ajax( (new Ajax())->setUrl(\URL::route('superadmin.nomenclatoare.users.data-source'))->setExtra(['a' => 1, 'b' => 2]) );
		
		/*
		 * Care coloana contine ordinea initiala
		 */
		$this->order( (new Order())->setColumn(1)->setDirection('asc') );

		/*
		 * Definirea coloanelor
		 **/
		$this->addColumns([

			'rec-no' => 
				(new Column())
				->withHeader( 
					(new Header())
					->with(['caption' => '#', 'width' => 5])
				)
				->alignRight(),

			'id' => 
				(new Column())
				->withHeader( 
					(new Header())
					->with(['caption' => 'ID', 'width' => 5])
				)
				->alignCenter()
				->orderable(),

			'name' =>
				(new Column())
				->withHeader( 
					(new Header())
					->with(['caption' => 'Name', 'width' => 15])
					->withFilter( (new Filter())->view('superadmin.nomenclatoare.users.grid.filters.name') )
				)->orderable(),

			'email' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Email', 'width' => 15])
					->withFilter( (new Filter())->view('superadmin.nomenclatoare.users.grid.filters.email') )
				)->orderable(),

			'confirmed' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Confirmed', 'width' => 7])
				)->orderable(),

			'fbPage' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Fb page URL', 'width' => 20])
				)->orderable(),

			'fbPrintScreen' =>
				(new Column())
					->withHeader(
						(new Header())
							->with(['caption' => 'Fb print screen', 'width' => 20])
					)->orderable(),

			'created_at' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Created At', 'width' => 10])
				)->orderable(),

			'actions' =>
				(new Column())
				->withHeader(
					(new Header())
					->with(['caption' => 'Actions'])
				),


		
		]);

	}
}