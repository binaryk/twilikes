<table id="{{$attributes['id']}}" class="{{$class}}" cellspacing="{{$cellspacing}}" width="{{$width}}">
	{!! $header !!}

	{!! $footer !!}
</table>
