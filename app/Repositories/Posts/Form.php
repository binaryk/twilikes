<?php namespace App\Repositories\Superadmin\Nomenclatoare\Posts;

use App\Models\Nomenclatoare\Post;

class Form extends \App\Comptechsoft\Form\Form
{

	public function __construct($action, $id)
	{
		parent::__construct($action, $id);
		
		$this->substantiv = 'Post';

		$this->captions = [
	        'insert' => 'Adăugare ' . $this->substantiv,
	        'update' => 'Modificare ' . $this->substantiv . ' (#' . $this->id . ')',
	        'delete' => 'Ştergere ' . $this->substantiv,
	    ];

	    if( $id )
	    {
	    	$this->record(Post::find( (int) $id ));

			$this->setParameter('file','<div id="insert"></div>');
	    }

	    $this->view('superadmin.nomenclatoare.posts.form.index');


	}
}