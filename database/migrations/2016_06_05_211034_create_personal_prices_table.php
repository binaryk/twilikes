<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_prices', function(Blueprint $t){
            $t->bigIncrements('id');
            $t->integer('country_id');
            $t->float('mobile_price',8,4);
            $t->float('desktop_price',8,4);
            $t->integer('user_id');
            $t->softDeletes();
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personal_prices');
    }
}
