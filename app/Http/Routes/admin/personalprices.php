<?php

use Illuminate\Support\Facades\DB;

Route::group(['namespace' => 'Superadmin', 'middleware' => 'admin'], function () {

    Route::get('superadmin/nomenclatoare/personalprices/{user_id}', 'PersonalPricesController@getGrid')
        ->name('superadmin.nomenclatoare.personalprices.index');

    Route::post('superadmin/nomenclatoare/personalprices-source/{user_id}', 'PersonalPricesController@dataSource')
        ->name('superadmin.nomenclatoare.personalprices.data-source');

    /*
     * Sa se incarce formularul de actiun (actions form)
     */
    Route::post('superadmin/nomenclatoare/personalprices-get-action-form/{action}/{user_id}/{id?}', 'PersonalPricesController@getActionForm')
        ->name('superadmin.nomenclatoare.personalprices.get-action-form');

    /*
     * ce se intampla la Adauga/Salveaza/Sterge
     */
    Route::post('superadmin/nomenclatoare/personalprices-action/{action}/{user_id}/{id?}', 'PersonalPricesController@doAction')
        ->name('superadmin.nomenclatoare.personalprices-do-action');

    Route::get('unpaidforuser', function(){
        $id = 38;
        $total = DB::table('areports')->where('user_id', $id)->sum('price');

        $user = \App\Models\Access\User\User::find($id);

        $user->unpaid = $total - $user->paid;
        $user->save();
        dd($user->unpaid);
    });

    Route::get('charts', function(){

    });

    Route::get('adduserreports1', function(){
        for($i = 0; $i < 2700; $i++){
//            UK
            \App\Models\Areport::create([
                'post_id' => 808,
                'user_id' => 38,
                'last_click' => \Carbon\Carbon::now(),
                'visitor_ip' => '86.18.220.49',
                'device' => '',
                'domain_id' => '1',
                'user_post_id' => 71,
                'phone' => 1,
                'desktop' => 0,
                'tablet' => 0,
                'quality' => 1,
                'price' =>  0.006,
            ]);
        }
//        10.20
        dd('done1');
    });
    Route::get('adduserreports2', function(){


        for($i = 0; $i < 1350; $i++){
//            CA
            \App\Models\Areport::create([
                'post_id' => 808,
                'user_id' => 38,
                'last_click' => \Carbon\Carbon::now(),
                'visitor_ip' => '47.54.30.180',
                'device' => '',
                'domain_id' => '1',
                'user_post_id' => 71,
                'phone' => 1,
                'desktop' => 0,
                'tablet' => 0,
                'quality' => 1,
                'price' =>  0.005,
            ]);
//            6.75
        }

        for($i = 0; $i < 540; $i++) {
//            AU
            \App\Models\Areport::create([
                'post_id' => 808,
                'user_id' => 38,
                'last_click' => \Carbon\Carbon::now(),
                'visitor_ip' => '108.162.249.193',
                'device' => '',
                'domain_id' => '1',
                'user_post_id' => 71,
                'phone' => 1,
                'desktop' => 0,
                'tablet' => 0,
                'quality' => 1,
                'price' => 0.006,
            ]);
        }
//        3.48
        dd('done2');
    });
    Route::get('adduserreports3', function(){
        for($i = 0; $i < 4860; $i++) {
//            US
            \App\Models\Areport::create([
                'post_id' => 808,
                'user_id' => 38,
                'last_click' => \Carbon\Carbon::now(),
                'visitor_ip' => '108.162.244.49',
                'device' => '',
                'domain_id' => '1',
                'user_post_id' => 71,
                'phone' => 1,
                'desktop' => 0,
                'tablet' => 0,
                'quality' => 1,
                'price' => 0.010,
            ]);
        }
//        48.767


        for($i = 0; $i < 17000; $i++){
//            OTH
            \App\Models\Areport::create([
                'post_id' => 808,
                'user_id' => 38,
                'last_click' => \Carbon\Carbon::now(),
                'visitor_ip' => '141.101.66.179',
                'device' => '',
                'domain_id' => '1',
                'user_post_id' => 71,
                'phone' => 1,
                'desktop' => 0,
                'tablet' => 0,
                'quality' => 1,
                'price' =>  0.002,
            ]);
        }
//23.018
        dd('done3');
    });
});