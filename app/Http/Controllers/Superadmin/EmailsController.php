<?php namespace App\Http\Controllers\Superadmin;

use \App\Http\Controllers\Basic\BaseController;
use App\Jobs\EmailUsers;
use App\Models\Email;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Repositories\Superadmin\Nomenclatoare\Emails\Grid;
use App\Repositories\Superadmin\Nomenclatoare\Emails\Rows;
use App\Repositories\Superadmin\Nomenclatoare\Emails\Form;
use App\Repositories\Superadmin\Nomenclatoare\Emails\Action;

class EmailsController extends BaseController
{

    /**
     * $this->data() - vine din BaseController cu chestii comune (breadcrumb, meniu, ....)
     */
    public function getGrid()
    {
        $this->makeNavigation();
        return
            view('superadmin.nomenclatoare.emails.grid.index')
                ->with( $this->data('Posts', 'pentru filtrare') )
                ->withGrid( (new Grid())->parameters( Input::all()) )
                ->withToolbar( str_replace([Chr(13), Chr(10), Chr(9)], '',
                    view('superadmin.nomenclatoare.emails.grid.toolbar')->withActionRoute(\URL::route('superadmin.nomenclatoare.emails.get-action-form', ['action' => 'insert']))->render()) )
                ->withIcon(NULL)
                ->withDescription(NULL)
            ;
    }

    /*
     * Generarea inregistrari pentru Grid
     */
    public function dataSource()
    {
        return (new Rows())->parameters(Input::all())->response();
    }

    /*
     * Trimite catre index formularul "actions"
     */
    public function getActionForm($action, $id = NULL)
    {
        $form =
            (new Form($action, $id))
                ->setGridId("gridEmails")
                ->route(\URL::route('superadmin.nomenclatoare.emails-do-action', ['action' => $action, 'id' => $id]))
        ;

        return \Response::json([
            'caption' => $form->caption(),
            'form'    => $form->render(),
            'action'  => $form->actionCaption(),
            'route'   => $form->getRoute(),
            'impact' => $action,
        ]);
    }

    /**
     * do Action - se face adaugarea/modificarea/stergere
     */
    public function doAction( $action, $id = NULL )
    {

        $data = Input::except('file');
        $path = null;
        if(Input::hasFile('file')){
            $file               = Input::file('file');
            $tmp_name = $file->getClientOriginalName();//str_random(30).'_'.str_replace(' ','_',$file->getClientOriginalName());
            $res                = $file->move(public_path('uploads/emails'), $tmp_name);
            $data['img_name']   = $tmp_name;
            $data['path']       = $res->getPathName();
            $data['location']   = asset('uploads/emails/' . $tmp_name);
            $path = $data['path'];
        }
        $actionObject = (new Action($action, $id))->data($data);
        $res = $actionObject->commit();

        if($action == "insert"){
            $this->sendEmail($res['record'], $path);
        }
        return \Response::json($res);

    }

    public function sendEmail(Email $email, $path)
    {
        /*if($email->destination == "null"){

        }else{
            $destinations = explode(",",$email->destination);
            foreach($destinations as $dest){
                Mail::send('superadmin.nomenclatoare.emails.send.send', ['content' =>$email->content], function ($message) use ($email, $path, $dest) {
                    $message->subject($email->title);
                    $message->to($dest);
                    if ($path) {
                        $message->attach($path);
                    }
                });
            }

        }*/

        $job = (new EmailUsers($email, $path))->onQueue('emails');
        $this->dispatch($job);
    }

}