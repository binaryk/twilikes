<div class="row">

    <div class="form-group col-xs-12" id="image-upload-box">
        <label class="col-sm-2 control-label">Încarcă fișierul</label>
        <input type="file" name="file" class="form-control filestyle" id="file">
    </div>


    <div class="col-xs-12 form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-12">Destination</label>
        <div class="col-md-10 col-sm-10 col-xs-12">
            <select class="select2_multiple form-control" id="destination" multiple="multiple" name="destination">
                @foreach(\App\Models\Access\User\User::all() as $user)
                    <option value="{!! $user->email  !!}">{!! $user->email !!}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-xs-12 form-group">
        {!!
            App\Repositories\Ui\Controls\Textbox::make()
            ->name( $field = 'title')
            ->class('form-group mb')
            ->wrapper('col-sm-10')
            ->lclass('col-sm-2')
            ->caption( 'Title' )
            ->icon(NULL)
            ->value( $action != 'insert' ? $record->title : '')
            ->attributes([
                'id'          => $field,
                'class'       => 'form-control form-data-source',
                'placeholder' => 'Title...',
                'data-field'  => $field,
                'data-type'   => 'textbox'
            ])
            ->help('')
            ->render()
        !!}
	</div>

    @include('superadmin.nomenclatoare.emails.form.editor')

    <div class="clearfix"></div>

  <div class="row form-group">
  </div>
</div>