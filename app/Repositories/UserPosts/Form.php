<?php namespace App\Repositories\UserPosts;

use App\Models\Nomenclatoare\Post;

class Form extends \App\Comptechsoft\Form\Form
{

	public function __construct($action, $id)
	{
		parent::__construct($action, $id);
		
		$this->substantiv = 'Campaign';

		$this->action_captions = [
			'insert' => 'Submit image gallery',
			'update' => 'Save',
			'delete' => 'Delete',
		];

		$this->captions = [
	        'insert' => 'Add ' . $this->substantiv,
	        'update' => 'Edit ' . $this->substantiv . ' (#' . $this->id . ')',
	        'delete' => 'Delete ' . $this->substantiv,
	    ];

	    if( $id )
	    {
	    	$this->record(Post::find( (int) $id ));

			$this->setParameter('file','<div id="insert"></div>');
	    }

	    $this->view('user_posts.form.index');


	}
}