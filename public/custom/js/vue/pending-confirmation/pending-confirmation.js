new Vue({
    el: '#pending-confirmation',
    data: function() {
        return {
            formHasErrors: true,
            fbPrintScreenError: false,
            fbPageError: false
        }
    },
    ready: function() {
        $('#submit-button').on('click',this.submit);
    },
    methods: {
        submit() {
            if(!this.isFormInvalid()){
                $('#pending-form').submit();
            }
        },
        isFormInvalid() {
            var $url = $('#fbPage'),
                $print = $('#fbPrintScreen');

            this.fbPageError = $url.val() == '';
            this.fbPrintScreenError = $print.val() == '';
            this.formHasErrors = this.fbPageError || this.fbPrintScreenError;
            if(this.formHasErrors) {
                $('#messages').show();
            }
            return this.formHasErrors;
        }
    }
})
