<?php namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use App\Models\ArticlesEarning;
use App\Models\EarningsChart;
use App\Models\NewReferral;
use App\Models\NewReports;
use App\Models\Nomenclatoare\Post;
use App\Models\UserPost;
use App\Models\Visit;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

/**
 * Class LanguageController
 * @package App\Http\Controllers
 */

class TestController extends Controller
{
    public function __construct()
    {

    }

    public function images()
    {
        $images = File::allFiles(public_path('images'));

//        $posts  = Post::all();
        $total_images = count($images);
        $total_match = 0;
        $total_subst = 0;
        foreach($images as $image) {
            $originalName = $image->getFileName();
            $post = Post::where('path', 'like','%'.$originalName.'%')->first();

            $total_match++;
            if($post) {
                echo 'Post: '.$post->id.'<br/>';
                if(! file_exists($post->path)){
                    $total_subst++;
                    $this->changePhotoInPost($post, $image);
                } else {
                    echo 'file: '.$post->path.'<br/>';

                }
            }
        }
        dd('Operation success: Total img'.$total_images.'; total matches: '.$total_match.'; total substitutions:'.$total_subst);

    }

    public function changePhotoInPost($post, $file)
    {
        $filename = $file->getFileName();
        $file                = Image::make($file->getPathName());
        $tmp_name = str_random(30).'_'.str_replace(' ','_',$filename);

        $pathName = public_path('uploads').'/'.$tmp_name;
        $res                = $file->save($pathName);
        $data['img_name']   = $tmp_name;
        $data['path']       = $pathName;
        $data['location']   = asset('uploads/' . $tmp_name);
        $post->update($data);
        return true;
    }

    public function posts()
    {
        $posts = Post::orderBy('id','desc')->get();
        $arr = '';
        $len = 0;
        foreach($posts as $post) {
            if(file_exists($post->path)){
                $arr .= ';'.$post->id;
                $len++;
                $post->havent_image = 0;
                $post->save();
            }
        }
        dd($len);
    }

    public function getUsersByEarningsDesc($days)
    {
        $start_date = Carbon::now()->subDays($days)->toDateString();

        if($days == 0) {
            $data = DB::table('areports')
//            ->leftJoin('users','areports.user_id','=','users.id')
                ->select(DB::raw('
                areports.user_id as user_id,
                FLOOR(sum(price)) as earnings,
                areports.created_at
            '))
                ->whereDate('areports.created_at','=',$start_date)
                ->groupBy('user_id')
                ->orderBy('earnings', 'DESC')
                ->get();
        } else {
            $data = NewReports::select(DB::raw(
                '
                newreports.user_id as user_id,
                FLOOR(sum(newreports.earnings)) as earnings,
                newreports.created_at
                '
            ))->whereDate('newreports.created_at','=',$start_date)
                ->groupBy('user_id')
                ->orderBy('earnings', 'DESC')
                ->get();
        }

        return view('superadmin.test.users-by-earnings')->withData($data);



    }

    public function hack()
    {
        dd(44);
        return view('hack.index');
    }

    public function reportsJob()
    {
        $start_date = '2016-11-15';
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $users = User::where('id', '>',1)->where('status',1)->where('confirmed', 1)->get();
        foreach($users as $user) {
            \Log::info(PHP_EOL . 'Article earnings for user ' . $user->id . PHP_EOL);
            $data = DB::table('user_posts')
                ->leftJoin('posts', 'user_posts.post_id', '=', 'posts.id')
                ->leftJoin('areports', 'user_posts.id', '=', 'areports.user_post_id')
                ->select(DB::raw('
                user_posts.id as user_post_id,
                user_posts.user_id as user_id,
                user_posts.created_at as user_post_created_at,
                posts.id as post_id,
                posts.location as photo,
                posts.title as title,
                posts.created_at as post_created_at,
                FLOOR(count(1)) as clicks,
                ROUND(count(1) * avg(quality),0) as clicks_calitate,
                ROUND(avg(quality) * 100,2) as quality,
                ROUND(sum(price),3) as earnings,
                IFNULL(ROUND((sum(price) / (count(1) * avg(quality))),3), 0) as ecpc,
                IFNULL(ROUND((sum(price) * 1000 / (count(1) * avg(quality))),3) , 0) as ecpm,
                areports.created_at as reports_created_at,
                CURRENT_TIMESTAMP() as created_at,
                CURRENT_TIMESTAMP() as updated_at
                
            '))
                ->where('user_posts.user_id', '=', $user->id)
                ->where('areports.user_id', '=', $user->id)
                ->whereDate('user_posts.created_at', '=', $start_date)
                ->whereDate('areports.created_at', '=', $start_date)
                ->groupBy('user_posts.post_id')
                ->orderBy('user_posts.created_at', 'DESC')
                ->get();
            ArticlesEarning::insert($data);
        }
        dd('done');
    }

    public function setNewReferrals()
    {

        $users = User::where('status',1)->where('confirmed', 1)->get();
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        foreach($users as $user) {
            $dbData = DB::table('referrals')
                ->leftJoin('users', 'users.id', '=', 'referrals.invited_id')
                ->select(DB::raw('
                sum(earning) as earnings,
                DATE_FORMAT(referrals.created_at, "%Y-%m-%d") as date,
                users.name,
                referrals.invited_id as connection_id,
                referrals.invited_by_id as user_id
            '))
                ->where('referrals.invited_by_id', '=',$user->id)
                ->orderBy('date')
                ->groupBy('invited_id', 'date')
                ->get();

            DB::setFetchMode(\PDO::FETCH_ASSOC);

            NewReferral::insert($dbData);
        }
    }

    public function setEearningsChartData()
    {
        $users = User::where('status',1)->where('confirmed', 1)->get();
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $yesterday = Carbon::yesterday()->toDateString();
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        foreach ($users as $user) {
            $weekChart = DB::table('areports')
                ->select(DB::raw('
                sum(price) as price,
                device as country_id,
                areports.created_at,
                DATE_FORMAT(areports.created_at, "%Y-%m-%d") as data,
                user_id
            '))
                ->where('user_id', '=', $user->id)
                ->whereDate('created_at', '=', $yesterday)
                ->groupBy('country_id')
                ->groupBy('data')
                ->orderBy('data')
                ->get();
            DB::setFetchMode(\PDO::FETCH_ASSOC);

            EarningsChart::insert($weekChart);
        }
    }
}