import * as Highcharts from "highcharts";
//require('highcharts/modules/exporting')(Highcharts);
//window.Highcharts = Highcharts;
//declare var $;
export default class Chart{
    init(){
        console.log('init chart');
        $(function () {
            $('#container-chart').highcharts({
                title: {
                    text: 'Weekly Earnings',
                    x: -20 //center
                },
                credits: {
                    enabled: false
                },
                subtitle: {
                },
                xAxis: {
                    categories: ['12 Apr', '13 Apr', '14 Apr', '15 Apr', '16 Apr', '17 Apr', '18 Apr', '19 Apr'/*, 'Sep', 'Oct', 'Nov', 'Dec'*/]
                },
                yAxis: {
                    title: {
                        text: 'Earnings ($)'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: '$'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: 'Australia',
                    data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2]
                }, {
                    name: 'Canada',
                    data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8,]
                }, {
                    name: 'UK',
                    data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6]
                }, {
                    name: 'United States',
                    data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0]
                }, {
                    name: 'Others',
                    data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0]
                }
                ]
            });
        });
    }

}