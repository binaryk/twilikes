new Vue({
    el: '#events',
    data: {
        event: { name: '', description: '', date: '' },
        events: []
    },
    ready: function() {
        this.fetchEvents();
        $("#window").kendoWindow({
            width: "600px",
            title: "About Alvar Aalto",
            visible: false,
            actions: [
                "Pin",
                "Minimize",
                "Maximize",
                "Close"
            ]
        }).data("kendoWindow").center().open();
    },
    methods: {
        fetchEvents: function () {
            var events = [
                {
                    id: 1,
                    name: 'TIFF',
                    description: 'Toronto International Film Festival',
                    date: '2015-09-10'
                },
                {
                    id: 2,
                    name: 'The Martian Premiere',
                    description: 'The Martian comes to theatres.',
                    date: '2015-10-02'
                },
                {
                    id: 3,
                    name: 'SXSW',
                    description: 'Music, film and interactive festival in Austin, TX.',
                    date: '2016-03-11'
                }
            ];

            // $set is a convenience method provided by Vue that is similar to pushing
            // data onto an array
            this.$set('events', events);
        },
        info: function(el){
        }
    }
});